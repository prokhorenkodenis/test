import random
import re
import sys

import unittest
import time
import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotInteractableException # Взаимодействие(ввод текста...)
from selenium.common.exceptions import NoSuchElementException # Поиск
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementClickInterceptedException # Кликабельность
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from random import randint
import datetime
import allure
from allure_commons.types import AttachmentType
import requests
import json
from selenium.webdriver.common.keys import Keys
# import undetected_chromedriver as uc
from selenium.common.exceptions import JavascriptException

UA = 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36'
mobileEmulation = {"deviceMetrics": {"width": 412, "height": 914, "pixelRatio": 2.6}, "userAgent": UA}

link = "https://stroylandiya.ru/"
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-dev-shm-usage')  

browser = webdriver.Remote (command_executor = "http://selenium__standalone-chrome:4444/wd/hub", options = chrome_options) #chrome_options = chrome_options) command_executor="http://selenium__standalone-chrome:4444"
browser.set_window_position(0, 0)
browser.set_window_size(1920, 1080)
WebDriverWait(browser, 5)
action = ActionChains(browser)
datetime.datetime.utcnow()




def find_elem(selector):
    return(browser.find_element(By.CSS_SELECTOR, f'{selector}'))

def find_elems(selector):
    return(browser.find_elements(By.CSS_SELECTOR, f'{selector}'))

steps = []
debug_list = []
debug = False

def steps_list(step_msg):
    step = f'{step_msg}'
    steps.append(step)

def steps_case():
    for i in steps:
        steps_str = ('\n'.join(steps))
    with allure.step('steps'):
        allure.attach(steps_str, name = 'steps', attachment_type=AttachmentType.TEXT)
    steps.clear()

def debugger(debug_msg):
    if debug == True:
        debug_list.append(debug_msg)

def debug_case():
    if debug == True:
        for i in debug_list:
            debug_str = ('\n'.join(debug_list))
        with allure.step('debug'):
            allure.attach(debug_str, name = 'debug', attachment_type=AttachmentType.TEXT)
        debug_list.clear()
#document.querySelector("#swiper-wrapper-fc5e87843561cf1a > div.p_product_swiper--item.swiper-slide.swiper-slide-active > article > div.product_card_shoping.js-product_card_shoping.js-product-in-cart > div.dc-row.-sm.f-nowrap.m-0 > div:nth-child(2) > div > div:nth-child(3) > button")

def allure_step_border(name_step, border, index):
    try:
        browser.execute_script(f'document.getElementsByClassName("{border}")[{index}].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
    except JavascriptException:
        pass
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
    try:
        browser.execute_script(f"document.getElementsByClassName('{border}')[{index}].style.border=\"0px\";")
    except JavascriptException:
        pass
    
def allure_step(name_step):
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)

def result(result_list):
    for r in result_list:
        if r == False:
            test_result = False
            break
        else:
            test_result = True
    return(test_result)


def city_def():
        try:
            popup = find_elem('#city_locate')
        except NoSuchElementException:
            allure_step('В DOM нет попапа подтверждения города')
            return
        try:
            if popup.is_displayed() == True:
                find_elem('a[class="cities-locate__button cities-locate__button-ok js-cities-locate-ok"]').click()
                time.sleep(1)
        except NoSuchElementException:
            pass
        city = find_elem('a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        if city != "Оренбург":
            find_elem('div[class="city-choose-wrapper"]').click()
            find_elem('button[class="cities__results-item js-city-btn"][data-city="Оренбург"]').click()
            time.sleep(1)

def move_to(element):
    action.move_to_element(element).perform()

def idx_photo(idx, new_idx):
    if idx != new_idx:
        result = True
        debugger('Индексы разные TRUE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} TRUE')
    else:
        result = False
        debugger('Индексы одинаковые FALSE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} FALSE')
    return(result)

def open_close_photo(asd, photo_popup):
    if asd == "open_photo":
        if photo_popup.is_displayed() == True:
            allure_step_border('Фотографии открылись на весь экран',
                                'popup_wrapper',
                                '0')
            result = True
            debugger('открытие фотографий на весьэкран TRUE')
            steps_list('ОР: фотография развернулась на весь экран')
        else:
            allure_step('Фотографии не открылись на весь экран')
            result = False
            debugger('открытие фотографий на весьэкран FALSE')
    elif asd == "close_photo":
        if photo_popup.is_displayed() == False:
            allure_step('Фотографии закрылись')
            result = True
            debugger('Закрытие фоторгафий TRUE')
            steps_list('ОР: Фотографии закрылись')
        else:
            allure_step('Фотографии не закрылись')
            result = False
            debugger('Закрытие фоторгафий FALSE')
    return(result)


def tabs(result_list, name, button, border, block, block_border):
    try:
        tab = find_elem(f'{button}')
        move_to(tab)
        steps_list(f'Найти кнопку "{name}"')
        allure_step_border(f'Кнопка "{name}" найдена',
                            f'{border}',
                            '0')
        debugger(f'Поиск кнопк "{name}" TRUE')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Поиск кнопк "{name}" FALSE')
        allure_step(f'Кнопа "{name}" НЕ найдена')
        result_list.append(False)
    try:
        tab.click()
        time.sleep(1)
        debugger(f'Кликабельность кнопки "{name}" TRUE')
        allure_step(f'Кнопка "{name}" кликабельна')
        result_list.append(True)
        steps_list(f'Кликнуть в кнопку "{name}"')
    except ElementClickInterceptedException:
        debugger(f'Кликабельность кнопки "{name}" FALSE')
        result_list.append(False)
        allure_step(f'Кнопка "{name}" НЕ кликабельна')
    try:
        tab_block = find_elem(f'{block}')
        move_to(tab_block)
        time.sleep(0.4)
        debugger(f'Вкладка {name} открыта')
        allure_step_border(f'Вкладка "{name}" активна', 
                        f'{block_border}', 
                        '0')
        steps_list(f'ОР: Открылась вкладка "{name}" под блоком фото')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Вкладка {name} не открыта')
        result_list.append(False)
        allure_step(f'{name} не активна')


def no_such_element(result_list, css_sel, border, idx, msg, step, res):
    try:
        elem = find_elem(f'{css_sel}')
        try:
            move_to(elem)
        except:
            pass
        time.sleep(0.3)
        debugger(f'{msg} TRUE')
        allure_step_border(f'{msg} TRUE',
                            f'{border}',
                            f'{idx}')
        steps_list(f'{step}')
        result_list.append(True)
        return(find_elem(f'{css_sel}'))
    except NoSuchElementException:
        if res == True:
            result_list.append(True)
            allure_step(f'{msg} TRUE')
            debugger(f'{msg} TRUE')
            steps_list(f'{step}')
        else:
            result_list.append(False)
            allure_step(f'{msg} FALSE')
            debugger(f'{msg} FALSE')
        return(None)
    
    

def click_intercepted(elem, msg, step):
    try:
        elem.click()
        time.sleep(0.5)
        debugger(f'{msg} TRUE')
        allure_step(f'{msg} TRUE')
        result = True
        steps_list(f'{step}')
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == True:
        result = True
        debugger(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def not_displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == False:
        debugger(f'{msg} TRUE')
        result = True
        allure_step(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
    return(result)

def elements(css_sel, idx):
    elems = find_elems(f'{css_sel}')
    if idx == 'random':
        elem = elems[randint(0, len(elems) -1)]
    else:
        elem = elems[idx]
    return(elem)

def send_text(elem, text, msg):
    try:
        elem.send_keys(f'{text}')
        debugger(f'{msg} TRUE')
        allure_step(f'В поле ввода можно ввести {text} True')
        steps_list(f'Ввести в поле "{text}"')
        result = True
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        allure_step(f'В поле ввода нельзя ввести {text} False')
        result = False
    return(result)

def count_value(border, idx, old_count_value, new_count_value, msg, step, sign):
    if sign == "plus":
        if int(new_count_value) - int(old_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    else:
        if int(old_count_value) - int(new_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    return(result)
def sleep(sec):
    time.sleep(sec)

def delete_value(block_input, max_count):
    for r in range(len(max_count)+1):
        block_input.send_keys(Keys.ARROW_RIGHT)
        sleep(0.3)
    for d in range(len(max_count)+1):
        block_input.send_keys(Keys.BACKSPACE)  
        sleep(0.3)


def cityes(old_city_name, new_city_name):
    if old_city_name != new_city_name:
        result = True
        allure_step(f'Город изменился был - {old_city_name} стал - {new_city_name} TRUE') 
    else:
        result = False
        allure_step(f'Город не изменился был - {old_city_name} стал - {new_city_name} FALSE') 
    return(result)

def city_in_title(city, title_page):
    first_city_name = city.split(' ', 1)[0]
    now_city = first_city_name[:-2]
    if (re.findall(rf'{now_city}\w+', title_page, flags=re.IGNORECASE)):
        result = True
        with allure.step(f'Название города в тайтле страницы TRUE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    else:
        result = False
        with allure.step(f'Название города в тайтле страницы FALSE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    return(result)

def elems_displayed(result_list, css_sel, border, msg, step):
    try:
        elems = find_elems(f'{css_sel}')
    except NoSuchElementException:
        allure_step('На странице нет кнопок переключения блока')
        result_list.append(True)
        return
    idx = -1
    disp = 0
    for e in elems:
        idx += 1
        if e.is_displayed() == True:
            elem = e
            #move_to(elem)
            time.sleep(0.3)
            debugger(f'{msg} TRUE')
            allure_step_border(f'{msg} TRUE',
                                f'{border}',
                                f'{idx}')
            steps_list(f'{step}')
            result_list.append(True)
            break
    return(elem)
        
    
        
    # for d in elems:
    #     if d.is_displayed() == True:
    #         disp += 1
    # if disp <= 6:
    #     allure_step('На странице нет активных кнопок переключения блока')
    #     result_list.append(True)

def comparison(first, second, msg, equal):
    if equal == True:
        if first == second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    elif equal == False:
        if first != second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    return(result)

def banner_url(page, name):
    url = browser.current_url
    if page in url:
        result = True
    else: result = False
    #allure_step(f'После клика в {name}')
    return(result)

def back():
    browser.back()  

def h1_title(name):
    page_title = find_elem('#pagetitle').text
    if name in page_title:
        result = True
    else: result = False
    allure_step_border(f'h1 title страницы, после клика в {name}',
                       'fb-font-gilroy',
                       '0')
    return(result)  

def rel_bunner_page(with_city):
    if with_city == True:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_city}\w+', title, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    else:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    return(result)

link = "https://stroylandiya.ru/"






########################FOOTER#############################FOOTER#########################FOOTER#############################FOOTER#########################FOOTER#############################FOOTER#########################FOOTER   

@allure.feature('Карточка товара')
class test_a_product_page_header(unittest.TestCase):
    def test_a(self):
        # кнопка каталог
        result_list = []
        browser.get(link)
        try:
            city_def()
        except: pass
        try:
            item_of_day = find_elem('a[class="item-of-day__name"]')
            item_of_day.click()
            steps_list("Клик в активный слайд блока товары дня")
        except:
            menu = find_elem('div[class="menu-only"]')
            menu.click()
            steps_list("Клик в кнопку каталога")
            sleep(0.2)
            categoryes = find_elems('a[class="fb-header-catalog-menu__parent-link"]')
            category = categoryes[randint(0, len(categoryes) -1)]
            category_name = category.text
            move_to(category)   
            category.click()
            steps_list(f'Клик в {category_name}')
            sleep(1)
            sub_category = find_elem('div[class="fb-category-image-link__name"]')
            sub_category_name = sub_category.text
            sub_category.click()
            steps_list(f'Клик в {sub_category_name}')
            sleep(1)
            item = find_elem('a[class="fb-product-card__title-inner"]')
            item_name = item.text
            item.click()
            steps_list(f'Клик в {item_name}')
        #переход на карточку товара
        #browser.get('https://stroylandiya.ru/products/arka-pryamaya-berlin-pvkh-190kh2200kh2200mm-belyy-yasen/') #!!!!!!!!!ОДНО ФОТО!!!!
        #browser.get('https://stroylandiya.ru/products/drel-shurupovert-akkumulyatornaya-makita-df-347dwen/') # !!!!!!!ТУТ ЕСТЬ КНОПКА ПРОКРУТКИ ФОТО!!!!!!!!!!
        city_def() 
        try:
            city = no_such_element(result_list,
                                'div[class="city-choose-wrapper"]',
                                'city-choose-wrapper',
                                '0',
                                'Поиск кнопки выбора города',
                                'Найти кнопку выбора города',
                                False)
            old_city_name = city.text
            title_page = browser.title
            result_list.append(city_in_title(old_city_name,
                          title_page))
            result_list.append(displayed(city,
                                        "Отображение кнопки города на странице",
                                        'ОР: Кнопка город отображается на странице'))
            result_list.append(click_intercepted(city,
                                                'Кликабельность кнопки выбора города',
                                                'Кликнуть в кнопку выбора города'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="popup__body cities__body"]',
                                    'popup__body cities__body',
                                    '0',
                                    'Открытие попапа со списком городов',
                                    'ОР: Попап открылся',
                                    False)
            rnd_city = elements('button[class="cities__results-item js-city-btn"]',
                                'random')
            move_to(rnd_city)
            city_name = rnd_city.text
            result_list.append(click_intercepted(rnd_city,
                                                f'Кликабельность города {city_name} в списке',
                                                f'Кликнуть в город {city_name}'))
            sleep(1)
            new_city = no_such_element(result_list,
                                'div[class="city-choose-wrapper"]',
                                'city-choose-wrapper',
                                '0',
                                'Поиск кнопки выбора города',
                                'Найти кнопку выбора города',
                                False)
            new_city_name = new_city.text
            result_list.append(cityes(new_city_name,
                                       old_city_name))
            new_title_page = browser.title
            result_list.append(city_in_title(new_city_name,
                                            new_title_page))
            find_elem('div[class="city-choose-wrapper"]').click()
            sleep(0.5)
            form_input = no_such_element(result_list,
                                        'input[class="cities__form-input js-search-city"]',
                                        'cities__form-input js-search-city',
                                        '0',
                                        'Наличие поля ввода названия города',
                                        'Найти поле ввода названия города',
                                        False)
            result_list.append(click_intercepted(form_input,
                                                'Кликабельность поля ввода названия города',
                                                'Кликнуть в поле поиска города'))
            result_list.append(send_text(form_input,
                                        old_city_name,
                                        f'Ввести в поле {old_city_name}'))
            sleep(0.3)
            search_city =  no_such_element(result_list, 
                            'div[class="cities__results js-cities-list"] button[class="cities__results-item js-city-btn"]',
                            'cities__results js-cities-list',
                            '0',
                            'Есть результаты поиска города',
                            f'ОР: В результаттах поиска есть {old_city_name}',
                            False)
            result_list.append(click_intercepted(search_city,
                                                'Кликабельность результатов поиска города',
                                                'Кликнуть в первый результат поиска'))
            sleep(0.5)
            result_list.append(cityes(new_city_name,
                                       old_city_name))
            finally_page_title = browser.title
            result_list.append(city_in_title(old_city_name, 
                                             finally_page_title))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Банер над хедером')
    def test_b(self):
        result_list = []
        city_def()
        try:
            bunner = no_such_element(result_list,
                            'div[class="banner CROP TOP_HEADER hidden-sm hidden-xs"]',
                            'banner CROP TOP_HEADER hidden-sm hidden-xs',
                            '0',
                            'Поиск баннера над хедером',
                            'Найти баннер над хедером',
                            False)
            result_list.append(displayed(bunner,
                                        'Баннер отображается на странице',
                                        'ОР: Баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                'Кликабельность баннера над хедером',
                                                'Кликнуть в баннер над хедером'))
            sleep(1)
            result_list.append(rel_bunner_page(False))
            back()
            sleep(1)
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Магазины')
    def test_c(self):
        result_list= []
        try:
            shops = no_such_element(result_list,
                                    'a[class="shop_link"]',
                                    "shop_link",
                                    '0',
                                    'Поиск кнопки "Магазины"',
                                    'Найти кнопку "Магазины"',
                                    False)
            result_list.append(click_intercepted(shops,
                                                'Кликабельность кнопки "Магазины"',
                                                'Кликнуть в кнопку "Магазины"'))
            sleep(1)
            result_list.append(banner_url('shops',
                                          'Магазины'))
            back()
            sleep(1)
        
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Статус заказа')
    def test_d(self):
        result_list = []
        try:
            status = no_such_element(result_list,
                                    'a[class="button-header"]',
                                    'button-header',
                                    '0',
                                    'Поиск кнопки "Статус заказа"',
                                    'Найти кнопку статус заказа',
                                    False)
            result_list.append(click_intercepted(status,
                                                'Кликабельность кнопки "Статус заказа"',
                                                'Кликнуть в кнопку "Статус заказа"'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Поиск попапа авторизации',
                                    'Найти попап авторизации',
                                    False)
            result_list.append(displayed(popup,
                                        'Отображение попапа авторизации на странице',
                                        'ОР: Попап авторизации виден пользователю'))
            close_button = no_such_element(result_list,
                                           'div[class="auth-popup auth-popup-mobile fancybox-content"] a[class="popup__close popup__close-x close-popup"]',
                                           "popup__close popup__close-x close-popup",
                                           '2',
                                           'Поиск кнопки закрытия попапа',
                                           'Найти кнопку закрытия попапа',
                                           False)
            result_list.append(displayed(close_button,
                                        'Отображение кнопки закрытия попапа на странице',
                                        'ОР: Кнопка закрытия попапа видна пользователю'))
            result_list.append(click_intercepted(close_button,
                                                'Кликабельность кнопки закрытия попапа',
                                                'Кликнуть в кнопку закрытия попапа'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Закрытие попапа авторизации',
                                    'ОР: Попап авторизации не виден пользователю',
                                    True) 
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Номер телефона')
    def test_e(self):
        result_list = []
        try:
            phone = no_such_element(result_list,
                                    'a[class="header-phone"]',
                                    'header-phone',
                                    '0',
                                    'Поиск номера телефона',
                                    'Найти номер телефона',
                                    False)
            result_list.append(displayed(phone,
                                         'Отображение номера телефона на странице',
                                         'ОР: Номер телефона виден пользователю'))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    


    @allure.story('Хедер')
    @allure.title('Логотип')
    def test_f(self):
        result_list = []
        city_def()
        try:
            logo = find_elem('a[class="header-v2-logo"]')
            debug_msg = 'Поиск логотипа TRUE'
            steps_list('Найти логотип на странице')
            allure_step_border('Логотип найден', 'header-v2-logo', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'поиск логотипа FALSE'
            allure_step('Логотип не найден')
            result_list.append(False)
        debugger(debug_msg)
        try:
            logo.click()
            debug_msg = 'Клик в логотип TRUE'
            allure_step('Логотип кликабелен')
            result_list.append(True)
            steps_list("Кликнуть в логотип")
        except ElementClickInterceptedException:
            debug_msg = 'Клик в логотип FALSE'
            allure_step('Логотип не Кликабелен')
            result_list.append(False)
        debugger(debug_msg)
        back()
        sleep(1)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Проверка каталога')
    def test_g(self):
        result_list = []
        menu = no_such_element(result_list,
                'div[class="menu-only"]',
                'menu-only',
                '0',
                'Поиск кнопки "Каталог"',
                'Найти кнопку "Каталог"',
                False)
        result_list.append(click_intercepted(menu,
                           'Кликабельность кнопки "Каталог"', 
                           'Кликнуть в кнопку "Каталог"'))
        sleep(0.3)
        catalog_block = no_such_element(result_list,
                        'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]',
                        'fb-header-catalog-menu fb-header-catalog-menu_opened',
                        '0',
                        'Открытие каталога',
                        'ОР: Каталог открылся',
                        False)
        close_menu = no_such_element(result_list,
                        'div[class="menu-only"]',
                        'menu-only',
                        '0',
                        'Поиск кнопки закрытия каталога',
                        'Найти кнопку закрытия каталога',
                        False)
        result_list.append(click_intercepted(close_menu,
                            'Кликабельность кнопки закрытия каталога', 
                            'Кликнуть в кнопку закрытия каталога'))
        sleep(0.5)
        no_such_element(result_list,
                        'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]',
                        'fb-header-catalog-menu fb-header-catalog-menu_opened',
                        '0',
                        'Закрытие каталога',
                        'ОР: Каталог закрылся',
                        True)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Проверка поиска')
    def test_h(self):
        result_list = []
        search = no_such_element(result_list,
                        'input[class="search-input js-digi-search-input"]',
                        'search-input js-digi-search-input',
                        '0',
                        'Поиск блока "Поиска"',
                        'Найти блок поиска',
                        False)
        result_list.append(click_intercepted(search,
                          'Кликабельность поля поиска',
                          'Кликнуть в поле поиска'))
        search_results = no_such_element(result_list,
                        'div[class="search-results__wrapper"]',
                        'search-results__wrapper',
                        '0',
                        'Поиск блока результатов поиска',
                        'Найти блок поиска',
                        False)
        result_list.append(displayed(search_results,
                                     "Отображение блока поиска на странице",
                                     'Проверить отображение блока поиска на странице'))
        result_list.append(send_text(search,
                                     'Ламинат',
                                     'Ввести текст в поле поиска'))
        sleep(0.5)
        popular_searches = no_such_element(result_list,
                        'div[class="search-results__search-container search-results__popular-searches js-popular-searches"]',
                        'search-results__search-container search-results__popular-searches js-popular-searches',
                        '0',
                        'Поиск блока "Часто ищу"',
                        'Найти блок "Часто ищу"',
                        False)
        result_list.append(displayed(popular_searches,
                                     'Отображение блока "Часто ищу" на странице',
                                     'ОР: Блок "Часто ищу" отображается на странице'))
        search_categories = no_such_element(result_list,
                        'div[class="search-results__search-container search-results__categories js-search-categories"]',
                        'search-results__search-container search-results__categories js-search-categories',
                        '0',
                        'Поиск блока "Категории"',
                        'Найти блок "Категории"',
                        False)
        result_list.append(displayed(search_categories,
                                     'Отображение блока "Категории" на странице',
                                     'ОР: Блок "Категории" отображается на странице'))
        search_products = no_such_element(result_list,
                        'div[class="search-results__items-container js-search-products"]',
                        'search-results__items-container js-search-products',
                        '0',
                        'Поиск блока "Популярные товары"',
                        'Найти блок "Популярные товары"',
                        False)
        result_list.append(displayed(search_products,
                                     'Отображение блока "Популярные товары" на странице',
                                     'ОР: Блок "Популярные товары" отображается на странице'))
        search_submit_btn = no_such_element(result_list,
                        'button[class="search-results__all-results-btn js-search-submit-btn"]',
                        'search-results__all-results-btn js-search-submit-btn',
                        '0',
                        'Поиск кнопки "Все результаты"',
                        'Найти кнопку "Все результаты"',
                        False)
        result_list.append(displayed(search_submit_btn,
                                     'Отображение кнопки "Все результаты" на странице',
                                     'ОР: Кнопка "Все результаты" отображается на странице'))
        search_btn_desktop = no_such_element(result_list,
                        'button[class="search-input-digi-btn js-digi-search-btn-desktop"]',
                        'search-input-digi-btn js-digi-search-btn-desktop',
                        '0',
                        'Поиск кнопки "Найти"',
                        'Найти кнопку "Найти"',
                        False)
        result_list.append(displayed(search_btn_desktop,
                                     'Отображение кнопки "Найти" на странице',
                                     'ОР: Кнопка "Найти" отображается на странице'))
        reset_btn_desktop = no_such_element(result_list,
                        'button[class="search-input-digi-reset-btn js-digi-reset-btn-desktop"]',
                        'search-input-digi-reset-btn js-digi-reset-btn-desktop',
                        '0',
                        'Поиск кнопки удаления текста',
                        'Найти кнопку удаления текста',
                        False)
        result_list.append(displayed(reset_btn_desktop,
                                     'Отображение кнопки удаления текста на странице',
                                     'ОР: Кнопка удаления текста отображается на странице'))
        result_list.append(click_intercepted(reset_btn_desktop,
                          'Кликабельность кнопки удаления текста',
                          'Кликнуть в кнопку удаления текста'))
        try:
            header_grey = find_elem('header[class="header -grey"]').click()
        except:
            allure_step('не удалось кликнуть в хедер для снятия фикуса с поиска')
        result_list.append(not_displayed(search_results,
                                         'Закрытие окна поиска',
                                         'ОР: Окно результатов поиска закрыто'))
        steps_case()
        debug_case()
        assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Акции')
    def test_j(self):
        result_list = []
        try:
            stock = no_such_element(result_list,
                                    'a[class="header-v2-action"]',
                                    'header-v2-action',
                                    '0',
                                    'Поиск кнопки "Акции"',
                                    'Найти кнопку "Акции"',
                                    False)
            result_list.append(displayed(stock,
                                        'Отображении кнопки "Акции" на странице',
                                        'ОР: Кнопка "Акции" Видна пользователю'))
            result_list.append(click_intercepted(stock,
                            'Кликабельность кнопки "Акции"',
                            'Кликнуть в кнопку "Акции"'))
            sleep(0.5)
            page_title = browser.title
            result_list.append(banner_url('actions',
                                        'Акции'))
            result_list.append(h1_title('Акции'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
        

    @allure.story('Хедер')
    @allure.title('Вход')
    def test_k(self):
        result_list = []
        try:
            entrance = no_such_element(result_list,
                                    'a[class="header-v2-enter "]',
                                    'header-v2-enter ',
                                    '0',
                                    'Поиск кнопки "Вход"',
                                    'Найти кнопку "Вход"',
                                    False)
            result_list.append(click_intercepted(entrance,
                                                'Кликабельность кнопки "Вход"',
                                                'Кликнуть в кнопку "Вход"'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Поиск попапа авторизации',
                                    'Найти попап авторизации',
                                    False)
            result_list.append(displayed(popup,
                                        'Отображение попапа авторизации на странице',
                                        'ОР: Попап авторизации виден пользователю'))
            close_button = no_such_element(result_list,
                                           'div[class="auth-popup auth-popup-mobile fancybox-content"] a[class="popup__close popup__close-x close-popup"]',
                                           "popup__close popup__close-x close-popup",
                                           '2',
                                           'Поиск кнопки закрытия попапа',
                                           'Найти кнопку закрытия попапа',
                                           False)
            result_list.append(displayed(close_button,
                                        'Отображение кнопки закрытия попапа на странице',
                                        'ОР: Кнопка закрытия попапа видна пользователю'))
            result_list.append(click_intercepted(close_button,
                                                'Кликабельность кнопки закрытия попапа',
                                                'Кликнуть в кнопку закрытия попапа'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Закрытие попапа авторизации',
                                    'ОР: Попап авторизации не виден пользователю',
                                    True) 
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Сравнение')
    def test_l(self):
        result_list = []
        try:
            comparison_button = no_such_element(result_list,
                                                'span[class="basket-link jlink compare header-v2-compare "]',
                                                "basket-link jlink compare header-v2-compare ",
                                                '0',
                                                'Поиск кнопки "Сравнение"',
                                                'Найти кнопку сравнение',
                                                False)
            result_list.append(displayed(comparison_button,
                                        'Отображение кнопки "Сравнение" на странице',
                                        'ОР: Кнопка "Сравнение" видна пользователю'))
            result_list.append(click_intercepted(comparison_button,
                                                'Кликабельность кнопки "Сравнение"',
                                                'Кликнуть в кнопку "Сравнение"'))
            result_list.append(banner_url('compare',
                                        'Сравнение'))
            result_list.append(h1_title('Сравнение товаров'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Избранное')
    def test_m(self):
        result_list = []
        try:
            favorite = no_such_element(result_list,
                                                'span[class="header-v2-favourites jlink"]',
                                                "header-v2-favourites jlink",
                                                '0',
                                                'Поиск кнопки "Избранное"',
                                                'Найти кнопку "Избранное"',
                                                False)
            result_list.append(displayed(favorite,
                                        'Отображение кнопки "Избранное" на странице',
                                        'ОР: Кнопка "Избранное" видна пользователю'))
            result_list.append(click_intercepted(favorite,
                                                'Кликабельность кнопки "Избранное"',
                                                'Кликнуть в кнопку "Избранное"'))
            result_list.append(h1_title('Отложенные товары'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
 
    @allure.story('Хедер')
    @allure.title('Корзина')
    def test_n(self):
        result_list = []
        try:
            basket = no_such_element(result_list,
                                                'span[class="jlink header-v2-basket"]',
                                                "jlink header-v2-basket",
                                                '0',
                                                'Поиск кнопки "Корзина"',
                                                'Найти кнопку "Корзина"',
                                                False)
            result_list.append(displayed(basket,
                                        'Отображение кнопки "Корзина" на странице',
                                        'ОР: Кнопка "Корзина" видна пользователю'))
            result_list.append(click_intercepted(basket,
                                                'Кликабельность кнопки "Корзина"',
                                                'Кликнуть в кнопку "Корзина"'))
            result_list.append(banner_url('cart',
                                        'Корзина'))
            page_title = find_elem('h1[class="basket-d__heading"]').text
            if 'Корзина' in page_title:
                 result_list.append(True)
            else:  result_list.append(False)
            allure_step_border(f'h1 title страницы, после клика в "Корзина"',
                            'basket-d__heading',
                            '0')
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

@allure.feature('Карточка товара')
class test_b_product_page_body(unittest.TestCase):  
    @allure.story('Тело')
    @allure.title('Проверка кнопки Отзывы')
    def test_c(self):
    # отзывы 
    # тут баг с кнопкой, вернусь к этому позже
        url_before = browser.current_url
        result_list = []
        review = no_such_element(result_list,
                        'div[class="dc-row"] div[class="p_product_review dc-row -sm"] a[class="js-scrollto decoration-none"]',
                        'js-scrollto decoration-none',
                        '1',
                        'Поиск кнопки "Отзывы"',
                        'Найти кнопку "Отзывы"',
                        False)
        result_list.append(displayed(review,
                                     'Отображение кнопки "Отзывы" на странице',
                                     'ОР: Кнопка "Отзывы" отображается на странице'))
        result_list.append(click_intercepted(review,
                          'Кликабельность кнопки "Отзывы"',
                          'Кликнуть в кнопку "Отзывы"'))
        tab_review = no_such_element(result_list,
                        'div[class="dcol-0 p_product_info--item_wrapper js-reviews js-btns active"]',
                        'dcol-0 p_product_info--item_wrapper js-reviews js-btns active',
                        '0',
                        'Поиск вкладки "Отзывы"',
                        'Найти вкуладку "Отзывы"',
                        False)
        result_list.append(displayed(tab_review,
                                    'Отображение вкладки "Отзывы" на странице',
                                    'ОР:Вкладка "Отзывы" отображается на странице'))
        url_after = browser.current_url
        # В данный момент кнопка НЕ работает, срабатывает якорь, сами отзывы НЕ разворачиваются, можно сравнивать ссылки //otzyvy/ - рабочая кнопка
        print(url_before)
        print(url_after)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Проверка кнопки к сравнению')
    def test_d(self):
        # к сравнению
        result_list = []
        sleep(1)
        action.scroll(0, 0, 0, -2000).perform()
        compare = no_such_element(result_list,
                        'div[class="dc-row"] button[class="p_product_action--btn js_compare_item"] span',
                        'p_product_action--btn js_compare_item',
                        '1',
                        'Поиск кнопки "К сравнению"',
                        'Найти кнопку "К сравнению"',
                        False)
        result_list.append(displayed(compare,
                                     'Отображение кнопки "К сравнению" на странице',
                                     'ОР: Кнопка "К сравнению" отображается на странице'))
        result_list.append(click_intercepted(compare,
                          'Кликабельность кнопки "К сравнению"',
                          'Кликнуть в кнопку "К сравнению"'))
        compare_count = no_such_element(result_list,
                        'span[class="basket-link jlink compare header-v2-compare "] span[class="count"]',
                        'basket-link compare header-v2-compare ',
                        '0',
                        'Поиск счетчика в иконке "Сравнение"',
                        'Найти счетчик в иконке "Сравнение"',
                        False)
        result_list.append(displayed(compare_count,
                                    'Отображение счетчика количества товаров',
                                    'ОР: В иконке сравнение отображается счетчик товаров'))
        try:
            compare_count = find_elem('span[class="basket-link jlink compare header-v2-compare "] span[class="count"]')
            compare_count_name = compare_count.text
            debug_msg = f'Количество товара в сравнении TRUE' #!!!!!!!!!!!!!!!!!!!!!!обработать количество!!!!!!!!!!!!!!!!!!!!!!!!!!
            allure_step_border('Количество товара в сравнении изменилось', 'basket-link compare header-v2-compare ', '0')
            steps_list('Проверить количество товара в иконке сравнения')
            result_list.append(True)
        except NoSuchElementException:
            allure_step('Количество в сравнении товара НЕ изменилось')
            debug_msg = 'Количество товара в сравнении FALSE'
            result_list.append(False)
        
        # debugger(debug_msg)
        
        steps_case()
        debug_case()
        assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Проверка кнопки в избранное')
    def test_e(self):
    # в избранное
        result_list = []
        try:
            favorit = find_elem('div[class="dc-row"] button[class="p_product_action--btn js_wish_item"] span[class="d-none d-lg-inline-flex"]')
            debug_msg = 'Поиск кнопки "В избранное" True'
            steps_list('Найти кнопку "В избранное" на странице')
            allure_step_border('На странице есть кнопка "В избранное"', 'dcol-0 p_product_action--item', '3')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Поиск кнопки "В избранное" FALSE'
            allure_step('Кнопка "В избранное" НЕ найдена')
            result_list.append(False)
        # debugger(debug_msg)
        try:
            favorit.click()
            debug_msg = 'Кликабельность кнопки "В избранное" True'
            steps_list('Кликнуть в кнопку избранное')
            result_list.append(True)
        except ElementClickInterceptedException:
            debug_msg = 'Кликабельность кнопки "В избранное" False'
            result_list.append(False)
        sleep(1.5)
        # debugger(debug_msg)
        try:
            favorin_count = find_elem('span[class="fb-sticky-menu__badge_type_personal count"]')
            favorin_count_int = favorin_count.text
            debug_msg = f'Количество товара в иконке True {favorin_count_int}'
            allure_step_border('Проверить что в иконку избранное добавилось +1', 'header-v2-favourites', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Количество товара в иконке False' 
            result_list.append(False) 
        # debugger(debug_msg)
        steps_case()
        debug_case()
        try:
            find_elem('span[class="fb-sticky-menu__badge_type_basket count"]')
            find_elem('div[class="logo_and_menu-row"] a[href="/cart/"]').click()
            browser.back()
        except NoSuchElementException:
            pass
        assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Проверка кнопки поделиться')
    def test_f(self):
        # поделиться
        result_list = []
        try:
            share = find_elem('div[class="dcol-0 p_product_action--item share"]')
            debug_msg = 'Поиск кнопки "Поделиться" TRUE'
            steps_list('Найти кнопку поделиться')
            allure_step_border('Кнопка "Поделиться" есть на странице', 'dcol-0 p_product_action--item share', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Поиск кнопки "Поделиться" FALSE'
            allure_step('Кнопка "Поделиться" отсутствует на странице')
            result_list.append(False)
        # debugger(debug_msg)
        try:
            move_to(share)
            debug_msg = 'Кликабельность кнопки поделиться TRUE'
            steps_list('Кликнуть в кнопку поделиться')
            allure_step('Кнопка поделиться кликабельна')
            result_list.append(True)
        except ElementClickInterceptedException:
            debug_msg = 'Кликабельность кнопки поделиться FALSE'
            allure_step('Кнопка поделиться НЕ кликабельна')
            result_list.append(False)
        sleep(0.4)
        # debugger(debug_msg)
        try:
            find_elem('ul[class="p_product_action_list"]')
            debug_msg = 'Открытие блока "Поделиться" TRUE'
            allure_step_border('Блок "Поделиться" открывается', 'p_product_action_list', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Открытие блока "Поделиться" FALSE'
            allure_step('Блок "Поделиться" НЕ открывается')
            result_list.append(False)
        steps_case()
        debug_case()
        find_elem('header[class="header -grey"]').click()
        assert result(result_list) == True


    # добавить в корзину
    @allure.story('Тело')
    @allure.title('Проверка работы кнопки добавления в корзину')
    def test_g(self):
        result_list = []
        try:
            add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            debug_msg = 'Поиск кнопки "добавкить в корзину" TRUE'
            steps_list('Найти кнопку "Добавитьв корзину"')
            allure_step_border('Кнопка "добавить в корзину" найдена', 'dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Поиск кнопки "добавкить в корзину" FALSE'
            allure_step('Кнопка "Добавить в корзину" НЕ найдена')
            result_list.append(False)
        # debugger(debug_msg)
        try:
            icon_cart = find_elem('span[class="fb-sticky-menu__badge_type_basket count "]')
            count_cart = int(icon_cart.text)
        except NoSuchElementException:
            count_cart = None
            pass
        try:
            add_cart.click()
            steps_list('Кликнуть в кнопку "Добавить в корзину"')
            debug_msg = 'Клик в кнопку в корзину TRUE'
            allure_step('Кнопка "Добавить в корзину" кликабельна')
            result_list.append(True)
        except ElementClickInterceptedException:
            result_list.append(False)
            debug_msg = 'Клик в кнопку в корзину FALSE'
            allure_step('Кнопка "Добавить в корзину" НЕ кликабельна')
        # debugger(debug_msg)
        try:
            new_icon_cart = find_elem('span[class="fb-sticky-menu__badge_type_basket count"]')
            new_count_cart = int(new_icon_cart.text)
            allure_step_border('Количесвто товара в иконке появилось', 'header-v2-basket', '0')
            steps_list('Проверить количество товара в корзине')
            debug_msg = 'Количество товара в корзине = TRUE'
            result_list.append(True)
        except NoSuchElementException:
            result_list.append(False)
            debug_msg = 'Количество товара в корзине = FALSE'
            allure_step_border('Количесвто товара в иконке НЕ появилось', 'header-v2-basket', '0')
        # debugger(debug_msg)
        if count_cart == None:
            count = new_count_cart
        else:
            count = new_count_cart - count_cart
        if count == 1:
            result_list.append(True)
        else: result_list.append(False)
        steps_case()
        debug_case()
        assert result(result_list) == True

    # изменить количество в корзину 
    @allure.story('Тело')
    @allure.title('Проверка работы поля корзины')
    def test_h(self):
        try:
            result_list = []
            try:
                count = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            except NoSuchElementException:
                try:
                    add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
                    add_cart.click()
                except NoSuchElementException:
                    pytest.skip('На странице нет возможности добавить товар в корзину')
            old_count_value = count.get_attribute('data-quantity')
            plus = no_such_element(result_list,
                            'div[class="p_product_shoping--wrapper"] div[class="dcol-0 product_card_qty--plus-container"]',
                            'dcol-0 product_card_qty--plus-container',
                            '1',
                            'Поиск кнопки "+"',
                            'Найти кнопку "+"',
                            False)
            result_list.append(click_intercepted(plus,
                              'Кликабельность кнопк "+"',
                              'Кликнуть в кнопку "+"'))
            try:
                new_count_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('p_product_shoping--wrapper',
                                           '0',
                                           old_count_value,
                                           new_count_value,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'plus'))
            minus = no_such_element(result_list,
                            'div[class="p_product_shoping--wrapper"] button[class="btn_reset product_card_qty--btn js_minus"]',
                            'btn_reset product_card_qty--btn js_minus',
                            '3',
                            'Поиск кнопки "-"',
                            'Найти нопку "-"',
                            False)
            result_list.append(click_intercepted(minus,
                              'Кликабельность кнопк "-"',
                              'Кликнуть в кнопку "-"'))
            sleep(0.5)
            try:
                count_value_minus = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('p_product_shoping--wrapper',
                                           '0',
                                           new_count_value,
                                           count_value_minus,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'minus'))
            minus.click()
            steps_list('Повторно кликнуть в кнопку "-"')
            sleep(1)
            try:
                add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            except NoSuchElementException:
                pass
            if add_cart.is_displayed() == True:
                result_list.append(True)
                allure_step_border('Кликом в кнопку "-" можно закрыть поле корзины', 'dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width', '0')
                debug_msg = 'Закрытие поля корзины кликом в кнопку "-" True'
                steps_list('Проверить закрытие поля корзины')
            else:
                result_list.append(False)
                allure_step_border('Кликом в кнопку "-" НЕльзя закрыть поле корзины', 'p_product_block -shoping mb-24 js-shoping_block', '0')
                debug_msg = 'Закрытие поля корзины кликом в кнопку "-" False'
            # debugger(debug_msg)
            count_value_minus_second = find_elem('span[class="jlink header-v2-basket"] span[class="fb-sticky-menu__badge_type_basket count"]').text
            if int(count_value_minus_second) == 0:
                result_list.append(True)
                allure_step_border('Количесвто товара в иконке изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины "-" True'
                steps_list('Проверить изменение количества товара в иконке корзины')
            else:
                allure_step_border('Количесвто товара в иконке НЕ изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины "-" False'
                result_list.append(False) 
            # debugger(debug_msg)
            try:
                add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            except NoSuchElementException:
                pass
            add_cart.click()
            sleep(0.3)
            try:
                count_input = find_elem('div[class="p_product_block -shoping mb-24 js-shoping_block"] input[type="number"]')
                debug_msg = 'Поиск поля ввода TRUE'
                allure_step_border("На странице есть поле для ввода количества товара", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
                steps_list('найти поле ввода количества товара')
                result_list.append(True)
            except NoSuchElementException:
                debug_msg = 'Поиск поля ввода FALSE'
                result_list.append(False)
                allure_step_border("На странице нет поля для ввода количества товара", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            try:
                count_input.click()
                debug_msg = "Кликабельность поля ввода количества TRUE"
                steps_list('Кликнуть в поле вовода количества товара')
                result_list.append(True)
                allure_step("Поле ввода количества товара кликабельно")
            except ElementClickInterceptedException:
                debug_msg = "Кликабельность поля ввода количества FALSE"
                result_list.append(False)
                allure_step("Поле ввода количества товара НЕ кликабельно")
            # debugger(debug_msg)
            try:
                sleep(0.4)
                count_input.send_keys(Keys.ARROW_RIGHT)
                sleep(0.4)
                count_input.send_keys(Keys.BACKSPACE)  
                sleep(0.4)
                debug_msg = "Удаление старого значения TRUE"
            except:
                debug_msg = "Удаление старого значения FALSE"
            # debugger(debug_msg)
            try:
                count_input.send_keys('2')
                steps_list('Ввести в поле значение "2"')
                debug_msg = 'Ввод значения в поле TRUE'
                result_list.append(True)
                allure_step_border("В поле можно ввести значение", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = 'Ввод значения в поле FALSE'
                result_list.append(False)
                allure_step_border("В поле НЕльзя ввести значение", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            try:
                without_focus = find_elem('header[class="header -grey"]').click()
                steps_list('Снять фокус с поля')
                debug_msg = 'Снятие фокуса с поля TRUE'
            except:
                debug_msg = 'Снятие фокуса с поля FALSE'
            count_input_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            sleep(0.3)
            if int(count_input_value) == 2:
                debug_msg = "Введенное значение сохранилось в поле TRUE"
                result_list.append(True)
                allure_step_border("Введенное значение сохранилось в поле", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            else:
                debug_msg = "Введенное значение сохранилось в поле FALSE"
                result_list.append(False)
                allure_step_border("Введенное значение НЕ сохранилось в поле", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            delete_value(count_input, 
                         "1")
            max_count = find_elem('div[class="dcol-0 product_card_qty--plus-container"] button[class="btn_reset product_card_qty--btn js_plus"]').get_attribute('data-max')
            if '.' in max_count:
                max_count = max_count.split('.')[0]
            try:
                count_input.send_keys(max_count)
                steps_list(f'Ввести в поле максимальное значение {max_count}')
                debug_msg = 'Ввод максимального значения в поле TRUE'
                result_list.append(True)
                allure_step_border(f"В поле можно ввести максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = 'Ввод максимального значения в поле FALSE'
                result_list.append(False)
                allure_step_border(f"В поле НЕльзя ввести максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            without_focus = find_elem('header[class="header -grey"]')
            without_focus.click()
            steps_list("Снять фокус с поля")
            sleep(0.3)
            max_input_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            if int(max_input_value) == int(max_count):
                steps_list(f'Проверить значение в поле {max_count}')
                debug_msg = 'Сохранение максимального значения в поле TRUE'
                result_list.append(True)
                allure_step_border(f"В поле сохранилось максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            else:
                debug_msg = 'Сохранение максимального значения в поле FALSE'
                result_list.append(False)
                allure_step_border(f"В поле НЕ сохранилось максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            delete_value(count_input, 
                         max_count)
            sleep(1)
            try:
                count_input.send_keys(int(max_count)+1)
                steps_list('Ввести в поле значение больше максимального')
                debug_msg = f'Ввод значения, больше максимального {int(max_count)+1} TRUE'
                result_list.append(True)
                allure_step_border(f"В поле можно ввести значение больше максимального {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = f'Ввод значения, больше максимального {int(max_count)+1} FALSE'
                result_list.append(False)
                allure_step_border(f"В поле НЕльзя ввести значение больше максимального {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            without_focus.click()
            steps_list("Снять фокус с поля")
            sleep(0.3)
            upper_max_input_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            if int(max_input_value) == int(max_count):
                steps_list(f'Проверить значение в поле {max_count}')
                debug_msg = f'Изменение значения на максимальное {max_count} TRUE '
                result_list.append(True)
                allure_step_border(f"В поле не сохранилось значение больше максимального максимальное - {max_count} тестируемое - {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            else:
                debug_msg = f'Изменение значения на максимальное {max_count} FALSE'
                result_list.append(False)
                allure_step_border(f"В поле сохранилось значение больше максимального максимальное - {max_count} тестируемое - {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            delete_value(count_input, 
                         max_count)
            try:
                count_input.send_keys('0')
                steps_list('Ввести в поле "0"')
                debug_msg = f'Ввеод в поле "0" TRUE'
                result_list.append(True)
                allure_step_border(f'В поле можно ввести "0"', 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = f'Ввеод в поле "0" FALSE'
                result_list.append(False)
                allure_step_border(f'В поле НЕльзя ввести "0"', 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            without_focus.click()
            steps_list("Снять фокус с поля")
            sleep(0.3)
            if add_cart.is_displayed() == True:
                result_list.append(True)
                allure_step_border('Вводом в поле "0" можно закрыть поле корзины', 'dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width', '0')
                debug_msg = 'Закрытие поля корзины вводом в поле "0" True'
                steps_list('Проверить закрытие поля корзины')
            else:
                result_list.append(False)
                allure_step_border('Вводом в поле "0" НЕльзя закрыть поле корзины', 'p_product_block -shoping mb-24 js-shoping_block', '0')
                debug_msg = 'Закрытие поля корзины вводом в поле "0" False'
            # debugger(debug_msg)
            count_value_after_null = find_elem('span[class="jlink header-v2-basket"] span[class="fb-sticky-menu__badge_type_basket count"]').text
            if int(count_value_after_null) == 0:
                result_list.append(True)
                allure_step_border('Количесвто товара в иконке изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины после ввода "0" True'
                steps_list('Проверить изменение количества товара в иконке корзины')
            else:
                allure_step_border('Количесвто товара в иконке НЕ изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины после ввода "0" False'
                result_list.append(False) 
        finally:
            # debugger(debug_msg)
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Проверка фотографий')
    def test_i(self):
        try:
            result_list = []
            size_list = []
            try:
                main_photo = find_elem('div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"] img')
                result_size = main_photo.size
                result_list.append(True)
                allure_step_border('На странице есть фотография', 'p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active', '0')
                debug_msg = 'Поиск открытого фото True'
                steps_list('Найти открытое фото на странице')
            except NoSuchElementException:
                try:
                    main_photo = find_elem('div[class="p_product_swiper_main--item swiper-slide js-zoom"] [class="img"]')
                    result_size = main_photo.size
                    result_list.append(True)
                    debug_msg = 'Поиск открытого фото True'
                    allure_step_border('На странице есть фотография', 'p_product_swiper_main--item swiper-slide js-zoom', '0')
                    steps_list('Найти открытое фото на странице')
                except NoSuchElementException:
                    result_list.append(False)
                    debug_msg = 'Поиск открытого фото False'
            # debugger(debug_msg)
            steps_list('Проверить размеры фото')
            for i in result_size.values():
                if int(i) > 50:
                    result_list.append(True)
                    debug_msg = 'Размеры фото TRUE'
                else: 
                    result_list.append(False)  
                    debug_msg = 'Размеры фото False'
            # debugger(debug_msg)
            try:
                style_before = main_photo.get_attribute('style')
                allure_step_border('Фотография до фокуса', 'p_product_swiper_main--item swiper-slide js-zoom', '0')
                move_to(main_photo)
                sleep(0.5)
                steps_list('Навести фокус на фото')
                allure_step_border('Фотография после фокуса', 'p_product_swiper_main--item swiper-slide js-zoom', '0')
                style_after = main_photo.get_attribute('style')
                steps_list('ОР: Фото увеличилось')
                if style_before != style_after:
                    result_list.append(True)
                    debug_msg = 'Изменение стиля True'
                else: 
                    result_list.append(False)
                    debug_msg = 'Изменение стиля False'
                # debugger(debug_msg)
                debug_msg = 'Наведение фокуса TRUE'
            except:
                debug_msg = 'Наведение фокуса FALSE'
                allure_step('На странице нет фото товара')
            # debugger(debug_msg)     
            #переключение
            try:
                photos = find_elems('div[class="p_product_swiper_main_nav--wrapper d-none d-lg-block"] img[class="img"]') 
                debugger('Поиск блока миниатюр TRUE')
                allure_step_border('На странице есть блок с миниатюрами фото', 'p_product_swiper_main_nav--wrapper d-none d-lg-block', '0')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Поиск блока миниатюр FALSE')
                result_list.append(False)
                allure_step_border('На странице нет блока с миниатюрами фото', '"p_product_swiper_main_nav--wrapper d-none d-lg-block', '0')
            coun_photo = len(photos)
            steps_list('Посчитать количество фото')
            if coun_photo <= 1:
                allure_step('Всего одно фото')
                debugger(debug_msg = 'Фото больше 1шт. FALSE')
            else:
                sleep(0.5)
                debugger(debug_msg = 'Фото больше 1шт. TRUE')
                for i in photos:
                    try:
                        i.click()
                        allure_step('После клика по фото')
                        steps_list('Кликнуть по миниатюре фото')
                        result_list.append(True)
                        debugger(debug_msg = 'Кликабельность миниатюры TRUE')
                    except ElementNotInteractableException:
                        try:
                            button_next = find_elem('button[id="p_product_swiper_main_nav--next"]')
                            debugger(debug_msg = 'Поиск кнопки прокрутки TRUE')
                            steps_list('Найти кнопку прокрутки фото')
                            allure_step_border('Кнопка прокрутки фотографий найдена', 'icon -Icon_arrow_down ', '0')
                            result_list.append(True)
                            sleep(0.5)
                            try:
                                button_next.click()
                                steps_list('Кликнуть по кнопке прокрутки фотографий')
                                debugger(debug_msg = 'Кликабельность кнопки прокрутки TRUE')
                                allure_step('Кнопка прокрутки фотографий кликабельна"')
                                result_list.append(True)
                                try:
                                    i.click()
                                    steps_list('Кликнуть по миниатюре фото')
                                    allure_step('После клика по фото')
                                    result_list.append(True)
                                    debugger(debug_msg = 'Кликабельность миниатюры TRUE')
                                except ElementNotInteractableException:
                                    allure_step('Фотография не кликабельна')
                                    result_list.append(False)
                                    debugger(debug_msg = 'Кликабельность миниатюры FALSE')
                            except ElementNotInteractableException:
                                allure_step('Кнопка прокрутки фотографий не кликабельна"')
                                result_list.append(False)
                                debugger(debug_msg = 'Кликабельность кнопки прокрутки FALSE')
                        except NoSuchElementException:
                            allure_step('Кнопка прокрутки фотографий не найдена')
                            debugger(debug_msg = 'Поиск кнопки прокрутки FALSE')
                            result_list.append(False)
            steps_list('ОР: Фотографии переключаются')
            #открытие фото
            try:
                open_photo = find_elem('div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"]')
                steps_list('Найти открытое фото')
                debugger('Поиск открытого фото TRUE')
                result_list.append(True)
                allure_step_border('Открытое фото найдено', 
                                   'p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active', 
                                   '0')
            except NoSuchElementException:
                debugger('Поиск открытого фото FALSE')
                result_list.append(False)
                allure_step('Открытое фото НЕ найдено')
            try:
                open_photo.click()
                sleep(1)
                debugger('Кликабельность открытого фото TRUE')
                result_list.append(True)
                allure_step('Открытое фото кликабельно')
                steps_list("Кликнуть в открытое фото")
            except ElementClickInterceptedException:
                debugger('Кликабельность открытого фото FALSE')
                result_list.append(False)
                allure_step('Открытое фото НЕ кликабельно')
            try:
                photo_popup = find_elem('div[class="popup_wrapper"]')
                debugger('Поиск блока открытия фотограйи на весь экран TRUE')
            except NoSuchElementException:
                allure_step('На странице нет блока открытых фотографий')
            result_list.append(open_close_photo('open_photo', photo_popup))
            #переключение фото вперед
            try:
                button_next = find_elem('a[class="popup-next next"]')
                debugger('Поиск кнопки переключения фото TRUE')
                result_list.append(True)
                allure_step_border('Кнопка переключения фотографий next найдена', 
                                   'popup-next next', 
                                   '0')
                steps_list("Найти кнопку переключения фотографии next")
            except NoSuchElementException:
                debugger('Поиск кнопки переключения фото FALSE')
                result_list.append(False)
                allure_step('Кнопка переключения фотографий next НЕ найдена')
            try:
                idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Получить индекс активного фото TRUE')
                result_list.append(True)
                steps_list("Получить индекс активного фото")
            except NoSuchElementException:
                debugger('Получить индекс активного фото FALSE')
                result_list.append(False)
            try:
                button_next.click()
                sleep(1)
                debugger('Кликабельность кнопки переключения фото TRUE')
                result_list.append(True)
                allure_step('Кнопка переключения фото next кликабельна')
                steps_list("Кликнуть в кнопку переключения фотографии next")
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки переключения фото FALSE')
                result_list.append(False)
                allure_step('Кнопка переключения фото next НЕ кликабельна')
            try:
                new_idx = find_elem('div[class="slide swiper-slide slider-track-item swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Получить индекс нового активного фото TRUE')
                result_list.append(True)
                steps_list('Получить индекс нового активного фото(next)')
            except NoSuchElementException:
                debugger('Получить индекс нового активного фото FALSE')
                result_list.append(False)
            steps_list('ОР: Фотографии переключаются')
            result_list.append(idx_photo(idx, new_idx))
            # Переключение фото назад
            try:
                button_prev = find_elem('a[class="popup-prev prev"]')
                debugger('Поиск кнопки переключения фото back TRUE')
                steps_list('Найти кнопку переключения фотографий назад')
                allure_step_border('Кнопка переключения фото назад найдена', 'popup-prev prev', '0')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Поиск кнопки переключения фото back FALSE')
                result_list.append(False)
                allure_step('Кнопка переключения фото назад НЕ найдена')
            try:
                button_prev.click()
                debugger('Кликабельность кнопки переключения фото back TRUE')
                steps_list('Клик в кнопку переключения фотографий назад')
                allure_step('Кнопка переключения фото назад кликабельна')
                result_list.append(True)
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки переключения фото back False')
                allure_step('Кнопка переключения фото назад НЕ кликабельна')
                result_list.append(False)
            sleep(0.3)
            try:
                idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Получить индекс нового активного фото TRUE')
                result_list.append(True)
                steps_list('Получить индекс нового активного фото(back)')
            except NoSuchElementException:
                debugger('Получить индекс нового активного фото FALSE')
                result_list.append(False)
            steps_list('ОР: Фотографии переключаются')
            result_list.append(idx_photo(new_idx, idx))
            #Поиск миниатюр при открытых фото
            try:
                previews = find_elems('div[class="popup_preview swiper swiper-initialized swiper-vertical swiper-pointer-events swiper-backface-hidden swiper-thumbs"] a')
                steps_list('Найти миниатюры фотографий')
                allure_step_border('Миниатюры есть на странице',
                                   'popup_preview swiper swiper-initialized swiper-vertical swiper-pointer-events swiper-backface-hidden swiper-thumbs',
                                   '0')
                debugger('Поиск миниатюр TRUE')
                result_list.append(True)
            except NoSuchElementException:
                result_list.append(False)
                debugger('Поиск миниатюр FALSE')
                allure_step('Миниатюр нет на странице')
            # Переключение фотографий кликом по миниатюрам
            try:
                first_preview = previews[0]
                second_preview = previews[1]
                idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                allure_step('Получить индекс активного фото')
                second_preview.click()
                sleep(0.3)
                new_idx = find_elem('div[class="slide swiper-slide slider-track-item swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Кликабельность миниатюры TRUE')
                steps_list('Клик по второй миниатюре')
                allure_step('Кликнуть по второй миниатюре')
                result_list.append(True)
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки переключения фото back False')
                allure_step('Кнопка переключения фото назад НЕ кликабельна')
                result_list.append(False)
            result_list.append(idx_photo(new_idx, idx))
            #Закрытие фото
            try:
                close_photos = find_elems('a[class="close"]')
                close_photo = close_photos[1]
                debugger('Поиск кнопки закрытия фотографий TRUE')
                result_list.append(True)
                steps_list('Найти кнопку закрытия фотографий')
                allure_step_border('Кнопка закрытия фотографий найдена',
                                   'close',
                                   '1')
            except NoSuchElementException:
                debugger('Поиск кнопки закрытия фотографий FALSE')
                result_list.append(False)
            try:
                close_photo.click()
                debugger('Кликабельность кнопки закрытия фотографий TRUE')
                result_list.append(True)
                steps_list('Кликнуть в кнопку закрытия фотографий')
                allure_step('Кнопка закрытия фотографий кликабельна')
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки закрытия фотографий FALSE')
                result_list.append(False)
                allure_step('Кнопка закрытия фотографий НЕ кликабельна')
            result_list.append(open_close_photo('close_photo', photo_popup))
        finally:                
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Вкладки описания товара')
    def test_j(self):
        # Кнопка все характеристики
        result_list = []
        try:    
            try:
                all_characteristics = find_elem('a[class="js-scrollto all_characteristics"]')
                debugger('Поиск кнопки "Все характеристики" TRUE')
                allure_step_border('Кнопка "Все характеристики" найдена',
                                   'js-scrollto all_characteristics',
                                   '0')
                steps_list('Найти кнопку все характеристики')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Поиск кнопки "Все характеристики" FALSE')
                result_list.append(False)
                allure_step('Кнопка "Все характеристики" НЕ найдена')
            try:
                all_characteristics.click()
                debugger('Кликабельность кнопки "Все характеристики" TRUE')
                allure_step('Кнопка все характеристики кликабельна')
                result_list.append(True)
                steps_list('Кликнуть в кнопку "Все характеристики"')
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки "Все характеристики" FALSE')
                result_list.append(False)
                allure_step('Кнопка "Все характеристики" НЕ кликабельна')
            try:
                find_elem('div[class="p_product_params js-btns js-params active"]')
                debugger('Вкладка характеристики не открыта')
                allure_step_border('Вкладка характеристики активна', 
                                'p_product_params js-btns js-params active', 
                                '0')
                steps_list('ОР: Открылась вкладка "Характеристики" под блоком фото')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Вкладка характеристики не открыта')
                result_list.append(False)
                allure_step('Вкладка характеристики не активна')
            
            tabs(result_list, 
                 'О товаре', 
                 'div[class="dcol-0 p_product_info--item_wrapper js-about js-btns"]', 
                 'dcol-0 p_product_info--item_wrapper js-about js-btns', 
                 'div[class="p_product_about js-btns js-about text active"]', 
                 'p_product_about js-btns js-about text active')
            tabs(result_list,
                 'Характеристики',
                 'div[class="dcol-0 p_product_info--item_wrapper js-params js-btns"]',
                 'dcol-0 p_product_info--item_wrapper js-params js-btns',
                 'div[class="p_product_params js-btns js-params active"]',
                 'p_product_params js-btns js-params active')
            tabs(result_list, 
                 'Отзывы', 
                 'div[class="dcol-0 p_product_info--item_wrapper js-reviews js-btns"]', 
                 'dcol-0 p_product_info--item_wrapper js-reviews js-btns', 
                 'div[class="p_product_reviews js-btns js-reviews text active"]', 
                 'p_product_reviews js-btns js-reviews text active')
            tabs(result_list, 
                 "Вопрос и ответ", 
                 'div[class="dcol-0 p_product_info--item_wrapper js-faq js-btns"]', 
                 'dcol-0 p_product_info--item_wrapper js-faq js-btns', 
                 'div[class="p_product_faq js-btns js-faq js-questions-and-answers text active initialized"]', 
                 'p_product_faq js-btns js-faq js-questions-and-answers text active initialized')
            # !!!!!!!!!!!!!!!ТАБ ДОКУМЕНТАЦИЯ!!!!!!!!!!!! НАЙТИ ТОВАР С НАЛИЧИЕМ ДОКУМЕНТАЦИИ!!!!!!!!!!!!!!!!
            # try:
            #     about_product = find_elem('div[class="dcol-0 p_product_info--item_wrapper js-docs js-btns"]')
            #     steps_list(f'Найти кнопку "Документация"')
            #     allure_step_border(f'Кнопка "Документация" найдена',
            #                         f'dcol-0 p_product_info--item_wrapper js-docs js-btns',
            #                         '0')
            #     debugger(f'Поиск кнопк "Документация" TRUE')
            #     result_list.append(True)
            # except NoSuchElementException:
            #     debugger(f'Поиск кнопк "Документация" FALSE')
            #     allure_step(f'Кнопа "Документация" НЕ найдена')
            #     result_list.append(False)
            # try:
            #     about_product.click()
            #     debugger(f'Кликабельность кнопки "Документация" TRUE')
            #     allure_step(f'Кнопка "Документация" кликабельна')
            #     result_list.append(True)
            #     steps_list(f'Кликнуть в кнопку "Документация"')
            # except ElementClickInterceptedException:
            #     debugger(f'Кликабельность кнопки "Документация" FALSE')
            #     result_list.append(False)
            #     allure_step(f'Кнопка "Документация" НЕ кликабельна')
        finally:                
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Сопутствующие товары')
    def test_k(self):
        try:
            result_list = []
            product_slider = no_such_element(result_list,
                            'div[class="swiper p_product_swiper--swiper js-product-swiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden"]',
                            'swiper p_product_swiper--swiper js-product-swiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden',
                            '0',
                            'Поиск блока "Сопутствующие товары"',
                            'Найти блок "Сопутствующие товары"',
                            False)
            action.move_to_element_with_offset(product_slider, -100, -100).perform()
            product_card = no_such_element(result_list, 
                            'div[class="p_product_swiper--item swiper-slide swiper-slide-active"]',
                            'p_product_swiper--item swiper-slide swiper-slide-active',
                            '0',
                            'Поиск мини карточки товара',
                            'Найти мини карточку товара',
                            False)
            old_product_card = product_card.get_attribute('aria-label')
            add_cart = no_such_element(result_list, 
                            'button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]',
                            'dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart',
                            '0',
                            'Поиск кнопки добавить в корзину',
                            'Найти кнопку добавить в корзину',
                            False)
            result_list.append(click_intercepted(add_cart,
                                'Кникабельность внопки "Добавить в корзину"',
                                'Клик в кнопку добавить в корзину'))
            sleep(0.8)
            input_block = no_such_element(result_list, 
                            'div[class="dc-row -sm f-nowrap m-0"] div[class="dcol-0"]',
                            'dc-row -sm f-nowrap m-0',
                            '0',
                            'Открытие поля корзины',
                            'ОР: Открылось поле корзины',
                            False)
            result_list.append(displayed(input_block,
                                         'Открытие коля корзины',
                                         'ОР: Поле корзины открылось'))
            old_count_value = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"] button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]').get_attribute('data-quantity')
            plus = no_such_element(result_list,
                            'div[class="dc-row -sm f-nowrap m-0"] button[class="btn_reset product_card_qty--btn js_plus"]',
                            'dc-row -sm f-nowrap m-0',
                            '0',
                            'Поиск кнопки "+"',
                            'Найти кнопку "+"',
                            False)
            result_list.append(click_intercepted(plus,
                                                 'Кликабельность кнопки "+"',
                                                 'Кликнуть в кнопку "+"'))
            sleep(0.4)
            try:
                new_count_value = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"] button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('p_product_shoping--wrapper',
                                           '0',
                                           old_count_value,
                                           new_count_value,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'plus'))
            minus = no_such_element(result_list,
                            'div[class="dc-row -sm f-nowrap m-0"]  button[class="btn_reset product_card_qty--btn js_minus"]',
                            'dc-row -sm f-nowrap m-0',
                            '0',
                            'Поиск кнопки "-"',
                            'Найти нопку "-"',
                            False)
            result_list.append(click_intercepted(minus,
                              'Кликабельность кнопк "-"',
                              'Кликнуть в кнопку "-"'))
            sleep(0.5)
            try:
                count_value_minus = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"] button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('dc-row -sm f-nowrap m-0',
                                           '0',
                                           new_count_value,
                                           count_value_minus,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'minus'))
            result_list.append(click_intercepted(minus,
                              'Кликабельность кнопк "-"',
                              'Кликнуть в кнопку "-"'))
            result_list.append(displayed(add_cart,
                                         'Поле корзины закрывается кликом в кнопку "-"',
                                         'ОР: Поле корзины закрылось'))
            result_list.append(click_intercepted(add_cart,
                                                 'Кликабельность кнопки "Добавить в корзину',
                                                 'Кликнуть в кнопку "Добавить в корзину"'))
            sleep(0.8)
            result_list.append(displayed(input_block,
                                         'Открытие коля корзины',
                                         'ОР: Поле корзины открылось'))
            entry_field = find_elem('div[class="dc-row -sm f-nowrap m-0"] div[class="dcol-0"] input[class="product_card_qty--input"]')
            delete_value(entry_field, 
                         '1')
            result_list.append(send_text(entry_field,
                                         "2",
                                         'Ввести в поле "2"'))
            delete_value(entry_field, 
                         '1')
            result_list.append(send_text(entry_field,
                                         "0",
                                         'Ввести в поле "0"'))
            sleep(0.3)
            withot_focus = find_elem('div[class="h1 p_product--title"]')
            withot_focus.click()
            no_such_element(result_list, 
                            'button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]',
                            'dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart',
                            '0',
                            'Закрытие поля корзины',
                            'Найти кнопку добавить в корзину',
                            False)
            result_list.append(displayed(add_cart,
                                         'Поле корзины закрывается при вводе в поле "0"',
                                         'ОР: Поле корзины закрыто'))
            button_next = elems_displayed(result_list, 
                                  'div[class="footer-swiper-next dc-btn cblue -icon"]',
                                  'footer-swiper-next dc-btn cblue -icon',
                                  'Поиск кнопки переключения слайдов вперед',
                                  'Найти кнопку переключения слайдов в блоке')
            result_list.append(click_intercepted(button_next,
                                          'Кликабельность кнопки переключения вперед',
                                          "Кликнуть по кнопке переключения слайдов вперед"))
            sleep(0.8)
            new_product_card = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"]').get_attribute('aria-label')
            comparison(old_product_card,
                       new_product_card,
                       'Переключение слайдов после клика в кнопку',
                       False)
            button_back = elems_displayed(result_list, 
                                        'div[class="footer-swiper-prev dc-btn cblue -icon"]',
                                        'footer-swiper-prev dc-btn cblue -icon',
                                        'Поиск кнопки переключения слайдов назад',
                                        'Найти кнопку переключения слайдов в блоке')
            result_list.append(click_intercepted(button_back,
                                          'Кликабельность кнопки переключения назад',
                                          "Кликнуть по кнопке переключения слайдов назад"))
            sleep(0.8)
            finally_product_card = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"]').get_attribute('aria-label')
            comparison(new_product_card,
                       finally_product_card,
                       'Переключение слайдов после клика в кнопку',
                       False)
        finally:                
            steps_case()
            debug_case()
            assert result(result_list) == True

    def test_zzzz(self):
        browser.quit()

if __name__ == "__main__":
    unittest.main()
