import random
import re
import sys

import unittest
import time
import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotInteractableException # Взаимодействие(ввод текста...)
from selenium.common.exceptions import NoSuchElementException # Поиск
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementClickInterceptedException # Кликабельность
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from random import randint
import datetime
import allure
from allure_commons.types import AttachmentType
import requests
import json
from selenium.webdriver.common.keys import Keys
# import undetected_chromedriver as uc
from selenium.common.exceptions import JavascriptException

UA = 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36'
mobileEmulation = {"deviceMetrics": {"width": 412, "height": 914, "pixelRatio": 2.6}, "userAgent": UA}

options = webdriver.ChromeOptions()
#options.add_experimental_option('mobileEmulation', mobileEmulation)
options.add_argument('log-level=3')
options.add_argument("user-agent=YandexDirect")
# options.add_argument("--start-maximized")
# options.add_argument("--headless=new")
browser = webdriver.Chrome(chrome_options=options)
# browser = uc.Chrome()
WebDriverWait(browser, 5)
#browser.implicitly_wait(10)
browser.set_window_position(0, 0)
browser.set_window_size(1920, 1080)
action = ActionChains(browser)
debug = False

# 89201242855



datetime.datetime.utcnow()
date = datetime.datetime(2015, 2, 18, 4, 53, 28)
link = "https://stroylandiya.ru/"
#scroll_top = action.scroll(0, 0, 0, -2000).perform()

#browser.get(link)



def find_elem(selector):
    return(browser.find_element(By.CSS_SELECTOR, f'{selector}'))

def find_elems(selector):
    return(browser.find_elements(By.CSS_SELECTOR, f'{selector}'))

steps = []
debug_list = []

def steps_list(step_msg):
    step = f'{step_msg}'
    steps.append(step)

def steps_case():
    for i in steps:
        steps_str = ('\n'.join(steps))
    with allure.step('steps'):
        allure.attach(steps_str, name = 'steps', attachment_type=AttachmentType.TEXT)
    steps.clear()

def debugger(debug_msg):
    if debug == True:
        debug_list.append(debug_msg)

def debug_case():
    if debug == True:
        for i in debug_list:
            debug_str = ('\n'.join(debug_list))
        with allure.step('debug'):
            allure.attach(debug_str, name = 'debug', attachment_type=AttachmentType.TEXT)
        debug_list.clear()
#document.querySelector("#swiper-wrapper-fc5e87843561cf1a > div.p_product_swiper--item.swiper-slide.swiper-slide-active > article > div.product_card_shoping.js-product_card_shoping.js-product-in-cart > div.dc-row.-sm.f-nowrap.m-0 > div:nth-child(2) > div > div:nth-child(3) > button")

def allure_step_border(name_step, border, index):
    try:
        browser.execute_script(f'document.getElementsByClassName("{border}")[{index}].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
    except JavascriptException:
        pass
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
    try:
        browser.execute_script(f"document.getElementsByClassName('{border}')[{index}].style.border=\"0px\";")
    except JavascriptException:
        pass
    
def allure_step(name_step):
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)

def result(result_list):
    for r in result_list:
        if r == False:
            test_result = False
            break
        else:
            test_result = True
    return(test_result)


def city_def():
        try:
            popup = find_elem('#city_locate')
        except NoSuchElementException:
            allure_step('В DOM нет попапа подтверждения города')
            return
        try:
            if popup.is_displayed() == True:
                find_elem('a[class="cities-locate__button cities-locate__button-ok js-cities-locate-ok"]').click()
                time.sleep(1)
        except NoSuchElementException:
            pass
        city = find_elem('a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        if city != "Оренбург":
            find_elem('div[class="city-choose-wrapper"]').click()
            find_elem('button[class="cities__results-item js-city-btn"][data-city="Оренбург"]').click()
            time.sleep(1)

def move_to(element):
    action.move_to_element(element).perform()

def idx_photo(idx, new_idx):
    if idx != new_idx:
        result = True
        debugger('Индексы разные TRUE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} TRUE')
    else:
        result = False
        debugger('Индексы одинаковые FALSE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} FALSE')
    return(result)

def open_close_photo(asd, photo_popup):
    if asd == "open_photo":
        if photo_popup.is_displayed() == True:
            allure_step_border('Фотографии открылись на весь экран',
                                'popup_wrapper',
                                '0')
            result = True
            debugger('открытие фотографий на весьэкран TRUE')
            steps_list('ОР: фотография развернулась на весь экран')
        else:
            allure_step('Фотографии не открылись на весь экран')
            result = False
            debugger('открытие фотографий на весьэкран FALSE')
    elif asd == "close_photo":
        if photo_popup.is_displayed() == False:
            allure_step('Фотографии закрылись')
            result = True
            debugger('Закрытие фоторгафий TRUE')
            steps_list('ОР: Фотографии закрылись')
        else:
            allure_step('Фотографии не закрылись')
            result = False
            debugger('Закрытие фоторгафий FALSE')
    return(result)


def tabs(result_list, name, button, border, block, block_border):
    try:
        tab = find_elem(f'{button}')
        move_to(tab)
        steps_list(f'Найти кнопку "{name}"')
        allure_step_border(f'Кнопка "{name}" найдена',
                            f'{border}',
                            '0')
        debugger(f'Поиск кнопк "{name}" TRUE')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Поиск кнопк "{name}" FALSE')
        allure_step(f'Кнопа "{name}" НЕ найдена')
        result_list.append(False)
    try:
        tab.click()
        time.sleep(1)
        debugger(f'Кликабельность кнопки "{name}" TRUE')
        allure_step(f'Кнопка "{name}" кликабельна')
        result_list.append(True)
        steps_list(f'Кликнуть в кнопку "{name}"')
    except ElementClickInterceptedException:
        debugger(f'Кликабельность кнопки "{name}" FALSE')
        result_list.append(False)
        allure_step(f'Кнопка "{name}" НЕ кликабельна')
    try:
        tab_block = find_elem(f'{block}')
        move_to(tab_block)
        time.sleep(0.4)
        debugger(f'Вкладка {name} открыта')
        allure_step_border(f'Вкладка "{name}" активна', 
                        f'{block_border}', 
                        '0')
        steps_list(f'ОР: Открылась вкладка "{name}" под блоком фото')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Вкладка {name} не открыта')
        result_list.append(False)
        allure_step(f'{name} не активна')


def no_such_element(result_list, css_sel, border, idx, msg, step, res):
    try:
        elem = find_elem(f'{css_sel}')
        try:
            move_to(elem)
        except:
            pass
        time.sleep(0.3)
        debugger(f'{msg} TRUE')
        allure_step_border(f'{msg} TRUE',
                            f'{border}',
                            f'{idx}')
        steps_list(f'{step}')
        result_list.append(True)
        return(find_elem(f'{css_sel}'))
    except NoSuchElementException:
        if res == True:
            result_list.append(True)
            allure_step(f'{msg} TRUE')
            debugger(f'{msg} TRUE')
            steps_list(f'{step}')
        else:
            result_list.append(False)
            allure_step(f'{msg} FALSE')
            debugger(f'{msg} FALSE')
        return(None)
    
    

def click_intercepted(elem, msg, step):
    try:
        elem.click()
        time.sleep(0.5)
        debugger(f'{msg} TRUE')
        allure_step(f'{msg} TRUE')
        result = True
        steps_list(f'{step}')
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == True:
        result = True
        debugger(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def not_displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == False:
        debugger(f'{msg} TRUE')
        result = True
        allure_step(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
    return(result)

def elements(css_sel, idx):
    elems = find_elems(f'{css_sel}')
    if idx == 'random':
        elem = elems[randint(0, len(elems) -1)]
    else:
        elem = elems[idx]
    return(elem)

def send_text(elem, text, msg):
    try:
        elem.send_keys(f'{text}')
        debugger(f'{msg} TRUE')
        allure_step(f'В поле ввода можно ввести {text} True')
        steps_list(f'Ввести в поле "{text}"')
        result = True
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        allure_step(f'В поле ввода нельзя ввести {text} False')
        result = False
    return(result)

def count_value(border, idx, old_count_value, new_count_value, msg, step, sign):
    if sign == "plus":
        if int(new_count_value) - int(old_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    else:
        if int(old_count_value) - int(new_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    return(result)
def sleep(sec):
    time.sleep(sec)

def delete_value(block_input, max_count):
    for r in range(len(max_count)+1):
        block_input.send_keys(Keys.ARROW_RIGHT)
        sleep(0.3)
    for d in range(len(max_count)+1):
        block_input.send_keys(Keys.BACKSPACE)  
        sleep(0.3)


def cityes(old_city_name, new_city_name):
    if old_city_name != new_city_name:
        result = True
        allure_step(f'Город изменился был - {old_city_name} стал - {new_city_name} TRUE') 
    else:
        result = False
        allure_step(f'Город не изменился был - {old_city_name} стал - {new_city_name} FALSE') 
    return(result)

def city_in_title(city, title_page):
    first_city_name = city.split(' ', 1)[0]
    now_city = first_city_name[:-2]
    if (re.findall(rf'{now_city}\w+', title_page, flags=re.IGNORECASE)):
        result = True
        with allure.step(f'Название города в тайтле страницы TRUE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    else:
        result = False
        with allure.step(f'Название города в тайтле страницы FALSE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    return(result)

def elems_displayed(result_list, css_sel, border, msg, step):
    try:
        elems = find_elems(f'{css_sel}')
    except NoSuchElementException:
        allure_step('На странице нет кнопок переключения блока')
        result_list.append(True)
        return
    idx = -1
    disp = 0
    for e in elems:
        idx += 1
        if e.is_displayed() == True:
            elem = e
            #move_to(elem)
            time.sleep(0.3)
            debugger(f'{msg} TRUE')
            allure_step_border(f'{msg} TRUE',
                                f'{border}',
                                f'{idx}')
            steps_list(f'{step}')
            result_list.append(True)
            break
    return(elem)
        
    
        
    # for d in elems:
    #     if d.is_displayed() == True:
    #         disp += 1
    # if disp <= 6:
    #     allure_step('На странице нет активных кнопок переключения блока')
    #     result_list.append(True)

def comparison(first, second, msg, equal):
    if equal == True:
        if first == second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    elif equal == False:
        if first != second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    return(result)

def banner_url(page, name):
    url = browser.current_url
    if page in url:
        result = True
    else: result = False
    #allure_step(f'После клика в {name}')
    return(result)

def back():
    browser.back()  

def h1_title(name):
    page_title = find_elem('#pagetitle').text
    if name in page_title:
        result = True
    else: result = False
    allure_step_border(f'h1 title страницы, после клика в {name}',
                       'fb-font-gilroy',
                       '0')
    return(result)  

def rel_bunner_page(with_city):
    if with_city == True:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_city}\w+', title, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    else:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    return(result)
