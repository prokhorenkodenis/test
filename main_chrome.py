import random
import re
import sys

import unittest
import time
import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotInteractableException # Взаимодействие(ввод текста...)
from selenium.common.exceptions import NoSuchElementException # Поиск
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementClickInterceptedException # Кликабельность
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from random import randint
import datetime
import allure
from allure_commons.types import AttachmentType
import requests
import json
from selenium.webdriver.common.keys import Keys
# import undetected_chromedriver as uc
from selenium.common.exceptions import JavascriptException

UA = 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36'
mobileEmulation = {"deviceMetrics": {"width": 412, "height": 914, "pixelRatio": 2.6}, "userAgent": UA}

link = "https://stroylandiya.ru/"
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless=new')
chrome_options.add_argument('--disable-dev-shm-usage')  

browser = webdriver.Remote (command_executor = "http://selenium__standalone-chrome:4444/wd/hub", options = chrome_options) #chrome_options = chrome_options) command_executor="http://selenium__standalone-chrome:4444"
browser.set_window_position(0, 0)
browser.set_window_size(1920, 1080)
WebDriverWait(browser, 5)
action = ActionChains(browser)
datetime.datetime.utcnow()
debug = False

# 89201242855



datetime.datetime.utcnow()
date = datetime.datetime(2015, 2, 18, 4, 53, 28)
link = "https://stroylandiya.ru/"
#scroll_top = action.scroll(0, 0, 0, -2000).perform()

#browser.get(link)



def find_elem(selector):
    return(browser.find_element(By.CSS_SELECTOR, f'{selector}'))

def find_elems(selector):
    return(browser.find_elements(By.CSS_SELECTOR, f'{selector}'))

steps = []
debug_list = []

def steps_list(step_msg):
    step = f'{step_msg}'
    steps.append(step)

def steps_case():
    for i in steps:
        steps_str = ('\n'.join(steps))
    with allure.step('steps'):
        allure.attach(steps_str, name = 'steps', attachment_type=AttachmentType.TEXT)
    steps.clear()

def debugger(debug_msg):
    if debug == True:
        debug_list.append(debug_msg)

def debug_case():
    if debug == True:
        for i in debug_list:
            debug_str = ('\n'.join(debug_list))
        with allure.step('debug'):
            allure.attach(debug_str, name = 'debug', attachment_type=AttachmentType.TEXT)
        debug_list.clear()
#document.querySelector("#swiper-wrapper-fc5e87843561cf1a > div.p_product_swiper--item.swiper-slide.swiper-slide-active > article > div.product_card_shoping.js-product_card_shoping.js-product-in-cart > div.dc-row.-sm.f-nowrap.m-0 > div:nth-child(2) > div > div:nth-child(3) > button")

def allure_step_border(name_step, border, index):
    try:
        browser.execute_script(f'document.getElementsByClassName("{border}")[{index}].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
    except JavascriptException:
        pass
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
    try:
        browser.execute_script(f"document.getElementsByClassName('{border}')[{index}].style.border=\"0px\";")
    except JavascriptException:
        pass
    
def allure_step(name_step):
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)

def result(result_list):
    for r in result_list:
        if r == False:
            test_result = False
            break
        else:
            test_result = True
    return(test_result)


def city_def():
        try:
            popup = find_elem('#city_locate')
        except NoSuchElementException:
            allure_step('В DOM нет попапа подтверждения города')
            return
        try:
            if popup.is_displayed() == True:
                find_elem('a[class="cities-locate__button cities-locate__button-ok js-cities-locate-ok"]').click()
                time.sleep(1)
        except NoSuchElementException:
            pass
        city = find_elem('a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        if city != "Оренбург":
            find_elem('div[class="city-choose-wrapper"]').click()
            find_elem('button[class="cities__results-item js-city-btn"][data-city="Оренбург"]').click()
            time.sleep(1)

def move_to(element):
    action.move_to_element(element).perform()

def idx_photo(idx, new_idx):
    if idx != new_idx:
        result = True
        debugger('Индексы разные TRUE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} TRUE')
    else:
        result = False
        debugger('Индексы одинаковые FALSE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} FALSE')
    return(result)

def open_close_photo(asd, photo_popup):
    if asd == "open_photo":
        if photo_popup.is_displayed() == True:
            allure_step_border('Фотографии открылись на весь экран',
                                'popup_wrapper',
                                '0')
            result = True
            debugger('открытие фотографий на весьэкран TRUE')
            steps_list('ОР: фотография развернулась на весь экран')
        else:
            allure_step('Фотографии не открылись на весь экран')
            result = False
            debugger('открытие фотографий на весьэкран FALSE')
    elif asd == "close_photo":
        if photo_popup.is_displayed() == False:
            allure_step('Фотографии закрылись')
            result = True
            debugger('Закрытие фоторгафий TRUE')
            steps_list('ОР: Фотографии закрылись')
        else:
            allure_step('Фотографии не закрылись')
            result = False
            debugger('Закрытие фоторгафий FALSE')
    return(result)


def tabs(result_list, name, button, border, block, block_border):
    try:
        tab = find_elem(f'{button}')
        move_to(tab)
        steps_list(f'Найти кнопку "{name}"')
        allure_step_border(f'Кнопка "{name}" найдена',
                            f'{border}',
                            '0')
        debugger(f'Поиск кнопк "{name}" TRUE')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Поиск кнопк "{name}" FALSE')
        allure_step(f'Кнопа "{name}" НЕ найдена')
        result_list.append(False)
    try:
        tab.click()
        time.sleep(1)
        debugger(f'Кликабельность кнопки "{name}" TRUE')
        allure_step(f'Кнопка "{name}" кликабельна')
        result_list.append(True)
        steps_list(f'Кликнуть в кнопку "{name}"')
    except ElementClickInterceptedException:
        debugger(f'Кликабельность кнопки "{name}" FALSE')
        result_list.append(False)
        allure_step(f'Кнопка "{name}" НЕ кликабельна')
    try:
        tab_block = find_elem(f'{block}')
        move_to(tab_block)
        time.sleep(0.4)
        debugger(f'Вкладка {name} открыта')
        allure_step_border(f'Вкладка "{name}" активна', 
                        f'{block_border}', 
                        '0')
        steps_list(f'ОР: Открылась вкладка "{name}" под блоком фото')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Вкладка {name} не открыта')
        result_list.append(False)
        allure_step(f'{name} не активна')


def no_such_element(result_list, css_sel, border, idx, msg, step, res):
    try:
        elem = find_elem(f'{css_sel}')
        try:
            move_to(elem)
        except:
            pass
        time.sleep(0.3)
        debugger(f'{msg} TRUE')
        allure_step_border(f'{msg} TRUE',
                            f'{border}',
                            f'{idx}')
        steps_list(f'{step}')
        result_list.append(True)
        return(find_elem(f'{css_sel}'))
    except NoSuchElementException:
        if res == True:
            result_list.append(True)
            allure_step(f'{msg} TRUE')
            debugger(f'{msg} TRUE')
            steps_list(f'{step}')
        else:
            result_list.append(False)
            allure_step(f'{msg} FALSE')
            debugger(f'{msg} FALSE')
        return(None)
    
    

def click_intercepted(elem, msg, step):
    try:
        elem.click()
        time.sleep(0.5)
        debugger(f'{msg} TRUE')
        allure_step(f'{msg} TRUE')
        result = True
        steps_list(f'{step}')
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == True:
        result = True
        debugger(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def not_displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == False:
        debugger(f'{msg} TRUE')
        result = True
        allure_step(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
    return(result)

def elements(css_sel, idx):
    elems = find_elems(f'{css_sel}')
    if idx == 'random':
        elem = elems[randint(0, len(elems) -1)]
    else:
        elem = elems[idx]
    return(elem)

def send_text(elem, text, msg):
    try:
        elem.send_keys(f'{text}')
        debugger(f'{msg} TRUE')
        allure_step(f'В поле ввода можно ввести {text} True')
        steps_list(f'Ввести в поле "{text}"')
        result = True
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        allure_step(f'В поле ввода нельзя ввести {text} False')
        result = False
    return(result)

def count_value(border, idx, old_count_value, new_count_value, msg, step, sign):
    if sign == "plus":
        if int(new_count_value) - int(old_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    else:
        if int(old_count_value) - int(new_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    return(result)
def sleep(sec):
    time.sleep(sec)

def delete_value(block_input, max_count):
    for r in range(len(max_count)+1):
        block_input.send_keys(Keys.ARROW_RIGHT)
        sleep(0.3)
    for d in range(len(max_count)+1):
        block_input.send_keys(Keys.BACKSPACE)  
        sleep(0.3)


def cityes(old_city_name, new_city_name):
    if old_city_name != new_city_name:
        result = True
        allure_step(f'Город изменился был - {old_city_name} стал - {new_city_name} TRUE') 
    else:
        result = False
        allure_step(f'Город не изменился был - {old_city_name} стал - {new_city_name} FALSE') 
    return(result)

def city_in_title(city, title_page):
    first_city_name = city.split(' ', 1)[0]
    now_city = first_city_name[:-2]
    if (re.findall(rf'{now_city}\w+', title_page, flags=re.IGNORECASE)):
        result = True
        with allure.step(f'Название города в тайтле страницы TRUE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    else:
        result = False
        with allure.step(f'Название города в тайтле страницы FALSE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    return(result)

def elems_displayed(result_list, css_sel, border, msg, step):
    try:
        elems = find_elems(f'{css_sel}')
    except NoSuchElementException:
        allure_step('На странице нет кнопок переключения блока')
        result_list.append(True)
        return
    idx = -1
    disp = 0
    for e in elems:
        idx += 1
        if e.is_displayed() == True:
            elem = e
            #move_to(elem)
            time.sleep(0.3)
            debugger(f'{msg} TRUE')
            allure_step_border(f'{msg} TRUE',
                                f'{border}',
                                f'{idx}')
            steps_list(f'{step}')
            result_list.append(True)
            break
    return(elem)
        
    
        
    # for d in elems:
    #     if d.is_displayed() == True:
    #         disp += 1
    # if disp <= 6:
    #     allure_step('На странице нет активных кнопок переключения блока')
    #     result_list.append(True)

def comparison(first, second, msg, equal):
    if equal == True:
        if first == second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    elif equal == False:
        if first != second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    return(result)

def banner_url(page, name):
    url = browser.current_url
    if page in url:
        result = True
    else: result = False
    #allure_step(f'После клика в {name}')
    return(result)

def back():
    browser.back()  

def h1_title(name):
    page_title = find_elem('#pagetitle').text
    if name in page_title:
        result = True
    else: result = False
    allure_step_border(f'h1 title страницы, после клика в {name}',
                       'fb-font-gilroy',
                       '0')
    return(result)  

def rel_bunner_page(with_city):
    if with_city == True:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_city}\w+', title, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    else:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    return(result)


check_list = f'https://docs.google.com/spreadsheets/d/1x77VeZxM9GvXX1O3cCMIHMf95MDv1s1MkHieoDaxlf4/edit#gid=1828904958&range='

def test_status_code():
    dat = {'q':'goog'}
    response = requests.get(link, params=dat, headers={'User-Agent': 'Mozilla/5.0'})
    return(response.status_code)



browser.get(link)
if test_status_code() != 200:
    sys.exit(f'Статус код {test_status_code()}')



def ads():
    def qwe():
        try:
            del_buttons = find_elems('div[class="cart_item text"]')
            for d in del_buttons:
                try:
                    d.click()
                    sleep(1)
                    allure_step('После клика удаления')
                except ElementClickInterceptedException:
                    browser.refresh()
                    sleep(1)
                    qwe()
        except NoSuchElementException:
            pass
    browser.get(link)
    try:
        find_elem('span[class="basket-link jlink compare header-v2-compare basket-count"]').click()
        find_elem('span[class="wrap_remove_button"]').click()
        sleep(1)
    except NoSuchElementException:
        pass
    try:
        find_elem('span[class="header-v2-favourites jlink"]').click()
        qwe()
    except NoSuchElementException: pass
    
    try:
        find_elem('span[class="jlink header-v2-basket"]').click()
        del_buttons = find_elems('a[class="basket-actions__item"]')
        for d in del_buttons:
            d.click()
            sleep(1)
    except NoSuchElementException:
        pass
    assert 1+1 == 2




class test_title(unittest.TestCase):
    def test_title(self):
        #title = browser.title
        city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        if city != "Оренбург":
            browser.find_element(By.CSS_SELECTOR, 'div[class="city-choose-wrapper"]').click()
            browser.find_element(By.CSS_SELECTOR, 'button[class="cities__results-item js-city-btn"][data-city="Оренбург"]').click()
            time.sleep(1)
        else: city = "Оренбург"
        new_city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        #result = (any(map(title.lower().__contains__, map(str.lower, city))))
        self.assertEqual(new_city, city)



@allure.feature('Главная страница')
class test_a_header_elements(unittest.TestCase):
    @allure.story('Хедер')
    @allure.title('Выбор города')
    def test_a(self): #_choosing_city    
        result_list = []
        city_def()
        try:
            city = no_such_element(result_list,
                                'div[class="city-choose-wrapper"]',
                                'city-choose-wrapper',
                                '0',
                                'Поиск кнопки выбора города',
                                'Найти кнопку выбора города',
                                False)
            old_city_name = city.text
            title_page = browser.title
            result_list.append(city_in_title(old_city_name,
                          title_page))
            result_list.append(displayed(city,
                                        "Отображение кнопки города на странице",
                                        'ОР: Кнопка город отображается на странице'))
            result_list.append(click_intercepted(city,
                                                'Кликабельность кнопки выбора города',
                                                'Кликнуть в кнопку выбора города'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="popup__body cities__body"]',
                                    'popup__body cities__body',
                                    '0',
                                    'Открытие попапа со списком городов',
                                    'ОР: Попап открылся',
                                    False)
            rnd_city = elements('button[class="cities__results-item js-city-btn"]',
                                'random')
            move_to(rnd_city)
            city_name = rnd_city.text
            if city_name == 'Оренбург':
                rnd_city = elements('button[class="cities__results-item js-city-btn"]',
                                'random')
                move_to(rnd_city)
                city_name = rnd_city.text
            result_list.append(click_intercepted(rnd_city,
                                                f'Кликабельность города {city_name} в списке',
                                                f'Кликнуть в город {city_name}'))
            sleep(1)
            new_city = no_such_element(result_list,
                                'div[class="city-choose-wrapper"]',
                                'city-choose-wrapper',
                                '0',
                                'Поиск кнопки выбора города',
                                'Найти кнопку выбора города',
                                False)
            new_city_name = new_city.text
            result_list.append(cityes(new_city_name,
                                       old_city_name))
            new_title_page = browser.title
            result_list.append(city_in_title(new_city_name,
                                            new_title_page))
            find_elem('div[class="city-choose-wrapper"]').click()
            sleep(0.5)
            form_input = no_such_element(result_list,
                                        'input[class="cities__form-input js-search-city"]',
                                        'cities__form-input js-search-city',
                                        '0',
                                        'Наличие поля ввода названия города',
                                        'Найти поле ввода названия города',
                                        False)
            result_list.append(click_intercepted(form_input,
                                                'Кликабельность поля ввода названия города',
                                                'Кликнуть в поле поиска города'))
            result_list.append(send_text(form_input,
                                        old_city_name,
                                        f'Ввести в поле {old_city_name}'))
            sleep(2)
            search_city =  no_such_element(result_list, 
                            'div[class="cities__results js-cities-list"] button[class="cities__results-item js-city-btn"]',
                            'cities__results js-cities-list',
                            '0',
                            'Есть результаты поиска города',
                            f'ОР: В результаттах поиска есть {old_city_name}',
                            False)
            result_list.append(click_intercepted(search_city,
                                                'Кликабельность результатов поиска города',
                                                'Кликнуть в первый результат поиска'))
            sleep(0.5)
            result_list.append(cityes(new_city_name,
                                       old_city_name))
            finally_page_title = browser.title
            result_list.append(city_in_title(old_city_name, 
                                             finally_page_title))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Банер над хедером')
    def test_b(self):
        result_list = []
        city_def()
        try:
            bunner = no_such_element(result_list,
                            'div[class="banner CROP TOP_HEADER hidden-sm hidden-xs"]',
                            'banner CROP TOP_HEADER hidden-sm hidden-xs',
                            '0',
                            'Поиск баннера над хедером',
                            'Найти баннер над хедером',
                            False)
            result_list.append(displayed(bunner,
                                        'Баннер отображается на странице',
                                        'ОР: Баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                'Кликабельность баннера над хедером',
                                                'Кликнуть в баннер над хедером'))
            sleep(1)
            result_list.append(banner_url('action',
                                          'Баннер над хедером'))
            back()
            sleep(1)
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Магазины')
    def test_c(self):
        result_list= []
        try:
            shops = no_such_element(result_list,
                                    'a[class="shop_link"]',
                                    "shop_link",
                                    '0',
                                    'Поиск кнопки "Магазины"',
                                    'Найти кнопку "Магазины"',
                                    False)
            result_list.append(click_intercepted(shops,
                                                'Кликабельность кнопки "Магазины"',
                                                'Кликнуть в кнопку "Магазины"'))
            sleep(1)
            result_list.append(banner_url('shops',
                                          'Магазины'))
            back()
            sleep(1)
        
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Статус заказа')
    def test_d(self):
        result_list = []
        try:
            status = no_such_element(result_list,
                                    'a[class="button-header"]',
                                    'button-header',
                                    '0',
                                    'Поиск кнопки "Статус заказа"',
                                    'Найти кнопку статус заказа',
                                    False)
            result_list.append(click_intercepted(status,
                                                'Кликабельность кнопки "Статус заказа"',
                                                'Кликнуть в кнопку "Статус заказа"'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Поиск попапа авторизации',
                                    'Найти попап авторизации',
                                    False)
            result_list.append(displayed(popup,
                                        'Отображение попапа авторизации на странице',
                                        'ОР: Попап авторизации виден пользователю'))
            close_button = no_such_element(result_list,
                                           'div[class="auth-popup auth-popup-mobile fancybox-content"] a[class="popup__close popup__close-x close-popup"]',
                                           "popup__close popup__close-x close-popup", 
                                           '1',
                                           'Поиск кнопки закрытия попапа',
                                           'Найти кнопку закрытия попапа',
                                           False)
            result_list.append(displayed(close_button,
                                        'Отображение кнопки закрытия попапа на странице',
                                        'ОР: Кнопка закрытия попапа видна пользователю'))
            result_list.append(click_intercepted(close_button,
                                                'Кликабельность кнопки закрытия попапа',
                                                'Кликнуть в кнопку закрытия попапа'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Закрытие попапа авторизации',
                                    'ОР: Попап авторизации не виден пользователю',
                                    True) 
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Номер телефона')
    def test_e(self):
        result_list = []
        try:
            phone = no_such_element(result_list,
                                    'a[class="header-phone"]',
                                    'header-phone',
                                    '0',
                                    'Поиск номера телефона',
                                    'Найти номер телефона',
                                    False)
            result_list.append(displayed(phone,
                                         'Отображение номера телефона на странице',
                                         'ОР: Номер телефона виден пользователю'))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    


    @allure.story('Хедер')
    @allure.title('Логотип')
    def test_f(self):
        result_list = []
        city_def()
        try:
            logo = find_elem('a[class="header-v2-logo"]')
            debug_msg = 'Поиск логотипа TRUE'
            steps_list('Найти логотип на странице')
            allure_step_border('Логотип найден', 'header-v2-logo', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'поиск логотипа FALSE'
            allure_step('Логотип не найден')
            result_list.append(False)
        debugger(debug_msg)
        try:
            logo.click()
            debug_msg = 'Клик в логотип TRUE'
            allure_step('Логотип кликабелен')
            result_list.append(True)
            steps_list("Кликнуть в логотип")
        except ElementClickInterceptedException:
            debug_msg = 'Клик в логотип FALSE'
            allure_step('Логотип не Кликабелен')
            result_list.append(False)
        debugger(debug_msg)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Проверка каталога')
    def test_g(self):
        result_list = []
        menu = no_such_element(result_list,
                'div[class="menu-only"]',
                'menu-only',
                '0',
                'Поиск кнопки "Каталог"',
                'Найти кнопку "Каталог"',
                False)
        result_list.append(click_intercepted(menu,
                           'Кликабельность кнопки "Каталог"', 
                           'Кликнуть в кнопку "Каталог"'))
        sleep(0.3)
        catalog_block = no_such_element(result_list,
                        'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]',
                        'fb-header-catalog-menu fb-header-catalog-menu_opened',
                        '0',
                        'Открытие каталога',
                        'ОР: Каталог открылся',
                        False)
        close_menu = no_such_element(result_list,
                        'div[class="menu-only"]',
                        'menu-only',
                        '0',
                        'Поиск кнопки закрытия каталога',
                        'Найти кнопку закрытия каталога',
                        False)
        result_list.append(click_intercepted(close_menu,
                            'Кликабельность кнопки закрытия каталога', 
                            'Кликнуть в кнопку закрытия каталога'))
        sleep(0.5)
        no_such_element(result_list,
                        'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]',
                        'fb-header-catalog-menu fb-header-catalog-menu_opened',
                        '0',
                        'Закрытие каталога',
                        'ОР: Каталог закрылся',
                        True)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Проверка поиска')
    def test_h(self):
        result_list = []
        search = no_such_element(result_list,
                        'input[class="search-input js-digi-search-input"]',
                        'search-input js-digi-search-input',
                        '0',
                        'Поиск блока "Поиска"',
                        'Найти блок поиска',
                        False)
        result_list.append(click_intercepted(search,
                          'Кликабельность поля поиска',
                          'Кликнуть в поле поиска'))
        search_results = no_such_element(result_list,
                        'div[class="search-results__wrapper"]',
                        'search-results__wrapper',
                        '0',
                        'Поиск блока результатов поиска',
                        'Найти блок поиска',
                        False)
        result_list.append(displayed(search_results,
                                     "Отображение блока поиска на странице",
                                     'Проверить отображение блока поиска на странице'))
        result_list.append(send_text(search,
                                     'Ламинат',
                                     'Ввести текст в поле поиска'))
        sleep(0.5)
        popular_searches = no_such_element(result_list,
                        'div[class="search-results__search-container search-results__popular-searches js-popular-searches"]',
                        'search-results__search-container search-results__popular-searches js-popular-searches',
                        '0',
                        'Поиск блока "Часто ищу"',
                        'Найти блок "Часто ищу"',
                        False)
        result_list.append(displayed(popular_searches,
                                     'Отображение блока "Часто ищу" на странице',
                                     'ОР: Блок "Часто ищу" отображается на странице'))
        search_categories = no_such_element(result_list,
                        'div[class="search-results__search-container search-results__categories js-search-categories"]',
                        'search-results__search-container search-results__categories js-search-categories',
                        '0',
                        'Поиск блока "Категории"',
                        'Найти блок "Категории"',
                        False)
        result_list.append(displayed(search_categories,
                                     'Отображение блока "Категории" на странице',
                                     'ОР: Блок "Категории" отображается на странице'))
        search_products = no_such_element(result_list,
                        'div[class="search-results__items-container js-search-products"]',
                        'search-results__items-container js-search-products',
                        '0',
                        'Поиск блока "Популярные товары"',
                        'Найти блок "Популярные товары"',
                        False)
        result_list.append(displayed(search_products,
                                     'Отображение блока "Популярные товары" на странице',
                                     'ОР: Блок "Популярные товары" отображается на странице'))
        search_submit_btn = no_such_element(result_list,
                        'button[class="search-results__all-results-btn js-search-submit-btn"]',
                        'search-results__all-results-btn js-search-submit-btn',
                        '0',
                        'Поиск кнопки "Все результаты"',
                        'Найти кнопку "Все результаты"',
                        False)
        result_list.append(displayed(search_submit_btn,
                                     'Отображение кнопки "Все результаты" на странице',
                                     'ОР: Кнопка "Все результаты" отображается на странице'))
        search_btn_desktop = no_such_element(result_list,
                        'button[class="search-input-digi-btn js-digi-search-btn-desktop"]',
                        'search-input-digi-btn js-digi-search-btn-desktop',
                        '0',
                        'Поиск кнопки "Найти"',
                        'Найти кнопку "Найти"',
                        False)
        result_list.append(displayed(search_btn_desktop,
                                     'Отображение кнопки "Найти" на странице',
                                     'ОР: Кнопка "Найти" отображается на странице'))
        reset_btn_desktop = no_such_element(result_list,
                        'button[class="search-input-digi-reset-btn js-digi-reset-btn-desktop"]',
                        'search-input-digi-reset-btn js-digi-reset-btn-desktop',
                        '0',
                        'Поиск кнопки удаления текста',
                        'Найти кнопку удаления текста',
                        False)
        result_list.append(displayed(reset_btn_desktop,
                                     'Отображение кнопки удаления текста на странице',
                                     'ОР: Кнопка удаления текста отображается на странице'))
        result_list.append(click_intercepted(reset_btn_desktop,
                          'Кликабельность кнопки удаления текста',
                          'Кликнуть в кнопку удаления текста'))
        try:
            header_grey = find_elem('header[class="header -grey"]').click()
        except:
            allure_step('не удалось кликнуть в хедер для снятия фикуса с поиска')
        result_list.append(not_displayed(search_results,
                                         'Закрытие окна поиска',
                                         'ОР: Окно результатов поиска закрыто'))
        steps_case()
        debug_case()
        assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Акции')
    def test_j(self):
        result_list = []
        try:
            stock = no_such_element(result_list,
                                    'a[class="header-v2-action"]',
                                    'header-v2-action',
                                    '0',
                                    'Поиск кнопки "Акции"',
                                    'Найти кнопку "Акции"',
                                    False)
            result_list.append(displayed(stock,
                                        'Отображении кнопки "Акции" на странице',
                                        'ОР: Кнопка "Акции" Видна пользователю'))
            result_list.append(click_intercepted(stock,
                            'Кликабельность кнопки "Акции"',
                            'Кликнуть в кнопку "Акции"'))
            sleep(0.5)
            page_title = browser.title
            result_list.append(banner_url('actions',
                                        'Акции'))
            result_list.append(h1_title('Акции'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
        

    @allure.story('Хедер')
    @allure.title('Вход')
    def test_k(self):
        result_list = []
        try:
            entrance = no_such_element(result_list,
                                    'a[class="header-v2-enter "]',
                                    'header-v2-enter ',
                                    '0',
                                    'Поиск кнопки "Вход"',
                                    'Найти кнопку "Вход"',
                                    False)
            result_list.append(click_intercepted(entrance,
                                                'Кликабельность кнопки "Вход"',
                                                'Кликнуть в кнопку "Вход"'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Поиск попапа авторизации',
                                    'Найти попап авторизации',
                                    False)
            result_list.append(displayed(popup,
                                        'Отображение попапа авторизации на странице',
                                        'ОР: Попап авторизации виден пользователю'))
            close_button = no_such_element(result_list,
                                           'div[class="auth-popup auth-popup-mobile fancybox-content"] a[class="popup__close popup__close-x close-popup"]',
                                           "popup__close popup__close-x close-popup",
                                           '2',
                                           'Поиск кнопки закрытия попапа',
                                           'Найти кнопку закрытия попапа',
                                           False)
            result_list.append(displayed(close_button,
                                        'Отображение кнопки закрытия попапа на странице',
                                        'ОР: Кнопка закрытия попапа видна пользователю'))
            result_list.append(click_intercepted(close_button,
                                                'Кликабельность кнопки закрытия попапа',
                                                'Кликнуть в кнопку закрытия попапа'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Закрытие попапа авторизации',
                                    'ОР: Попап авторизации не виден пользователю',
                                    True) 
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Сравнение')
    def test_l(self):
        result_list = []
        try:
            comparison_button = no_such_element(result_list,
                                                'span[class="basket-link jlink compare header-v2-compare "]',
                                                "basket-link compare header-v2-compare ",
                                                '0',
                                                'Поиск кнопки "Сравнение"',
                                                'Найти кнопку сравнение',
                                                False)
            result_list.append(displayed(comparison_button,
                                        'Отображение кнопки "Сравнение" на странице',
                                        'ОР: Кнопка "Сравнение" видна пользователю'))
            result_list.append(click_intercepted(comparison_button,
                                                'Кликабельность кнопки "Сравнение"',
                                                'Кликнуть в кнопку "Сравнение"'))
            result_list.append(banner_url('compare',
                                        'Сравнение'))
            result_list.append(h1_title('Сравнение товаров'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Избранное')
    def test_m(self):
        result_list = []
        try:
            favorite = no_such_element(result_list,
                                                'span[class="header-v2-favourites jlink"]',
                                                "header-v2-favourites",
                                                '0',
                                                'Поиск кнопки "Избранное"',
                                                'Найти кнопку "Избранное"',
                                                False)
            result_list.append(displayed(favorite,
                                        'Отображение кнопки "Избранное" на странице',
                                        'ОР: Кнопка "Избранное" видна пользователю'))
            result_list.append(click_intercepted(favorite,
                                                'Кликабельность кнопки "Избранное"',
                                                'Кликнуть в кнопку "Избранное"'))
            result_list.append(h1_title('Отложенные товары'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
 
    @allure.story('Хедер')
    @allure.title('Корзина')
    def test_n(self):
        result_list = []
        try:
            basket = no_such_element(result_list,
                                                'span[class="jlink header-v2-basket"]',
                                                "header-v2-basket",
                                                '0',
                                                'Поиск кнопки "Корзина"',
                                                'Найти кнопку "Корзина"',
                                                False)
            result_list.append(displayed(basket,
                                        'Отображение кнопки "Корзина" на странице',
                                        'ОР: Кнопка "Корзина" видна пользователю'))
            result_list.append(click_intercepted(basket,
                                                'Кликабельность кнопки "Корзина"',
                                                'Кликнуть в кнопку "Корзина"'))
            result_list.append(banner_url('cart',
                                        'Корзина'))
            page_title = find_elem('h1[class="basket-d__heading"]').text
            if 'Корзина' in page_title:
                 result_list.append(True)
            else:  result_list.append(False)
            allure_step_border(f'h1 title страницы, после клика в "Корзина"',
                            'basket-d__heading',
                            '0')
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

@allure.feature('Главная страница')
class test_b_body_elements(unittest.TestCase):
    @allure.story('Тело')
    @allure.title('Главный баннер')
    def test_a(self):
        result_list = []
        try:
            bunner = no_such_element(result_list,
                                     'div[class="main__banners-main"]',
                                     'main__banners-main',
                                     '0',
                                     'Поиск главного баннера',
                                     'Найти главный баннер',
                                     False)
            result_list.append(displayed(bunner,
                                         'Отображение главного баннера на странице',
                                         'ОР: Главный баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                 'Кликабельность главноего баннера',
                                                 'Кликнуть в главный баннер'))
            # result_list.append(banner_url('action',
            #                              'Главный баннер'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            navi = no_such_element(result_list,
                                   'div[class="main__banners-main-navigation"]',
                                   "main__banners-main-navigation",
                                   '0',
                                   'Поиск блока управления слайдами',
                                   'Найти блок управления слайдами баннера',
                                   False)
            result_list.append(displayed(navi,
                                         'Отображение панели переключения слайдов в главном баннере',
                                         'ОР: Панель управления слайдами видна пользователю'))
            button_next = no_such_element(result_list,
                                   'div[class="js-swiper__main-banner-next main__banners-main-navigation-next"]',
                                   "js-swiper__main-banner-next main__banners-main-navigation-next",
                                   '0',
                                   'Поиск кнопки переключения слайдов вперед',
                                   'Найти кнопку переключения слайдов вперед',
                                   False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов вперед видна пользователю'))
            old_activ_slide = find_elem('span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_activ_slide = find_elem('span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
            result_list.append(comparison(old_activ_slide,
                                          new_activ_slide,
                                          'Переключение слайдов вперед',
                                          False))
            button_back = no_such_element(result_list,
                                   'div[class="js-swiper__main-banner-prev main__banners-main-navigation-prev"]',
                                   "js-swiper__main-banner-prev main__banners-main-navigation-prev",
                                   '0',
                                   'Поиск кнопки переключения слайдов назад',
                                   'Найти кнопку переключения слайдов назад',
                                   False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов назад видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_activ_slide = find_elem('span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
            result_list.append(comparison(finally_activ_slide,
                                          new_activ_slide,
                                          'Переключение слайдов назад',
                                          False))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Товар дня')
    def test_b(self):
        result_list = []
        try:
            block = no_such_element(result_list,
                                    'div[class="main__banners-items"]',
                                    'main__banners-items',
                                    '0',
                                    'Поиск блока "Товар дня"',
                                    'Найти блок "Товар дня"',
                                    False)
            result_list.append(displayed(block,
                                         'Отображение блока "Товар дня"',
                                         'ОР: Блок "Товар дня" отображается на странице'))
            item_id = find_elem('div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(click_intercepted(block,
                                                 'Кликабельность блока "Товар дня"',
                                                 'Кликнуть в блок "Товар дня"'))
            page_item_id = find_elem('button[class="p_product_action--btn js_compare_item"]').get_attribute('data-item')
            back()
            result_list.append(comparison(item_id,
                                          page_item_id,
                                          'Релевантность перехода по клику в "Товар дня"',
                                          True))
            button_next = no_such_element(result_list,
                                          'div[class="js-swiper__item-banner-next main__banners-items-navigation-btn main__banners-items-navigation-next"]',
                                          "js-swiper__item-banner-next main__banners-items-navigation-btn main__banners-items-navigation-next",
                                          '0',
                                          'Поиск кнопки переключения слайдов "Товар дня" вперед',
                                          'Найти кнопку переключения слайдов "Товар дня" вперед',
                                          False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов "вперед" видна пользователю'))
            old_active_id = find_elem('div[class="swiper-slide swiper-slide-active"] div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность внопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_active_id = find_elem('div[class="swiper-slide swiper-slide-active"] div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(comparison(old_active_id,
                                          new_active_id,
                                          'Переключение слайдов кнопкой вперед',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__item-banner-prev main__banners-items-navigation-btn main__banners-items-navigation-prev"]',
                                          "js-swiper__item-banner-prev main__banners-items-navigation-btn main__banners-items-navigation-prev",
                                          '0',
                                          'Поиск кнопки переключения слайдов "Товар дня" назад',
                                          'Найти кнопку переключения слайдов "Товар дня" назад',
                                          False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов "назад" видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность внопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_active_id = find_elem('div[class="swiper-slide swiper-slide-active"] div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(comparison(new_active_id,
                                          finally_active_id,
                                          'Переключение слайдов кнопкой назад',
                                          False))
            add_cart = no_such_element(result_list,
                                  'div[class="to-cart cart js-add-to-cart js_to_cart--detail"]',
                                  "to-cart cart js-add-to-cart js_to_cart--detail",
                                  '0',
                                  'Поиск кнопки добавить в корзину',
                                  'Найти кнопку добавить в корзину в блоке "Товар дня"',
                                  False)
            result_list.append(displayed(add_cart,
                                         'Отображение кнопки добавть в корзину',
                                         'ОР: кнопка добавить в корзину видна пользователю'))
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В корзине нет товара',
                                         'Проверить что в иконке корзины нет числа',
                                         True)
            result_list.append(click_intercepted(add_cart,
                                                 'Кликабельность кнопки добавить в корзину',
                                                 'Кликнуть в кнопку "Добавить в корзину'))
            sleep(0.5)
            input_block = no_such_element(result_list,
                                          'div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "dc-row p_product_shoping js-product_card_shoping js-product-in-cart",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'Найти поле ввода количества товара',
                                          False)
            result_list.append(displayed(input_block, 
                                         'Отображение поля с количеством товара',
                                         'ОР: Поле с количеством товара отображается на странице'))
            new_cart_count = find_elem('span[class="fb-sticky-menu__badge_type_basket count"]').text
            browser.execute_script(f'document.getElementsByClassName("header-v2-basket")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
            result_list.append(comparison('0',
                                          new_cart_count,
                                          'Добавление товара в корзину',
                                          False))
            browser.execute_script(f"document.getElementsByClassName('header-v2-basket')[0].style.border=\"0px\";")
            plus_button = no_such_element(result_list,
                                          'div[class="item-of-a-day--first"] div[class="cart__amount-button cart__amount-increment btn_reset product_card_qty--btn js_plus-item"]',
                                          'dc-row p_product_shoping js-product_card_shoping js-product-in-cart',
                                          '0',
                                          'Поиск кнопки "+" в блоке Товар дня',
                                          'Найти кнопку "+" в блоке Товар дня',
                                          False)
            result_list.append(displayed(plus_button,
                                         'Отображение кнопки "+" на странице',
                                         'ОР: Кнопка "+" видна пользователю'))
            result_list.append(click_intercepted(plus_button,
                                                 'Кликкабельность кнопки "+"',
                                                 'Кликнуть в кнопку "+"'))
            minus_button = no_such_element(result_list,
                                          'div[class="item-of-a-day--first"] div[class="cart__amount-button cart__amount-decrement btn_reset product_card_qty--btn js_minus-item"]',
                                          'dc-row p_product_shoping js-product_card_shoping js-product-in-cart',
                                          '0',
                                          'Поиск кнопки "-" в блоке Товар дня',
                                          'Найти кнопку "-" в блоке Товар дня',
                                          False)
            result_list.append(displayed(minus_button,
                                         'Отображение кнопки "-" на странице',
                                         'ОР: Кнопка "-" видна пользователю'))
            result_list.append(click_intercepted(minus_button,
                                                'Кликабельность кнопки "-"',
                                                'Кликнуть в кнопку "-"'))
            sleep(0.5)
            result_list.append(click_intercepted(minus_button,
                                                'Закрытие поля корзины кликом в кнопку "-"',
                                                'Кликнуть в кнопку "-"'))
            input_block = no_such_element(result_list,
                                          'div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "dc-row p_product_shoping js-product_card_shoping js-product-in-cart",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'ОР: Поле корзины закрыто',
                                          True)
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В иконке корзины нет товара',
                                         'Проверить что, в иконке корзины нет числа',
                                         True)
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
                

    
    @allure.story('Тело')
    @allure.title('Популярные категории')
    def test_c(self):
        result_list = []
        try:
            popular_block = no_such_element(result_list,
                                            'div[class="main__categories"]',
                                            "main__categories",
                                            '0',
                                            'Поиск блока "Популярные категории"',
                                            'Найти блок "Популярные категории"',
                                            False)
            move_to(popular_block)
            popular_item = no_such_element(result_list,
                                           'div[class="main__categories"] div[class="swiper-slide swiper-slide-active"]',
                                           "main__categories",
                                           '0',
                                           'Поиск Слайда в блоке',
                                           'Найти слайд в блоке',
                                           False)
            result_list.append(displayed(popular_block,
                                         'Отображение блока "Популярные категории" на странице',
                                         'ОР: Блок "Популярные категории" виден пользователю'))
            #item_text = find_elem('p[class="categories__item-title-link"]').text
            button_next = no_such_element(result_list,
                            'div[class="js-swiper__categories-next categories__slider-navigation-link categories__slider-navigation-next"]',
                            'js-swiper__categories-next categories__slider-navigation-link categories__slider-navigation-next',
                            '0',
                            'Поиск кнопки переключения слайдов вперед',
                            'Найти кнопку переключения слайдов вперед',
                            False)
            old_item_href = find_elem('a[class="categories__item-info-link"]').text
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_item_href = find_elem('a[class="categories__item-info-link"]').text
            result_list.append(comparison(old_item_href,
                                          new_item_href,
                                          'Переключение слайдов',
                                          False))
            button_back = no_such_element(result_list,
                            'div[class="js-swiper__categories-prev categories__slider-navigation-link categories__slider-navigation-prev"]',
                            'js-swiper__categories-prev categories__slider-navigation-link categories__slider-navigation-prev',
                            '0',
                            'Поиск кнопки переключения слайдов назад',
                            'Найти кнопку переключения слайдов назад',
                            False)
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_item_href = find_elem('a[class="categories__item-info-link"]').text
            result_list.append(comparison(finally_item_href,
                                          new_item_href,
                                          'Переключение слайдов',
                                          False))
            sleep(0.3)
            result_list.append(click_intercepted(popular_item,
                                                 'Кликабельность блока "Популярные категории"',
                                                 'Кликнуть в блок "Популярные категории"'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            

        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Хиты продаж')
    def test_d(self):
        try:
            result_list = []
            block = no_such_element(result_list,
                            'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]',
                            'swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events',
                            '0',
                            'Поиск блока "Хиты продаж"',
                            'Найти блок "Хиты продаж"',
                            False)
            result_list.append(displayed(block,
                                         'Отображение блока "Хиты продаж" на станице',
                                         'ОР: блок "Хиты продаж" отображается на странице'))
            hits_item = no_such_element(result_list,
                                        'div[class="hits__item js-article-item"]',
                                        "hits__item js-article-item",
                                        '0',
                                        'Поиск слайда в блоке "Хиты продаж"',
                                        'Найти хотя бы один слайд в блоке "Хиты продаж"',
                                        False)
            button_next = no_such_element(result_list,
                            'div[class="js-swiper__hits-slider-next hits__slider-navigation-btn hits__slider-navigation-next"]',
                            "js-swiper__hits-slider-next hits__slider-navigation-btn hits__slider-navigation-next",
                            '0',
                            'Поиск кнопки переключения слайдов вперед',
                            'Найти кнопку переключения слайдов вперед',
                            False)
            old_item_id = find_elem('div[class="main__hits"] div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="swiper-slide swiper-slide-active"] a[class="hits__item-title"]').text
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_item_id = find_elem('div[class="main__hits"] div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="swiper-slide swiper-slide-active"] a[class="hits__item-title"]').text
            result_list.append(comparison(old_item_id,
                                          new_item_id,
                                          'Переключение слайдов в блоке',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__hits-slider-prev hits__slider-navigation-btn hits__slider-navigation-prev"]',
                                          "js-swiper__hits-slider-prev hits__slider-navigation-btn hits__slider-navigation-prev",
                                          '0',
                                          'Поиск кнопки переключения слайдов назад',
                                          'Найти кнопку переключения слайдов назад',
                                          False)
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_item_id = find_elem('div[class="main__hits"] div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="swiper-slide swiper-slide-active"] a[class="hits__item-title"]').text
            result_list.append(comparison(new_item_id,
                                          finally_item_id,
                                          'Переключение слайдов назад',
                                          False))
            #############################################################################################################################################
            add_cart = no_such_element(result_list,
                                  'div[class="main__hits"] div[class="to-cart cart js-add-to-cart js_to_cart--detail"]',
                                  "hits__item js-article-item",
                                  '0',
                                  'Поиск кнопки добавить в корзину',
                                  'Найти кнопку добавить в корзину в блоке "Хиты продаж"',
                                  False)
            result_list.append(displayed(add_cart,
                                         'Отображение кнопки добавть в корзину',
                                         'ОР: кнопка добавить в корзину видна пользователю'))
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В корзине нет товара',
                                         'Проверить что в иконке корзины нет числа',
                                         True)
            result_list.append(click_intercepted(add_cart,
                                                 'Кликабельность кнопки добавить в корзину',
                                                 'Кликнуть в кнопку "Добавить в корзину'))
            input_block = no_such_element(result_list,
                                          'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "hits__item js-article-item",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'Найти поле ввода количества товара',
                                          False)
            result_list.append(displayed(input_block, 
                                         'Отображение поля с количеством товара',
                                         'ОР: Поле с количеством товара отображается на странице'))
            sleep(0.3)
            new_cart_count = find_elem('span[class="fb-sticky-menu__badge_type_basket count "]').text
            browser.execute_script(f'document.getElementsByClassName("header-v2-basket")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
            result_list.append(comparison('0',
                                          new_cart_count,
                                          'Добавление товара в корзину',
                                          False))
            browser.execute_script(f"document.getElementsByClassName('header-v2-basket')[0].style.border=\"0px\";")
            plus_button = no_such_element(result_list,
                                          'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="cart__amount-button cart__amount-increment btn_reset product_card_qty--btn js_plus-item"]',
                                          'hits__item js-article-item',
                                          '0',
                                          'Поиск кнопки "+" в блоке "Хиты продаж"',
                                          'Найти кнопку "+" в блоке "Хиты продаж"',
                                          False)
            result_list.append(displayed(plus_button,
                                         'Отображение кнопки "+" на странице',
                                         'ОР: Кнопка "+" видна пользователю'))
            result_list.append(click_intercepted(plus_button,
                                                 'Кликкабельность кнопки "+"',
                                                 'Кликнуть в кнопку "+"'))
            minus_button = no_such_element(result_list,
                                          'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="cart__amount-button cart__amount-decrement btn_reset product_card_qty--btn js_minus-item"]',
                                          'hits__item js-article-item',
                                          '0',
                                          'Поиск кнопки "-" в блоке "Хиты продаж"',
                                          'Найти кнопку "-" в блоке "Хиты продаж"',
                                          False)
            result_list.append(displayed(minus_button,
                                         'Отображение кнопки "-" на странице',
                                         'ОР: Кнопка "-" видна пользователю'))
            result_list.append(click_intercepted(minus_button,
                                                'Кликабельность кнопки "-"',
                                                'Кликнуть в кнопку "-"'))
            sleep(0.5)
            result_list.append(click_intercepted(minus_button,
                                                'Закрытие поля корзины кликом в кнопку "-"',
                                                'Кликнуть в кнопку "-"'))
            input_block = no_such_element(result_list,
                                          'div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "dc-row p_product_shoping js-product_card_shoping js-product-in-cart",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'ОР: Поле корзины закрыто',
                                          True)
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В иконке корзины нет товара',
                                         'Проверить что, в иконке корзины нет числа',
                                         True)
            result_list.append(click_intercepted(hits_item,
                                                 'Кликабельность слайдов',
                                                 'Кликнуть в слайд'))
            sleep(1)
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            show_all = no_such_element(result_list,
                                       'a[class="hits__all-hits-link"]',
                                       'hits__all-hits-link',
                                       '0',
                                       'Поиск кнопки "Смотреть все"',
                                       'Найти кнопку Смеотреть все',
                                       False)
            result_list.append(displayed(show_all,
                                         'Отображение кнопки "Смотреть все" на странице',
                                         'ОР: Кнопка "Смотреть все" видна пользователю'))
            result_list.append(click_intercepted(show_all,
                                                 'Кликабельность кнопки "Смотреть все"',
                                                 'Кликнуть в кнопку "Смотреть все"'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)

        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Поиск хедера')
    def test_e_find_header(self):
        try:
            header = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
            action.move_to_element(header).perform()
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "header" в функции "test_find_header"')
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин хедера', attachment_type=AttachmentType.PNG)
        self.assertTrue(header.is_displayed(), msg = f'Хедер не найден {date}')

    @allure.story('Тело')
    @allure.title('Поиск футера')
    def test_find_footer(self):
        try:
            footer = browser.find_element(By.CSS_SELECTOR, 'div[class="page--wrapper -white"] footer[class="js-footer"]')
            action.move_to_element(footer).perform()
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "footer" в функции "test_find_footer"')
        with allure.step('Футер'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин футера', attachment_type=AttachmentType.PNG)
        self.assertTrue(footer.is_displayed(), msg = f'Футер не найден {date}')

    @allure.story('Тело')
    @allure.title('Популярные бренды')
    def test_e(self):
        result_list = []
        try:
            block = no_such_element(result_list,
                                    'div[class="popular-brands"]',
                                    'popular-brands',
                                    '0',
                                    'Поиск блок Популрные бренды',
                                    'Найти блок "Популярные бренды"',
                                    False)
            result_list.append(displayed(block,
                                         'Отображение блока "Популярные бренды" на странице',
                                         'ОР: Блок "Популярные бренды" виден пользователю'))
            item = no_such_element(result_list,
                                   'a[class="popular-brands__brand"]',
                                   'popular-brands__brand',
                                   '0',
                                   'Поиск слайдов',
                                   'найти хотя бы один слайд в блоке',
                                   False)
            result_list.append(displayed(item,
                                         'Отображение слайдов в блоке',
                                         'ОР: Слайды видны пользователю'))
            name = item.text
            result_list.append(click_intercepted(item,
                                                 'Кликабельность слайдов',
                                                 'Кликнуть в слайд'))
            result_list.append(h1_title(name))
            back()
            sleep(1)
            button_next = no_such_element(result_list,
                                          'div[class="js-swiper__popular-brands-next popular-brands__slider-navigation-button popular-brands__slider-navigation-next"]',
                                          'js-swiper__popular-brands-next popular-brands__slider-navigation-button popular-brands__slider-navigation-next',
                                          '0',
                                          'Поиск кнопки переключения слайдов вперед',
                                          'Найти кнопку переключения слайдов вперед',
                                          False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов вперед видна пользователю'))
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.5)
            new_name  = find_elem('div[class="popular-brands"] div[class="swiper-slide swiper-slide-active"]').text
            result_list.append(comparison(name,
                                          new_name,
                                          'переключение слайдов после клика вперед',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__popular-brands-prev popular-brands__slider-navigation-button popular-brands__slider-navigation-prev"]',
                                          'js-swiper__popular-brands-prev popular-brands__slider-navigation-button popular-brands__slider-navigation-prev',
                                          '0',
                                          'Поиск кнопки переключения слайдов назад',
                                          'Найти кнопку переключения слайдов назад',
                                          False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов назад видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.5)
            finally_name = find_elem('div[class="popular-brands"] div[class="swiper-slide swiper-slide-active"]').text
            result_list.append(comparison(finally_name,
                                          new_name,
                                          'переключение слайдов после клика назад',
                                          False))
            
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Нижний баннер')
    def test_f(self):
        result_list = []
        try:
            bunner = no_such_element(result_list,
                                     'div[class="promos"]',
                                     'promos',
                                     '0',
                                     'Поиск Нижнего баннера',
                                     'Найти нижний баннер',
                                     False)
            result_list.append(displayed(bunner,
                                         'Отображение нижнего баннера',
                                         'ОР: Нижний баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                 'Кликабельность нижнего баннера',
                                                 'Кликнуть в нижний баннер'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ПРОВЕРКА ПЕРЕКЛЮЧЕНИЯ СЛАЙДОВ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Советы эксперта')
    def test_g(self):
        browser.get(link)
        result_list = []
        try:
            block = no_such_element(result_list,
                                    'div[class="expert-advices"]',
                                    'expert-advices',
                                    '0',
                                    'Поиск блока "Советы эксперта"',
                                    'Найти блок советы эксперта',
                                    False)
            result_list.append(displayed(block,
                                         'Отображение блока "Советы эксперта" на странице',
                                         'ОР: Блок "Советы эксперта" видны пользователю'))
            item = no_such_element(result_list,
                                   'a[class="expert-advices__card expert-advices__card--dark-gray"]',
                                   'expert-advices__card expert-advices__card--dark-gray',
                                   '0',
                                   'Поиск слайдов в блоке',
                                   'Найти слайд в блоке',
                                   False)
            result_list.append(displayed(item,
                                         'Отображение слайдов в блоке "Советы эксперта"',
                                         'ОР: Слайды в блоке "Советы эксперта" видны пользователю'))
            name = find_elem('h3[class="expert-advices__card-description-title"]').text
            result_list.append(click_intercepted(item,
                                                 'Кликабельность слайда "Советы эксперта"',
                                                 'Кликнуть в слайд "Советы эксперта"'))
            result_list.append(h1_title(name))
            back()
            sleep(1)
            button_next = no_such_element(result_list,
                                        'div[class="js-swiper__expert-advices-next expert-advices__slider-navigation-button expert-advices__slider-navigation-next"]',
                                        'js-swiper__expert-advices-next expert-advices__slider-navigation-button expert-advices__slider-navigation-next',
                                        '0',
                                        'Поиск кнопки переключения слайдов вперед',
                                        'Найти кнопку переключения слайдов вперед',
                                        False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов вперед видна пользователю'))
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.5)
            new_name  = find_elem('div[class="expert-advices"] div[class="swiper-slide swiper-slide-active"] h3').text
            result_list.append(comparison(name,
                                          new_name,
                                          'переключение слайдов после клика вперед',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__expert-advices-prev expert-advices__slider-navigation-button expert-advices__slider-navigation-prev"]',
                                          'js-swiper__expert-advices-prev expert-advices__slider-navigation-button expert-advices__slider-navigation-prev',
                                          '0',
                                          'Поиск кнопки переключения слайдов назад',
                                          'Найти кнопку переключения слайдов назад',
                                          False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов назад видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.5)
            finally_name = find_elem('div[class="expert-advices"] div[class="swiper-slide swiper-slide-active"] h3').text
            result_list.append(comparison(finally_name,
                                          new_name,
                                          'переключение слайдов после клика назад',
                                          False))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
        
    def test_z(self):
        ads()


########################FOOTER#############################FOOTER#########################FOOTER#############################FOOTER#########################FOOTER#############################FOOTER#########################FOOTER   


@allure.feature('Каталог')
class test_catalog(unittest.TestCase):
    @allure.title('Проверка "изменение цвета текста пунктов каталога при наведении курсора"')
    def test_a(self):
        browser.delete_all_cookies()
        browser.get(link)
        sleep(1)
        city_def()
        sleep(1)
        browser.find_element(By.CSS_SELECTOR, 'div[class="menu-only"]').click()
        sleep(1)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        item = items[randint(0, len(items) - 1)]
        text_color_before = item.value_of_css_property('color')
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до фокуса на элемент', attachment_type=AttachmentType.PNG)
        action.move_to_element(item).perform()
        sleep(1)
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после фокуса на элемент', attachment_type=AttachmentType.PNG)
        text_color_after = item.value_of_css_property('color')
        self.assertNotEqual(text_color_before, text_color_after, msg = f'цвет текста не изменился {date}')

    @allure.title('Проверка "изменение цвета фона пунктов каталога при наведении курсора"')
    def test_b(self):
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        block_background_color = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-header-catalog-menu__parent-menu"]').value_of_css_property('background-color')
        item = items[randint(0, len(items) - 1)]
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до фокуса на элемент', attachment_type=AttachmentType.PNG)
        action.move_to_element(item).perform()
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после фокуса на элемент', attachment_type=AttachmentType.PNG)
        color_background_after = item.value_of_css_property('background-color')
        self.assertNotEqual(block_background_color, color_background_after, msg = f'цвет фона не изменился {date}')   

    @allure.title('Проверка кликабельности пунктов каталога')
    def test_c(self):
        result = []
        for i in range(3):
            try:
                items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
                item = items[randint(0, len(items) - 1)]
                name_item = item.text
                item.click()
                with allure.step(f'Скрин {name_item}'):
                    allure.attach(browser.get_screenshot_as_png(), name = f'Скрин после клика в пункт {name_item}', attachment_type=AttachmentType.PNG)
            except ElementNotInteractableException:
                result.append(False)
            else: result.append(True)
            browser.find_element(By.CSS_SELECTOR, 'div[class="menu-only"]').click()
            self.assertTrue(result, msg = f'Категории не кликабельны {date}')

    @allure.title('Проверка скролла каталога')
    def test_d(self):
        browser.get(link)
        city_def()
        catalog = browser.find_element(By.CSS_SELECTOR, 'div[class="menu-only"]')
        catalog.click()
        time.sleep(0.5)
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до прокрутки каталога', attachment_type=AttachmentType.PNG)
        result = len(browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]'))
        last_item = browser.find_element(By.CSS_SELECTOR, f'.fb-header-catalog-menu__parent-link[data-id="{result}"]')
        action.move_to_element(last_item).perform()
        sleep(0.5)
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после прокрутки каталога', attachment_type=AttachmentType.PNG)
        select_item = browser.find_element(By.CSS_SELECTOR, f'a[class="fb-header-catalog-menu__parent-link fb-header-catalog-menu__parent-menu_selected"][data-id="{result}"]')
        self.assertTrue(select_item, msg = f'меню каталога не скроллится {date}')

    @allure.title('Проверка изменения кнопки каталога после клика')
    def test_e(self):
        before_click = []
        after_click = []
        browser.get(link)
        city_def()
        button = browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]')
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до клика в кнопку "Каталог"', attachment_type=AttachmentType.PNG)
        before_click.append(browser.find_element(By.CSS_SELECTOR, 'a[class="dropdown-toggle header-v2-catalog"] span[class="first"]').value_of_css_property('transform'))
        before_click.append(browser.find_element(By.CSS_SELECTOR, 'a[class="dropdown-toggle header-v2-catalog"] span[class="second"]').value_of_css_property('transform'))
        before_click.append(browser.find_element(By.CSS_SELECTOR, 'a[class="dropdown-toggle header-v2-catalog"] span[class="third"]').value_of_css_property('transform'))
        button.click()
        time.sleep(0.5)
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после клика в кнопку "Каталог"', attachment_type=AttachmentType.PNG)
        after_click.append(browser.find_element(By.CSS_SELECTOR, 'td[class="menu-item dropdown catalog fb-toggle-catalog-button"] span[class="first"]').value_of_css_property('transform'))
        after_click.append(browser.find_element(By.CSS_SELECTOR, 'td[class="menu-item dropdown catalog fb-toggle-catalog-button"] span[class="second"]').value_of_css_property('transform'))
        after_click.append(browser.find_element(By.CSS_SELECTOR, 'td[class="menu-item dropdown catalog fb-toggle-catalog-button"] span[class="third"]').value_of_css_property('transform'))
        self.assertNotEqual(before_click, after_click, msg = f'Кнопка каталога не изменилась {date}')

    @allure.title('Проверка корректности расположения разделов каталога')
    def test_f(self):
        standard = [f'Тепловое оборудование', f'Сантехника', f'Стройматериалы', f'Напольные покрытия', f'Отделка стен и потолка', f'Керамическая плитка', f'Двери и окна', f'Лакокрасочные материалы', f'Климат и отопление', f'Инструменты', f'Товары для дачи и отдыха', f'Мебель', f'Освещение', f'Водоснабжение', f'Электротовары', f'Крепеж и фурнитура', f'Товары для дома', f'Автотовары']
        with allure.step('Эталон'):
            allure.attach(f'{standard}', name = 'Категории', attachment_type=AttachmentType.TEXT)
        new = []
        error = None
        browser.get(link)
        browser.find_element(By.CSS_SELECTOR, '.header-v2-catalog-button').click()
        selected_item = browser.find_element(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link fb-header-catalog-menu__parent-menu_selected"]')
        name_selected_item = selected_item.text
        new.append(name_selected_item)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        for item in items:
            item_text = item.text
            new.append(item_text)
        if standard == new: 
            result = True
        else: 
            result = False
            error = 'Порядок категорий не совпадает'
            with allure.step('Данные страницы'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин списка каталога', attachment_type=AttachmentType.PNG)
                allure.attach(f'{error}\n{new}', name = f'ошибка', attachment_type=AttachmentType.TEXT)
            if len(standard) != len(new):
                result = False
                error = 'Не совпадает количество категорий'
                with allure.step('Данные страницы'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин списка каталога', attachment_type=AttachmentType.PNG)
                    allure.attach(f'{error}\n{new}', name = f'ошибка', attachment_type=AttachmentType.TEXT)
        with allure.step('Эталон'):
            allure.attach(f'{new}', name = 'Категории сейчас', attachment_type=AttachmentType.TEXT)
        self.assertTrue(result, msg = f'{error} {date}')


@allure.feature('Раздел каталога')          
class catalog_section(unittest.TestCase):

    @allure.title('Проверкка открытия раздела каталога')
    def test_a_opening_directory_catalog(self):
        browser.get(link)
        catalog_button = browser.find_element(By.CSS_SELECTOR, 'div[class="table-menu"]')
        catalog_button.click()
        time.sleep(0.3)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        item = items[randint(0, len(items) -1)]
        item_name = item.text
        action.move_to_element(item).perform()
        item.click()
        subcategory_items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-deep-page__category-children-column"]')
        subcategory = subcategory_items[randint(0, len(subcategory_items) -1)]
        subcategory_name = subcategory.text
        action.move_to_element(subcategory).perform()
        subcategory.click()
        global url_subcategory
        url_subcategory = browser.current_url
        try:
            products = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__product-column"]')
            product_num = len(products)
            with allure.step('Ссылки и скрин'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.title, name = 'title', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step('Не найдены товары на странице'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.title, name = 'title', attachment_type=AttachmentType.TEXT)   
        self.assertTrue (product_num > 0, msg = f'На странице отсутствуют карточки товаров {date}')

    @allure.title('Проверка "хлебных корочек"')
    def test_bread_crusts(self):
        browser.get(url_subcategory)
        try:
            bread_crusts = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-breadcrumb__value"]')
            action.move_to_element(bread_crusts).perform()
            time.sleep(0.3)
            with allure.step("Хлебные корочки"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step("Хлебные корочки не найдены"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(bread_crusts.is_displayed(), msg = f'Хлебные корочки не найдены на странице {date}')    

    @allure.title('Проверка скролла страницы')
    def test_scroll_page(self):
        browser.get(url_subcategory)
        with allure.step('Скрин до скролла'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        # try:
        #     scroll_to_top = browser.find_element(By.CSS_SELECTOR, 'a[class="scroll-to-top RECT_WHITE PADDING visible"]')
        # except NoSuchElementException:
        scroll = browser.find_elements(By.CSS_SELECTOR, 'footer[class="js-footer"] div[class="container-fluid"]')
        action.scroll_to_element(scroll[-1]).perform()
        time.sleep(0.3)
        with allure.step('Скрин после скролла'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        scroll_to_top = browser.find_element(By.CSS_SELECTOR, 'a[class="scroll-to-top RECT_WHITE PADDING visible"]')
        self.assertTrue(scroll_to_top.is_displayed(), msg = f'Страница не скроллится {date}')
    
    @allure.title('Проверка сортировки по цене')
    def test_sorting_min_price(self):
        before_item_list = []
        after_item_list = []
        browser.get(url_subcategory)
        time.sleep(3)
        items_page = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items_page:
            item_id = i.get_attribute('data-id')
            before_item_list.append(item_id)
        try:
            filters = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__container"]')
            action.move_to_element(filters).perform()
            with allure.step('Скрин до изменения сортировки по минимальной цене'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            min_price = browser.find_element(By.CSS_SELECTOR, 'div[data-value="cheap"]').click()
            time.sleep(3)
            with allure.step('Скрин после изменения сортировки по минимальной цене'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items_page2 = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page2:
                item_id = i.get_attribute('data-id')
                after_item_list.append(item_id)
        except NoSuchElementException:
            pass
        self.assertNotEqual(before_item_list, after_item_list, msg = f'Сортировка по цене не работает {date}')

    @allure.title('Проверка сортировки по скидке')
    def test_sorting_discount(self):
        before_item_list = []
        after_item_list = []
        browser.get(url_subcategory)
        time.sleep(3)
        items_page = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items_page:
            item_id = i.get_attribute('data-id')
            before_item_list.append(item_id)
        try:
            filters = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__container"]')
            action.move_to_element(filters).perform()
            with allure.step('Скрин до изменения сортировки по скидке'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            discount_size = browser.find_element(By.CSS_SELECTOR, 'div[data-value="discount_size"]').click()
            time.sleep(3)
            with allure.step('Скрин после изменения сортировки по скидке'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items_page2 = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page2:
                item_id = i.get_attribute('data-id')
                after_item_list.append(item_id)
        except NoSuchElementException:
            pass
        self.assertNotEqual(before_item_list, after_item_list, msg = f'Сортировка по скидке не работает {date}')

    @allure.title("Проверка сортировки по популярности")
    def test_sorting_popular(self):
        before_item_list = []
        after_item_list = []
        browser.get(url_subcategory)
        time.sleep(3)
        try:
            filters = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__container"]')
            action.move_to_element(filters).perform()
            with allure.step('Скрин по популярности'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            discount_size = browser.find_element(By.CSS_SELECTOR, 'div[data-value="discount_size"]').click()
            time.sleep(3)
            items_page = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page:
                item_id = i.get_attribute('data-id')
                before_item_list.append(item_id)
            with allure.step('Скрин до изменения сортировки по популярности'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            popular = browser.find_element(By.CSS_SELECTOR, 'div[data-value="popular"]').click()
            time.sleep(3)
            with allure.step('Скрин после изменения сортировки по популярности'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items_page2 = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page2:
                item_id = i.get_attribute('data-id')
                after_item_list.append(item_id)
        except NoSuchElementException:
            pass
        self.assertNotEqual(before_item_list, after_item_list, msg = f'Сортировка по популярности не работает {date}')

    @allure.title('Проверка открытия фильтра')
    def test_opening_filter(self):
        try:
            input_block = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1111"] input[type="text"]')
            if input_block.is_displayed() == True:
                result_before = True
            else: result_before = False
        except NoSuchElementException:
            pass
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(buttons[9]).perform()
        with allure.step('Скрин до раскрытия фильтра'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        button = buttons[8].click()
        time.sleep(2)
        with allure.step('Скрин после раскрытия фильтра'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            input_block = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1111"] input[type="text"]')
            if input_block.is_displayed() == True:
                result_after = True
            else: result_after = False
        except NoSuchElementException:
            pass
        self.assertNotEqual(result_before, result_after, msg = f'Фильтр не открылся {date}')

    @allure.title('Проверка работы ползунков фильтра')
    def test_p_filter_sliders(self):
        error = ''
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(buttons[9]).perform()
        button = buttons[9]
        button.click()
        button_name = button.text
        time.sleep(1)
        with allure.step('Скрин до перемещения ползунков'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_max_before = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuemax')
        input_min_before = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuenow')
        dots = browser.find_elements(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-touch-area"]')
        dot_min = dots[0]
        dot_max = dots[1]
        action.drag_and_drop_by_offset(dot_max, -50, 0).perform()
        action.drag_and_drop_by_offset(dot_min, 50, 0).perform()
        time.sleep(2)
        with allure.step('Скрин после перемещения ползунков'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_max_after = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuemax')
        input_min_after = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuenow')
        if input_max_before != input_max_after:
            result_max = True
        else: 
            error = "Не работает ползунок максимум"
            result_max = False
            with allure.step('Максимальное значение не изменилась, ползунок не работает'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        if input_min_before != input_min_after:
            result_min = True
        else: 
            result_min = False
            error = "Не работает ползунок минимум"
            with allure.step('Минимальное значение не изменилась, ползунок не работает'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        if result_max == result_min:
            result = True
        else: result = False
        self.assertTrue(result, msg = f'{error} {date}')
    

    @allure.title('Проверка работы фильтра по минимальной цене')
    def test_filter_min_price(self):
        after_price = 0
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')).perform()
        input_min = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider-input__input"] input[type="text"]')
        action.move_to_element(input_min).perform()
        sleep(0.5)
        # input_name = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-slider__slider noUi-target noUi-ltr noUi-horizontal noUi-txt-dir-ltr"]').get_attribute('data-min')
        # back_num = len(input_name)
        with allure.step('Скрин выдачи до изменения минимальной цены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_min.click()
        for i in range(7):
            input_min.send_keys(Keys.BACKSPACE)
        input_min.send_keys('3000')
        #browser.find_element(By.CSS_SELECTOR, 'div[class="page-top-main"]').click()
        time.sleep(3)
        with allure.step('Скрин выдачи после изменения минимальной цены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        prices = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]')
        items_num = len(prices) 
        for i in prices:
            price_str = i.text
            price = int(''.join(filter(str.isdigit, price_str)))
            if price >= 3000:
                after_price += 1
            else: after_price -= 1
        try:
            clear = browser.find_element(By.CSS_SELECTOR, '#fb-filter-chips-remove-all')
        except NoSuchElementException:
            print('нет облака')
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')).perform()
        clear.click()
        self.assertEqual(after_price, items_num, msg = f'Фильтр по минимальной цене не работает {date}')

    @allure.title('Проверка работы фильтра по максимальной цене')
    def test_filter_max_price(self):
        browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        after_price = 0
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'h1[id="pagetitle"]')).perform()
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        #action.move_to_element(buttons[9]).perform()
        inputs = browser.find_elements(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider-input__input"] input[type="text"]')
        input_max = inputs[1]
        action.move_to_element(input_max).perform()
        inputs_names = browser.find_elements(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider__slider noUi-target noUi-ltr noUi-horizontal noUi-txt-dir-ltr"]')
        input_name = inputs_names[1].get_attribute('data-max')
        back_num = len(input_name)
        with allure.step('Скрин выдачи до изменения минимальной цены'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_max.click()
        for i in range(back_num):
            input_max.send_keys(Keys.BACKSPACE)
        input_max.send_keys('4000')
        #browser.find_element(By.CSS_SELECTOR, 'div[class="page-top-main"]').click()
        time.sleep(3)
        with allure.step('Скрин выдачи после изменения минимальной цены'): 
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        prices = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]')
        items_num = len(prices) 
        for i in prices:
            price_str = i.text
            price = int(''.join(filter(str.isdigit, price_str)))
            if price < 4000:
                after_price += 1
            else: after_price -= 1
        try:
            clear = browser.find_element(By.CSS_SELECTOR, '#fb-filter-chips-remove-all')
        except NoSuchElementException:
            print('нет облака')
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'h1[id="pagetitle"]')).perform()
        clear.click()
        self.assertEqual(after_price, items_num, msg = f'Фильтр по максимальной цене не работает {date}')
        
    def test_z(self):
        ads()

    
@allure.feature('Карточка товара')
@allure.severity('CRITICAL')
class ttest_b_product_page_header(unittest.TestCase):
    browser.delete_all_cookies()
    browser.get(link)
    @allure.story('Хедер')
    @allure.title('Выбор города')
    def test_a(self):
        # кнопка каталог
        result_list = []
        city_def()
        try:
            item_of_day = find_elem('a[class="item-of-day__name"]')
            item_of_day.click()
            steps_list("Клик в активный слайд блока товары дня")
        except:
            menu = find_elem('div[class="menu-only"]')
            menu.click()
            steps_list("Клик в кнопку каталога")
            sleep(0.2)
            categoryes = find_elems('a[class="fb-header-catalog-menu__parent-link"]')
            category = categoryes[randint(0, len(categoryes) -1)]
            category_name = category.text
            move_to(category)   
            category.click()
            steps_list(f'Клик в {category_name}')
            sleep(1)
            sub_category = find_elem('div[class="fb-category-image-link__name"]')
            sub_category_name = sub_category.text
            sub_category.click()
            steps_list(f'Клик в {sub_category_name}')
            sleep(1)
            item = find_elem('a[class="fb-product-card__title-inner"]')
            item_name = item.text
            item.click()
            steps_list(f'Клик в {item_name}')
        #переход на карточку товара
        #browser.get('https://stroylandiya.ru/products/arka-pryamaya-berlin-pvkh-190kh2200kh2200mm-belyy-yasen/') #!!!!!!!!!ОДНО ФОТО!!!!
        #browser.get('https://stroylandiya.ru/products/drel-shurupovert-akkumulyatornaya-makita-df-347dwen/') # !!!!!!!ТУТ ЕСТЬ КНОПКА ПРОКРУТКИ ФОТО!!!!!!!!!!
        city_def()
        test_a_header_elements.test_a(self)
        

    @allure.story('Хедер')
    @allure.title('Баннер над хедером')
    def test_b(self):  
        test_a_header_elements.test_b(self)

    @allure.story('Хедер')
    @allure.title('Магазины')
    def test_c(self):
        test_a_header_elements.test_c(self)

    @allure.story('Хедер')
    @allure.title('Статус заказа')
    def test_d(self):
        test_a_header_elements.test_d(self)

    @allure.story('Хедер')
    @allure.title('Номер телефона')
    def test_e(self):
        test_a_header_elements.test_e(self)
    
    @allure.story('Хедер')
    @allure.title('Логотип')
    def test_f(self):
        test_a_header_elements.test_f(self)
        sleep(2)
        back()
        sleep(2)
    
    @allure.story('Хедер')
    @allure.title('Каталог')
    def test_g(self):
        test_a_header_elements.test_g(self)

    @allure.story('Хедер')
    @allure.title('Поиск')
    def test_h(self):
        test_a_header_elements.test_h(self)
    
    @allure.story('Хедер')
    @allure.title('Акции')
    def test_i(self):
        test_a_header_elements.test_j(self)

    @allure.story('Хедер')
    @allure.title('Вход')
    def test_j(self):
        test_a_header_elements.test_k(self)

    @allure.story('Хедер')
    @allure.title('Сравнение')
    def test_k(self):
        test_a_header_elements.test_l(self)

    @allure.story('Хедер')
    @allure.title('Избранное')
    def test_l(self):
        test_a_header_elements.test_m(self)

    @allure.story('Хедер')
    @allure.title('Корзина')
    def test_m(self):
         test_a_header_elements.test_n(self)

@allure.feature('Карточка товара')
class test_b_product_page_body(unittest.TestCase):  
    @allure.story('Тело')
    @allure.title('Проверка кнопки Отзывы')
    def test_c(self):
    # отзывы 
    # тут баг с кнопкой, вернусь к этому позже
        url_before = browser.current_url
        result_list = []
        try:
            review = no_such_element(result_list,
                            'div[class="dc-row"] div[class="p_product_review dc-row -sm"] a[class="js-scrollto decoration-none"]',
                            'js-scrollto decoration-none',
                            '1',
                            'Поиск кнопки "Отзывы"',
                            'Найти кнопку "Отзывы"',
                            False)
            if review == None:
                result_list.clear()
                review = no_such_element(result_list,
                                            'div[class="container-fluid"] a[class="js-scrollto-tab decoration-none"]',
                                            'js-scrollto-tab decoration-none',
                                            '1',
                                            'Поиск кнопки "Отзывы"',
                                            'Найти кнопку "Отзывы"',
                                            False)
            result_list.append(displayed(review,
                                        'Отображение кнопки "Отзывы" на странице',
                                        'ОР: Кнопка "Отзывы" отображается на странице'))
            result_list.append(click_intercepted(review,
                            'Кликабельность кнопки "Отзывы"',
                            'Кликнуть в кнопку "Отзывы"'))
            tab_review = no_such_element(result_list,
                            'div[class="dcol-0 p_product_info--item_wrapper js-reviews js-btns active"]',
                            'dcol-0 p_product_info--item_wrapper js-reviews js-btns active',
                            '0',
                            'Поиск вкладки "Отзывы"',
                            'Найти вкуладку "Отзывы"',
                            False)
            result_list.append(displayed(tab_review,
                                        'Отображение вкладки "Отзывы" на странице',
                                        'ОР:Вкладка "Отзывы" отображается на странице'))
        # В данный момент кнопка НЕ работает, срабатывает якорь, сами отзывы НЕ разворачиваются, можно сравнивать ссылки //otzyvy/ - рабочая кнопка
        finally:
            url_after = browser.current_url
            print(url_before)
            print(url_after)
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Проверка кнопки к сравнению')
    def test_d(self):
        # к сравнению
        result_list = []
        action.scroll(0, 0, 0, -2000).perform()
        sleep(1)
        compare = no_such_element(result_list,
                        'div[class="dc-row"] button[class="p_product_action--btn js_compare_item"] span',
                        'p_product_action--btn js_compare_item',
                        '1',
                        'Поиск кнопки "К сравнению"',
                        'Найти кнопку "К сравнению"',
                        False)
        result_list.append(displayed(compare,
                                     'Отображение кнопки "К сравнению" на странице',
                                     'ОР: Кнопка "К сравнению" отображается на странице'))
        result_list.append(click_intercepted(compare,
                          'Кликабельность кнопки "К сравнению"',
                          'Кликнуть в кнопку "К сравнению"'))
        compare_count = no_such_element(result_list,
                        'span[class="basket-link jlink compare header-v2-compare "] span[class="count"]',
                        'basket-link compare header-v2-compare ',
                        '0',
                        'Поиск счетчика в иконке "Сравнение"',
                        'Найти счетчик в иконке "Сравнение"',
                        False)
        result_list.append(displayed(compare_count,
                                    'Отображение счетчика количества товаров',
                                    'ОР: В иконке сравнение отображается счетчик товаров'))
        try:
            compare_count = find_elem('span[class="basket-link jlink compare header-v2-compare "] span[class="count"]')
            compare_count_name = compare_count.text
            debug_msg = f'Количество товара в сравнении TRUE' #!!!!!!!!!!!!!!!!!!!!!!обработать количество!!!!!!!!!!!!!!!!!!!!!!!!!!
            allure_step_border('Количество товара в сравнении изменилось', 'basket-link compare header-v2-compare ', '0')
            steps_list('Проверить количество товара в иконке сравнения')
            result_list.append(True)
        except NoSuchElementException:
            allure_step('Количество в сравнении товара НЕ изменилось')
            debug_msg = 'Количество товара в сравнении FALSE'
            result_list.append(False)
        
        # debugger(debug_msg)
        
        steps_case()
        debug_case()
        assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Проверка кнопки в избранное')
    def test_e(self):
    # в избранное
        try:
            result_list = []
            try:
                favorit = find_elem('div[class="dc-row"] button[class="p_product_action--btn js_wish_item"] span[class="d-none d-lg-inline-flex"]')
                debug_msg = 'Поиск кнопки "В избранное" True'
                steps_list('Найти кнопку "В избранное" на странице')
                allure_step_border('На странице есть кнопка "В избранное"', 'dcol-0 p_product_action--item', '3')
                result_list.append(True)
            except NoSuchElementException:
                debug_msg = 'Поиск кнопки "В избранное" FALSE'
                allure_step('Кнопка "В избранное" НЕ найдена')
                result_list.append(False)
            # debugger(debug_msg)
            try:
                favorit.click()
                debug_msg = 'Кликабельность кнопки "В избранное" True'
                steps_list('Кликнуть в кнопку избранное')
                result_list.append(True)
            except ElementClickInterceptedException:
                debug_msg = 'Кликабельность кнопки "В избранное" False'
                result_list.append(False)
            sleep(1.5)
            # debugger(debug_msg)
            try:
                favorin_count = find_elem('span[class="fb-sticky-menu__badge_type_personal count"]')
                favorin_count_int = favorin_count.text
                debug_msg = f'Количество товара в иконке True {favorin_count_int}'
                allure_step_border('Проверить что в иконку избранное добавилось +1', 'header-v2-favourites', '0')
                result_list.append(True)
            except NoSuchElementException:
                debug_msg = 'Количество товара в иконке False' 
                result_list.append(False) 
            # debugger(debug_msg)
        finally:
            steps_case()
            debug_case()
            try:
                find_elem('span[class="fb-sticky-menu__badge_type_basket count"]')
                find_elem('span[class="jlink header-v2-basket"]').click()
                browser.back()
            except NoSuchElementException:
                pass
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Проверка кнопки поделиться')
    def test_f(self):
        # поделиться
        result_list = []
        try:
            share = find_elem('div[class="dcol-0 p_product_action--item share"]')
            debug_msg = 'Поиск кнопки "Поделиться" TRUE'
            steps_list('Найти кнопку поделиться')
            allure_step_border('Кнопка "Поделиться" есть на странице', 'dcol-0 p_product_action--item share', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Поиск кнопки "Поделиться" FALSE'
            allure_step('Кнопка "Поделиться" отсутствует на странице')
            result_list.append(False)
        # debugger(debug_msg)
        try:
            move_to(share)
            debug_msg = 'Кликабельность кнопки поделиться TRUE'
            steps_list('Кликнуть в кнопку поделиться')
            allure_step('Кнопка поделиться кликабельна')
            result_list.append(True)
        except ElementClickInterceptedException:
            debug_msg = 'Кликабельность кнопки поделиться FALSE'
            allure_step('Кнопка поделиться НЕ кликабельна')
            result_list.append(False)
        sleep(0.4)
        # debugger(debug_msg)
        try:
            find_elem('ul[class="p_product_action_list"]')
            debug_msg = 'Открытие блока "Поделиться" TRUE'
            allure_step_border('Блок "Поделиться" открывается', 'p_product_action_list', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Открытие блока "Поделиться" FALSE'
            allure_step('Блок "Поделиться" НЕ открывается')
            result_list.append(False)
        steps_case()
        debug_case()
        find_elem('header[class="header -grey"]').click()
        assert result(result_list) == True


    # добавить в корзину
    @allure.story('Тело')
    @allure.title('Проверка работы кнопки добавления в корзину')
    def test_g(self):
        result_list = []
        try:
            add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            debug_msg = 'Поиск кнопки "добавкить в корзину" TRUE'
            steps_list('Найти кнопку "Добавитьв корзину"')
            allure_step_border('Кнопка "добавить в корзину" найдена', 'dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'Поиск кнопки "добавкить в корзину" FALSE'
            allure_step('Кнопка "Добавить в корзину" НЕ найдена')
            result_list.append(False)
        # debugger(debug_msg)
        try:
            icon_cart = find_elem('span[class="fb-sticky-menu__badge_type_basket count "]')
            count_cart = int(icon_cart.text)
        except NoSuchElementException:
            count_cart = None
            pass
        try:
            add_cart.click()
            steps_list('Кликнуть в кнопку "Добавить в корзину"')
            debug_msg = 'Клик в кнопку в корзину TRUE'
            allure_step('Кнопка "Добавить в корзину" кликабельна')
            result_list.append(True)
        except ElementClickInterceptedException:
            result_list.append(False)
            debug_msg = 'Клик в кнопку в корзину FALSE'
            allure_step('Кнопка "Добавить в корзину" НЕ кликабельна')
        # debugger(debug_msg)
        sleep(1)
        try:
            new_icon_cart = find_elem('span[class="fb-sticky-menu__badge_type_basket count"]')
            new_count_cart = int(new_icon_cart.text)
            allure_step_border('Количесвто товара в иконке появилось', 'header-v2-basket', '0')
            steps_list('Проверить количество товара в корзине')
            debug_msg = 'Количество товара в корзине = TRUE'
            result_list.append(True)
        except NoSuchElementException:
            result_list.append(False)
            debug_msg = 'Количество товара в корзине = FALSE'
            allure_step_border('Количесвто товара в иконке НЕ появилось', 'header-v2-basket', '0')
        # debugger(debug_msg)
        if count_cart == None:
            count = new_count_cart
        else:
            count = new_count_cart - count_cart
        if count == 1:
            result_list.append(True)
        else: result_list.append(False)
        steps_case()
        debug_case()
        sleep(1)
        assert result(result_list) == True

    # изменить количество в корзину 
    @allure.story('Тело')
    @allure.title('Проверка работы поля корзины')
    def test_h(self):
        try:
            result_list = []
            try:
                count = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            except NoSuchElementException:
                try:
                    add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
                    add_cart.click()
                except NoSuchElementException:
                    pytest.skip('На странице нет возможности добавить товар в корзину')
            old_count_value = count.get_attribute('data-quantity')
            plus = no_such_element(result_list,
                            'div[class="p_product_shoping--wrapper"] div[class="dcol-0 product_card_qty--plus-container"]',
                            'dcol-0 product_card_qty--plus-container',
                            '1',
                            'Поиск кнопки "+"',
                            'Найти кнопку "+"',
                            False)
            result_list.append(click_intercepted(plus,
                              'Кликабельность кнопк "+"',
                              'Кликнуть в кнопку "+"'))
            try:
                new_count_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('p_product_shoping--wrapper',
                                           '0',
                                           old_count_value,
                                           new_count_value,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'plus'))
            minus = no_such_element(result_list,
                            'div[class="p_product_shoping--wrapper"] button[class="btn_reset product_card_qty--btn js_minus"]',
                            'btn_reset product_card_qty--btn js_minus',
                            '3',
                            'Поиск кнопки "-"',
                            'Найти нопку "-"',
                            False)
            result_list.append(click_intercepted(minus,
                              'Кликабельность кнопк "-"',
                              'Кликнуть в кнопку "-"'))
            sleep(0.5)
            try:
                count_value_minus = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('p_product_shoping--wrapper',
                                           '0',
                                           new_count_value,
                                           count_value_minus,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'minus'))
            minus.click()
            steps_list('Повторно кликнуть в кнопку "-"')
            sleep(1)
            try:
                add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            except NoSuchElementException:
                pass
            if add_cart.is_displayed() == True:
                result_list.append(True)
                allure_step_border('Кликом в кнопку "-" можно закрыть поле корзины', 'dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width', '0')
                debug_msg = 'Закрытие поля корзины кликом в кнопку "-" True'
                steps_list('Проверить закрытие поля корзины')
            else:
                result_list.append(False)
                allure_step_border('Кликом в кнопку "-" НЕльзя закрыть поле корзины', 'p_product_block -shoping mb-24 js-shoping_block', '0')
                debug_msg = 'Закрытие поля корзины кликом в кнопку "-" False'
            # debugger(debug_msg)
            count_value_minus_second = find_elem('span[class="jlink header-v2-basket"] span[class="fb-sticky-menu__badge_type_basket count"]').text
            if int(count_value_minus_second) == 0:
                result_list.append(True)
                allure_step_border('Количесвто товара в иконке изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины "-" True'
                steps_list('Проверить изменение количества товара в иконке корзины')
            else:
                allure_step_border('Количесвто товара в иконке НЕ изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины "-" False'
                result_list.append(False) 
            # debugger(debug_msg)
            try:
                add_cart = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
            except NoSuchElementException:
                pass
            add_cart.click()
            sleep(1)
            try:
                count_input = find_elem('div[class="p_product_block -shoping mb-24 js-shoping_block"] input[type="number"]')
                debug_msg = 'Поиск поля ввода TRUE'
                allure_step_border("На странице есть поле для ввода количества товара", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
                steps_list('найти поле ввода количества товара')
                result_list.append(True)
            except NoSuchElementException:
                debug_msg = 'Поиск поля ввода FALSE'
                result_list.append(False)
                allure_step_border("На странице нет поля для ввода количества товара", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            try:
                count_input.click()
                debug_msg = "Кликабельность поля ввода количества TRUE"
                steps_list('Кликнуть в поле вовода количества товара')
                result_list.append(True)
                allure_step("Поле ввода количества товара кликабельно")
            except ElementClickInterceptedException:
                debug_msg = "Кликабельность поля ввода количества FALSE"
                result_list.append(False)
                allure_step("Поле ввода количества товара НЕ кликабельно")
            # debugger(debug_msg)
            try:
                sleep(0.4)
                count_input.send_keys(Keys.ARROW_RIGHT)
                sleep(0.4)
                count_input.send_keys(Keys.BACKSPACE)  
                sleep(0.4)
                debug_msg = "Удаление старого значения TRUE"
            except:
                debug_msg = "Удаление старого значения FALSE"
            # debugger(debug_msg)
            try:
                count_input.send_keys('2')
                steps_list('Ввести в поле значение "2"')
                debug_msg = 'Ввод значения в поле TRUE'
                result_list.append(True)
                allure_step_border("В поле можно ввести значение", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = 'Ввод значения в поле FALSE'
                result_list.append(False)
                allure_step_border("В поле НЕльзя ввести значение", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            try:
                without_focus = find_elem('header[class="header -grey"]').click()
                steps_list('Снять фокус с поля')
                debug_msg = 'Снятие фокуса с поля TRUE'
            except:
                debug_msg = 'Снятие фокуса с поля FALSE'
            count_input_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            sleep(0.3)
            if int(count_input_value) == 2:
                debug_msg = "Введенное значение сохранилось в поле TRUE"
                result_list.append(True)
                allure_step_border("Введенное значение сохранилось в поле", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            else:
                debug_msg = "Введенное значение сохранилось в поле FALSE"
                result_list.append(False)
                allure_step_border("Введенное значение НЕ сохранилось в поле", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            delete_value(count_input, 
                         "1")
            max_count = find_elem('div[class="dcol-0 product_card_qty--plus-container"] button[class="btn_reset product_card_qty--btn js_plus"]').get_attribute('data-max')
            if '.' in max_count:
                max_count = max_count.split('.')[0]
            try:
                count_input.send_keys(max_count)
                steps_list(f'Ввести в поле максимальное значение {max_count}')
                debug_msg = 'Ввод максимального значения в поле TRUE'
                result_list.append(True)
                allure_step_border(f"В поле можно ввести максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = 'Ввод максимального значения в поле FALSE'
                result_list.append(False)
                allure_step_border(f"В поле НЕльзя ввести максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            without_focus = find_elem('header[class="header -grey"]')
            without_focus.click()
            steps_list("Снять фокус с поля")
            sleep(0.3)
            max_input_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            if int(max_input_value) == int(max_count):
                steps_list(f'Проверить значение в поле {max_count}')
                debug_msg = 'Сохранение максимального значения в поле TRUE'
                result_list.append(True)
                allure_step_border(f"В поле сохранилось максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            else:
                debug_msg = 'Сохранение максимального значения в поле FALSE'
                result_list.append(False)
                allure_step_border(f"В поле НЕ сохранилось максимальное значение {max_count}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            delete_value(count_input, 
                         max_count)
            sleep(1)
            try:
                count_input.send_keys(int(max_count)+1)
                steps_list('Ввести в поле значение больше максимального')
                debug_msg = f'Ввод значения, больше максимального {int(max_count)+1} TRUE'
                result_list.append(True)
                allure_step_border(f"В поле можно ввести значение больше максимального {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = f'Ввод значения, больше максимального {int(max_count)+1} FALSE'
                result_list.append(False)
                allure_step_border(f"В поле НЕльзя ввести значение больше максимального {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            without_focus.click()
            steps_list("Снять фокус с поля")
            sleep(0.3)
            upper_max_input_value = find_elem('button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
            if int(max_input_value) == int(max_count):
                steps_list(f'Проверить значение в поле {max_count}')
                debug_msg = f'Изменение значения на максимальное {max_count} TRUE '
                result_list.append(True)
                allure_step_border(f"В поле не сохранилось значение больше максимального максимальное - {max_count} тестируемое - {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            else:
                debug_msg = f'Изменение значения на максимальное {max_count} FALSE'
                result_list.append(False)
                allure_step_border(f"В поле сохранилось значение больше максимального максимальное - {max_count} тестируемое - {int(max_count)+1}", 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            delete_value(count_input, 
                         max_count)
            try:
                count_input.send_keys('0')
                steps_list('Ввести в поле "0"')
                debug_msg = f'Ввеод в поле "0" TRUE'
                result_list.append(True)
                allure_step_border(f'В поле можно ввести "0"', 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            except ElementNotInteractableException:
                debug_msg = f'Ввеод в поле "0" FALSE'
                result_list.append(False)
                allure_step_border(f'В поле НЕльзя ввести "0"', 'dc-row p_product_shoping js-product_card_shoping js-product-in-cart', '1')
            # debugger(debug_msg)
            without_focus.click()
            steps_list("Снять фокус с поля")
            sleep(0.3)
            if add_cart.is_displayed() == True:
                result_list.append(True)
                allure_step_border('Вводом в поле "0" можно закрыть поле корзины', 'dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width', '0')
                debug_msg = 'Закрытие поля корзины вводом в поле "0" True'
                steps_list('Проверить закрытие поля корзины')
            else:
                result_list.append(False)
                allure_step_border('Вводом в поле "0" НЕльзя закрыть поле корзины', 'p_product_block -shoping mb-24 js-shoping_block', '0')
                debug_msg = 'Закрытие поля корзины вводом в поле "0" False'
            # debugger(debug_msg)
            count_value_after_null = find_elem('span[class="jlink header-v2-basket"] span[class="fb-sticky-menu__badge_type_basket count"]').text
            if int(count_value_after_null) == 0:
                result_list.append(True)
                allure_step_border('Количесвто товара в иконке изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины после ввода "0" True'
                steps_list('Проверить изменение количества товара в иконке корзины')
            else:
                allure_step_border('Количесвто товара в иконке НЕ изменилось на 0', 'header-v2-basket', '0')
                debug_msg = 'Изменение количества товара в иконке корзины после ввода "0" False'
                result_list.append(False) 
        finally:
            # debugger(debug_msg)
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Проверка фотографий')
    def test_i(self):
        try:
            result_list = []
            size_list = []
            try:
                main_photo = find_elem('div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"] img')
                result_size = main_photo.size
                result_list.append(True)
                allure_step_border('На странице есть фотография', 'p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active', '0')
                debug_msg = 'Поиск открытого фото True'
                steps_list('Найти открытое фото на странице')
            except NoSuchElementException:
                try:
                    main_photo = find_elem('div[class="p_product_swiper_main--item swiper-slide js-zoom"] [class="img"]')
                    result_size = main_photo.size
                    result_list.append(True)
                    debug_msg = 'Поиск открытого фото True'
                    allure_step_border('На странице есть фотография', 'p_product_swiper_main--item swiper-slide js-zoom', '0')
                    steps_list('Найти открытое фото на странице')
                except NoSuchElementException:
                    result_list.append(False)
                    debug_msg = 'Поиск открытого фото False'
            # debugger(debug_msg)
            steps_list('Проверить размеры фото')
            for i in result_size.values():
                if int(i) > 50:
                    result_list.append(True)
                    debug_msg = 'Размеры фото TRUE'
                else: 
                    result_list.append(False)  
                    debug_msg = 'Размеры фото False'
            # debugger(debug_msg)
            try:
                style_before = main_photo.get_attribute('style')
                allure_step_border('Фотография до фокуса', 'p_product_swiper_main--item swiper-slide js-zoom', '0')
                move_to(main_photo)
                sleep(0.5)
                steps_list('Навести фокус на фото')
                allure_step_border('Фотография после фокуса', 'p_product_swiper_main--item swiper-slide js-zoom', '0')
                style_after = main_photo.get_attribute('style')
                steps_list('ОР: Фото увеличилось')
                if style_before != style_after:
                    result_list.append(True)
                    debug_msg = 'Изменение стиля True'
                else: 
                    result_list.append(False)
                    debug_msg = 'Изменение стиля False'
                # debugger(debug_msg)
                debug_msg = 'Наведение фокуса TRUE'
            except:
                debug_msg = 'Наведение фокуса FALSE'
                allure_step('На странице нет фото товара')
            # debugger(debug_msg)     
            #переключение
            try:
                photos = find_elems('div[class="p_product_swiper_main_nav--wrapper d-none d-lg-block"] img[class="img"]') 
                debugger('Поиск блока миниатюр TRUE')
                allure_step_border('На странице есть блок с миниатюрами фото', 'p_product_swiper_main_nav--wrapper d-none d-lg-block', '0')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Поиск блока миниатюр FALSE')
                result_list.append(False)
                allure_step_border('На странице нет блока с миниатюрами фото', '"p_product_swiper_main_nav--wrapper d-none d-lg-block', '0')
            coun_photo = len(photos)
            steps_list('Посчитать количество фото')
            if coun_photo <= 1:
                allure_step('Всего одно фото')
                debugger(debug_msg = 'Фото больше 1шт. FALSE')
            else:
                sleep(0.5)
                debugger(debug_msg = 'Фото больше 1шт. TRUE')
                for i in photos:
                    try:
                        i.click()
                        allure_step('После клика по фото')
                        steps_list('Кликнуть по миниатюре фото')
                        result_list.append(True)
                        debugger(debug_msg = 'Кликабельность миниатюры TRUE')
                    except ElementNotInteractableException:
                        try:
                            button_next = find_elem('button[id="p_product_swiper_main_nav--next"]')
                            debugger(debug_msg = 'Поиск кнопки прокрутки TRUE')
                            steps_list('Найти кнопку прокрутки фото')
                            allure_step_border('Кнопка прокрутки фотографий найдена', 'icon -Icon_arrow_down ', '0')
                            result_list.append(True)
                            sleep(0.5)
                            try:
                                button_next.click()
                                steps_list('Кликнуть по кнопке прокрутки фотографий')
                                debugger(debug_msg = 'Кликабельность кнопки прокрутки TRUE')
                                allure_step('Кнопка прокрутки фотографий кликабельна"')
                                result_list.append(True)
                                try:
                                    i.click()
                                    steps_list('Кликнуть по миниатюре фото')
                                    allure_step('После клика по фото')
                                    result_list.append(True)
                                    debugger(debug_msg = 'Кликабельность миниатюры TRUE')
                                except ElementNotInteractableException:
                                    allure_step('Фотография не кликабельна')
                                    result_list.append(False)
                                    debugger(debug_msg = 'Кликабельность миниатюры FALSE')
                            except ElementNotInteractableException:
                                allure_step('Кнопка прокрутки фотографий не кликабельна"')
                                result_list.append(False)
                                debugger(debug_msg = 'Кликабельность кнопки прокрутки FALSE')
                        except NoSuchElementException:
                            allure_step('Кнопка прокрутки фотографий не найдена')
                            debugger(debug_msg = 'Поиск кнопки прокрутки FALSE')
                            result_list.append(False)
            steps_list('ОР: Фотографии переключаются')
            #открытие фото
            try:
                open_photo = find_elem('div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"]')
                steps_list('Найти открытое фото')
                debugger('Поиск открытого фото TRUE')
                result_list.append(True)
                allure_step_border('Открытое фото найдено', 
                                   'p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active', 
                                   '0')
            except NoSuchElementException:
                debugger('Поиск открытого фото FALSE')
                result_list.append(False)
                allure_step('Открытое фото НЕ найдено')
            try:
                open_photo.click()
                sleep(1)
                debugger('Кликабельность открытого фото TRUE')
                result_list.append(True)
                allure_step('Открытое фото кликабельно')
                steps_list("Кликнуть в открытое фото")
            except ElementClickInterceptedException:
                debugger('Кликабельность открытого фото FALSE')
                result_list.append(False)
                allure_step('Открытое фото НЕ кликабельно')
            try:
                photo_popup = find_elem('div[class="popup_wrapper"]')
                debugger('Поиск блока открытия фотограйи на весь экран TRUE')
            except NoSuchElementException:
                allure_step('На странице нет блока открытых фотографий')
            result_list.append(open_close_photo('open_photo', photo_popup))
            #переключение фото вперед
            try:
                button_next = find_elem('a[class="popup-next next"]')
                debugger('Поиск кнопки переключения фото TRUE')
                result_list.append(True)
                allure_step_border('Кнопка переключения фотографий next найдена', 
                                   'popup-next next', 
                                   '0')
                steps_list("Найти кнопку переключения фотографии next")
            except NoSuchElementException:
                debugger('Поиск кнопки переключения фото FALSE')
                result_list.append(False)
                allure_step('Кнопка переключения фотографий next НЕ найдена')
            try:
                idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Получить индекс активного фото TRUE')
                result_list.append(True)
                steps_list("Получить индекс активного фото")
            except NoSuchElementException:
                try:
                    idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                except NoSuchElementException:
                    debugger('Получить индекс активного фото FALSE')
                    result_list.append(False)
            try:
                button_next.click()
                sleep(1)
                debugger('Кликабельность кнопки переключения фото TRUE')
                result_list.append(True)
                allure_step('Кнопка переключения фото next кликабельна')
                steps_list("Кликнуть в кнопку переключения фотографии next")
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки переключения фото FALSE')
                result_list.append(False)
                allure_step('Кнопка переключения фото next НЕ кликабельна')
            try:
                new_idx = find_elem('div[class="slide swiper-slide slider-track-item swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Получить индекс нового активного фото TRUE')
                result_list.append(True)
                steps_list('Получить индекс нового активного фото(next)')
            except NoSuchElementException:
                try:
                    new_idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                    debugger('Получить индекс нового активного фото TRUE')
                    result_list.append(True)
                    steps_list('Получить индекс нового активного фото(next)')
                except NoSuchElementException:
                    debugger('Получить индекс нового активного фото FALSE')
                    result_list.append(False)
            steps_list('ОР: Фотографии переключаются')
            result_list.append(idx_photo(idx, new_idx))
            # Переключение фото назад
            try:
                button_prev = find_elem('a[class="popup-prev prev"]')
                debugger('Поиск кнопки переключения фото back TRUE')
                steps_list('Найти кнопку переключения фотографий назад')
                allure_step_border('Кнопка переключения фото назад найдена', 'popup-prev prev', '0')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Поиск кнопки переключения фото back FALSE')
                result_list.append(False)
                allure_step('Кнопка переключения фото назад НЕ найдена')
            try:
                button_prev.click()
                debugger('Кликабельность кнопки переключения фото back TRUE')
                steps_list('Клик в кнопку переключения фотографий назад')
                allure_step('Кнопка переключения фото назад кликабельна')
                result_list.append(True)
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки переключения фото back False')
                allure_step('Кнопка переключения фото назад НЕ кликабельна')
                result_list.append(False)
            sleep(0.3)
            try:
                idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                debugger('Получить индекс нового активного фото TRUE')
                result_list.append(True)
                steps_list('Получить индекс нового активного фото(back)')
            except NoSuchElementException:
                debugger('Получить индекс нового активного фото FALSE')
                result_list.append(False)
            steps_list('ОР: Фотографии переключаются')
            result_list.append(idx_photo(new_idx, idx))
            #Поиск миниатюр при открытых фото
            try:
                previews = find_elems('div[class="popup_preview swiper swiper-initialized swiper-vertical swiper-pointer-events swiper-backface-hidden swiper-thumbs"] a')
                steps_list('Найти миниатюры фотографий')
                allure_step_border('Миниатюры есть на странице',
                                   'popup_preview swiper swiper-initialized swiper-vertical swiper-pointer-events swiper-backface-hidden swiper-thumbs',
                                   '0')
                debugger('Поиск миниатюр TRUE')
                result_list.append(True)
            except NoSuchElementException:
                result_list.append(False)
                debugger('Поиск миниатюр FALSE')
                allure_step('Миниатюр нет на странице')
            # Переключение фотографий кликом по миниатюрам
            try:
                first_preview = previews[0]
                second_preview = previews[1]
                idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                allure_step('Получить индекс активного фото')
                second_preview.click()
                sleep(0.3)
                try:
                    new_idx = find_elem('div[class="slide swiper-slide slider-track-item active swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                except NoSuchElementException: 
                    try:
                        new_idx = find_elem('div[class="slide swiper-slide slider-track-item swiper-slide-active"]').get_attribute('data-swiper-slide-index')
                    except NoSuchElementException:
                        try:
                            new_idx = find_elem('div[class="slide swiper-slide slider-track-item swiper-slide-duplicate swiper-slide-prev swiper-slide-duplicate-next"]').get_attribute('data-swiper-slide-index')
                        except NoSuchElementException:
                            allure_step('Получить индекс активного фото после клика в миниатюру FALSE')
                            pass
                debugger('Кликабельность миниатюры TRUE')
                steps_list('Клик по второй миниатюре')
                allure_step('Кликнуть по второй миниатюре')
                result_list.append(True)
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки переключения фото back False')
                allure_step('Кнопка переключения фото назад НЕ кликабельна')
                result_list.append(False)
            result_list.append(idx_photo(new_idx, idx))
            #Закрытие фото
            try:
                close_photos = find_elems('a[class="close"]')
                close_photo = close_photos[1]
                debugger('Поиск кнопки закрытия фотографий TRUE')
                result_list.append(True)
                steps_list('Найти кнопку закрытия фотографий')
                allure_step_border('Кнопка закрытия фотографий найдена',
                                   'close',
                                   '1')
            except NoSuchElementException:
                debugger('Поиск кнопки закрытия фотографий FALSE')
                result_list.append(False)
            try:
                close_photo.click()
                debugger('Кликабельность кнопки закрытия фотографий TRUE')
                result_list.append(True)
                steps_list('Кликнуть в кнопку закрытия фотографий')
                allure_step('Кнопка закрытия фотографий кликабельна')
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки закрытия фотографий FALSE')
                result_list.append(False)
                allure_step('Кнопка закрытия фотографий НЕ кликабельна')
            result_list.append(open_close_photo('close_photo', photo_popup))
        finally:         
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Вкладки описания товара')
    def test_j(self):
        # Кнопка все характеристики
        result_list = []
        try:    
            try:
                all_characteristics = find_elem('a[class="js-scrollto all_characteristics"]')
                debugger('Поиск кнопки "Все характеристики" TRUE')
                allure_step_border('Кнопка "Все характеристики" найдена',
                                   'js-scrollto all_characteristics',
                                   '0')
                steps_list('Найти кнопку все характеристики')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Поиск кнопки "Все характеристики" FALSE')
                result_list.append(False)
                allure_step('Кнопка "Все характеристики" НЕ найдена')
            try:
                all_characteristics.click()
                debugger('Кликабельность кнопки "Все характеристики" TRUE')
                allure_step('Кнопка все характеристики кликабельна')
                result_list.append(True)
                steps_list('Кликнуть в кнопку "Все характеристики"')
            except ElementClickInterceptedException:
                debugger('Кликабельность кнопки "Все характеристики" FALSE')
                result_list.append(False)
                allure_step('Кнопка "Все характеристики" НЕ кликабельна')
            try:
                find_elem('div[class="p_product_params js-btns js-params active"]')
                debugger('Вкладка характеристики не открыта')
                allure_step_border('Вкладка характеристики активна', 
                                'p_product_params js-btns js-params active', 
                                '0')
                steps_list('ОР: Открылась вкладка "Характеристики" под блоком фото')
                result_list.append(True)
            except NoSuchElementException:
                debugger('Вкладка характеристики не открыта')
                result_list.append(False)
                allure_step('Вкладка характеристики не активна')
            
            tabs(result_list, 
                 'О товаре', 
                 'div[class="dcol-0 p_product_info--item_wrapper js-about js-btns"]', 
                 'dcol-0 p_product_info--item_wrapper js-about js-btns', 
                 'div[class="p_product_about js-btns js-about text active"]', 
                 'p_product_about js-btns js-about text active')
            tabs(result_list,
                 'Характеристики',
                 'div[class="dcol-0 p_product_info--item_wrapper js-params js-btns"]',
                 'dcol-0 p_product_info--item_wrapper js-params js-btns',
                 'div[class="p_product_params js-btns js-params active"]',
                 'p_product_params js-btns js-params active')
            tabs(result_list, 
                 'Отзывы', 
                 'div[class="dcol-0 p_product_info--item_wrapper js-reviews js-btns"]', 
                 'dcol-0 p_product_info--item_wrapper js-reviews js-btns', 
                 'div[class="p_product_reviews js-btns js-reviews text active"]', 
                 'p_product_reviews js-btns js-reviews text active')
            tabs(result_list, 
                 "Вопрос и ответ", 
                 'div[class="dcol-0 p_product_info--item_wrapper js-faq js-btns"]', 
                 'dcol-0 p_product_info--item_wrapper js-faq js-btns', 
                 'div[class="p_product_faq js-btns js-faq js-questions-and-answers text active initialized"]', 
                 'p_product_faq js-btns js-faq js-questions-and-answers text active initialized')
            # !!!!!!!!!!!!!!!ТАБ ДОКУМЕНТАЦИЯ!!!!!!!!!!!! НАЙТИ ТОВАР С НАЛИЧИЕМ ДОКУМЕНТАЦИИ!!!!!!!!!!!!!!!!
            # try:
            #     about_product = find_elem('div[class="dcol-0 p_product_info--item_wrapper js-docs js-btns"]')
            #     steps_list(f'Найти кнопку "Документация"')
            #     allure_step_border(f'Кнопка "Документация" найдена',
            #                         f'dcol-0 p_product_info--item_wrapper js-docs js-btns',
            #                         '0')
            #     debugger(f'Поиск кнопк "Документация" TRUE')
            #     result_list.append(True)
            # except NoSuchElementException:
            #     debugger(f'Поиск кнопк "Документация" FALSE')
            #     allure_step(f'Кнопа "Документация" НЕ найдена')
            #     result_list.append(False)
            # try:
            #     about_product.click()
            #     debugger(f'Кликабельность кнопки "Документация" TRUE')
            #     allure_step(f'Кнопка "Документация" кликабельна')
            #     result_list.append(True)
            #     steps_list(f'Кликнуть в кнопку "Документация"')
            # except ElementClickInterceptedException:
            #     debugger(f'Кликабельность кнопки "Документация" FALSE')
            #     result_list.append(False)
            #     allure_step(f'Кнопка "Документация" НЕ кликабельна')
        finally:                
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Сопутствующие товары')
    def test_k(self):
        try:
            result_list = []
            product_slider = no_such_element(result_list,
                            'div[class="swiper p_product_swiper--swiper js-product-swiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden"]',
                            'swiper p_product_swiper--swiper js-product-swiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden',
                            '0',
                            'Поиск блока "Сопутствующие товары"',
                            'Найти блок "Сопутствующие товары"',
                            False)
            action.move_to_element_with_offset(product_slider, -100, -100).perform()
            product_card = no_such_element(result_list, 
                            'div[class="p_product_swiper--item swiper-slide swiper-slide-active"]',
                            'p_product_swiper--item swiper-slide swiper-slide-active',
                            '0',
                            'Поиск мини карточки товара',
                            'Найти мини карточку товара',
                            False)
            old_product_card = product_card.get_attribute('aria-label')
            add_cart = no_such_element(result_list, 
                            'button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]',
                            'dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart',
                            '0',
                            'Поиск кнопки добавить в корзину',
                            'Найти кнопку добавить в корзину',
                            False)
            result_list.append(click_intercepted(add_cart,
                                'Кникабельность внопки "Добавить в корзину"',
                                'Клик в кнопку добавить в корзину'))
            sleep(0.8)
            input_block = no_such_element(result_list, 
                            'div[class="dc-row -sm f-nowrap m-0"] div[class="dcol-0"]',
                            'dc-row -sm f-nowrap m-0',
                            '0',
                            'Открытие поля корзины',
                            'ОР: Открылось поле корзины',
                            False)
            result_list.append(displayed(input_block,
                                         'Открытие коля корзины',
                                         'ОР: Поле корзины открылось'))
            old_count_value = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"] button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]').get_attribute('data-quantity')
            plus = no_such_element(result_list,
                            'div[class="dc-row -sm f-nowrap m-0"] button[class="btn_reset product_card_qty--btn js_plus"]',
                            'dc-row -sm f-nowrap m-0',
                            '0',
                            'Поиск кнопки "+"',
                            'Найти кнопку "+"',
                            False)
            result_list.append(click_intercepted(plus,
                                                 'Кликабельность кнопки "+"',
                                                 'Кликнуть в кнопку "+"'))
            sleep(0.4)
            try:
                new_count_value = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"] button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('p_product_shoping--wrapper',
                                           '0',
                                           old_count_value,
                                           new_count_value,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'plus'))
            minus = no_such_element(result_list,
                            'div[class="dc-row -sm f-nowrap m-0"]  button[class="btn_reset product_card_qty--btn js_minus"]',
                            'dc-row -sm f-nowrap m-0',
                            '0',
                            'Поиск кнопки "-"',
                            'Найти нопку "-"',
                            False)
            result_list.append(click_intercepted(minus,
                              'Кликабельность кнопк "-"',
                              'Кликнуть в кнопку "-"'))
            sleep(0.5)
            try:
                count_value_minus = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"] button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]').get_attribute('data-quantity')
            except NoSuchElementException:
                allure_step('Пропало количество товаров в корзину')
            result_list.append(count_value('dc-row -sm f-nowrap m-0',
                                           '0',
                                           new_count_value,
                                           count_value_minus,
                                           'Изменение количества товара в поле корзины',
                                           'ОР: Количество товара изменилось',
                                           'minus'))
            result_list.append(click_intercepted(minus,
                              'Кликабельность кнопк "-"',
                              'Кликнуть в кнопку "-"'))
            result_list.append(displayed(add_cart,
                                         'Поле корзины закрывается кликом в кнопку "-"',
                                         'ОР: Поле корзины закрылось'))
            result_list.append(click_intercepted(add_cart,
                                                 'Кликабельность кнопки "Добавить в корзину',
                                                 'Кликнуть в кнопку "Добавить в корзину"'))
            sleep(0.8)
            result_list.append(displayed(input_block,
                                         'Открытие коля корзины',
                                         'ОР: Поле корзины открылось'))
            entry_field = find_elem('div[class="dc-row -sm f-nowrap m-0"] div[class="dcol-0"] input[class="product_card_qty--input"]')
            delete_value(entry_field, 
                         '1')
            result_list.append(send_text(entry_field,
                                         "2",
                                         'Ввести в поле "2"'))
            delete_value(entry_field, 
                         '1')
            result_list.append(send_text(entry_field,
                                         "0",
                                         'Ввести в поле "0"'))
            sleep(0.3)
            withot_focus = find_elem('div[class="h1 p_product--title"]')
            withot_focus.click()
            no_such_element(result_list, 
                            'button[class="dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart"]',
                            'dc-btn -primary -hover -mini_icon product_card_shoping--btn js_to_cart',
                            '0',
                            'Закрытие поля корзины',
                            'Найти кнопку добавить в корзину',
                            False)
            result_list.append(displayed(add_cart,
                                         'Поле корзины закрывается при вводе в поле "0"',
                                         'ОР: Поле корзины закрыто'))
            button_next = elems_displayed(result_list, 
                                  'div[class="footer-swiper-next dc-btn cblue -icon"]',
                                  'footer-swiper-next dc-btn cblue -icon',
                                  'Поиск кнопки переключения слайдов вперед',
                                  'Найти кнопку переключения слайдов в блоке')
            result_list.append(click_intercepted(button_next,
                                          'Кликабельность кнопки переключения вперед',
                                          "Кликнуть по кнопке переключения слайдов вперед"))
            sleep(0.8)
            new_product_card = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"]').get_attribute('aria-label')
            comparison(old_product_card,
                       new_product_card,
                       'Переключение слайдов после клика в кнопку',
                       False)
            button_back = elems_displayed(result_list, 
                                        'div[class="footer-swiper-prev dc-btn cblue -icon"]',
                                        'footer-swiper-prev dc-btn cblue -icon',
                                        'Поиск кнопки переключения слайдов назад',
                                        'Найти кнопку переключения слайдов в блоке')
            result_list.append(click_intercepted(button_back,
                                          'Кликабельность кнопки переключения назад',
                                          "Кликнуть по кнопке переключения слайдов назад"))
            sleep(0.8)
            finally_product_card = find_elem('div[class="p_product_swiper--item swiper-slide swiper-slide-active"]').get_attribute('aria-label')
            comparison(new_product_card,
                       finally_product_card,
                       'Переключение слайдов после клика в кнопку',
                       False)
        finally:                
            steps_case()
            debug_case()
            assert result(result_list) == True

    
    @allure.title('Проверка кнопки бонусы')
    def test_l(self):
        try:
            bonus_before = browser.find_element(By.CSS_SELECTOR, 'div[class="price-block--bonus---tooltip-text"]').is_displayed()
        except NoSuchElementException:
            bonus_before = None
        try:
            bonus = browser.find_element(By.CSS_SELECTOR, 'div[class="dcol-0 price-block--bonus"]')
        except NoSuchElementException:
            pytest.skip('на странице нет кнопки "Бонусы"')
        bonus.click()
        with allure.step('После клика по кнопке "бонусы"'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        bonus_after = browser.find_element(By.CSS_SELECTOR, 'div[class="price-block--bonus---tooltip-text"]').is_displayed()
        self.assertNotEqual(bonus_before, bonus_after)

    @allure.title('Проверка отображения наличия товара для самовывоза')
    def test_m(self):
        scroll = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
        action.move_to_element(scroll).perform()
        pickup = browser.find_element(By.CSS_SELECTOR, 'a[class="quantity-shops js-popup-open-link"]')
        pickup.click()
        try:
            availability = browser.find_element(By.CSS_SELECTOR, 'div[class="modal__map-block-shops-inner-all-li-body-text"]')
        except NoSuchElementException:
            with allure.step('Элементы "В наличии" отсутствуют на странице'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('Элементы "В наличии" отсутствуют на странице')
        with allure.step('Элементы "В наличии"'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(availability.is_displayed(), msg = f'Наличие в магазинах не видно пользователю {date}')

    @allure.title('Проверка переключения, все, магазины, пвз')
    def test_n(self):
        result = 0
        error = ""
        try:
            button_all = browser.find_element(By.CSS_SELECTOR, 'button[data-tab="all"]')
        except NoSuchElementException:
            with allure.step('Кнопка "Все" не найдена'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_all.click()
            activ_all = browser.find_element(By.CSS_SELECTOR, 'button[class="modal__map-block-map-tabs-item js-modal-map-btn-tab is-active"]').get_attribute('data-tab')
            with allure.step('После клика во "Все"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            if activ_all == 'all':
                result += 1
            else:
                result -= 1
                error = 'Кнопка "Все" не работает'
        except ElementNotInteractableException:
            with allure.step('Кнопка "Все" кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_shops = browser.find_element(By.CSS_SELECTOR, 'button[data-tab="shop"]')
        except NoSuchElementException:
            with allure.step('Кнопка "Магазины" не найдена'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_shops.click()
            activ_all = browser.find_element(By.CSS_SELECTOR, 'button[class="modal__map-block-map-tabs-item js-modal-map-btn-tab is-active"]').get_attribute('data-tab')
            with allure.step('После клика в "Магазины"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            if activ_all == 'shop':
                result += 1
            else:
                result -= 1
                error = 'Кнопка "Магазины" не работает'
        except ElementNotInteractableException:
            with allure.step('Кнопка "Маазины" кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_pv = browser.find_element(By.CSS_SELECTOR, 'button[data-tab="pvz"]')
        except NoSuchElementException:
            with allure.step('Кнопка "Пункты выдачи" не найдена'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_pv.click()
            activ_all = browser.find_element(By.CSS_SELECTOR, 'button[class="modal__map-block-map-tabs-item js-modal-map-btn-tab is-active"]').get_attribute('data-tab')
            with allure.step('После клика в "Пункты выдачи"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            if activ_all == 'pvz':
                result += 1
            else:
                result -= 1
                error = 'Кнопка "Пункты выдачи" не работает'
        except ElementNotInteractableException:
            with allure.step('Кнопка "Пункты выдачи" кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(result == 3, msg = f'Кнопки не работают {error} {date}')

    @allure.title('Поиск блока с характеристиками рядом с фото')
    def test_o(self):      
        try:
            specifications = browser.find_element(By.CSS_SELECTOR, 'div[class="dcol-8 dcol-lg-11 dcol-xl-12 d-none d-lg-block ml-five_percents"]')
            with allure.step("Блок характеристик рядом с фото найден"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step("Блок характеристик рядом с фото не найден"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(specifications, msg = f'Характеристики не видны пользователю {date}')

    def test_z(self):
        ads()

@allure.feature('Корзина')
@allure.severity('CRITICAL')
class test_basket(unittest.TestCase):
    @allure.title('Проверка добавления товаров в корзину')
    def test_basket_items(self):
        browser.get(link)
        items_names = []
        basket_items_names = []
        count_click_item = 0
        hit_sales = browser.find_element(By.CSS_SELECTOR, 'a.hits__all-hits-link')
        hit_sales.click()
        items = browser.find_elements(By.CSS_SELECTOR, 'div.fb-catalog-listing-page__product-column')
        count_items = len(items)
        if count_items > 5:
            for i in items[:4]:
                action.move_to_element(i).perform()
                time.sleep(0.5)
                item_name = i.find_element(By.CSS_SELECTOR, 'a.fb-product-card__title-inner').text
                items_names.append(item_name)
                i.find_element(By.CSS_SELECTOR, 'div.fb-product-card__basket-button').click()
                time.sleep(1)
                count_click_item +=1
        else:
            for i in items:
                action.move_to_element(i).perform()
                time.sleep(0.5)
                item_name = i.find_element(By.CSS_SELECTOR, 'a.fb-product-card__title-inner').text
                items_names.append(item_name)
                i.find_element(By.CSS_SELECTOR, 'div.fb-product-card__basket-button').click()
                time.sleep(1)
                count_click_item +=1
        with allure.step('Выбранные товары из каталога'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        basket_button = browser.find_element(By.CSS_SELECTOR, 'span[class="jlink header-v2-basket"]').click()
        time.sleep(3)
        with allure.step('Добавленные товары в корзине'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        basket_items = browser.find_elements(By.CSS_SELECTOR, 'div.basket-item')
        count_basket_items = len(basket_items)
        for i in basket_items:
            i.find_element(By.CSS_SELECTOR, 'div.basket-item__content a[href]')
            basket_item_name = i.text.split("\n")[0]
            basket_items_names.append(basket_item_name)
        if count_click_item == count_basket_items:
            count_result = True
            error_count = 'Количество совпадает'
        else: 
            count_result = False
            error_count = 'Количество не совпадает'
        if items_names == basket_items_names:
                items_result = True
                error_name = 'Наименование совпадает'
        else:
            items_result =False
            error_name = 'Наименование не совпадает'
        
        self.assertTrue(count_result and items_result, msg = f'{error_count}, {error_name} {date}')

    @allure.title('Удаление товаров из корзины')
    def test_delete_item(self):
        basket_items_before = browser.find_elements(By.CSS_SELECTOR, 'div.basket-item')
        count_items_before = len(basket_items_before)
        with allure.step('Корзина до удаления товара'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        delete_item = browser.find_element(By.CSS_SELECTOR, 'div.basket-actions__items a.basket-actions__item')
        try:
            delete_item.click()
        except ElementNotInteractableException:
            with allure.step('Кнопка удаления товара не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('Кнопка удаления товара не кликабельна')
        time.sleep(2)
        with allure.step('Корзина после удаления 1ед. товара'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        basket_items_after = browser.find_elements(By.CSS_SELECTOR, 'div.basket-item')
        count_items_after = len(basket_items_after)
        self.assertNotEqual(count_items_before, count_items_after, msg = f'Товар не удалился из корзины {date}')

    @allure.title('Поле ввода промокода')
    def test_promo_code(self):
        with allure.step('Блок промокода, до ввода промокода'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input = browser.find_element(By.CSS_SELECTOR, '.total-coupon__block input')
        except NoSuchElementException:
            with allure.step('Поле ввода промокода не найдено'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input.click()
            with allure.step('Блок промокода, после клика в поле'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)   
        except ElementNotInteractableException:
            with allure.step('Поле не кликабельно'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input.send_keys('test')
            time.sleep(0.5)
            with allure.step('Блок промокода, после ввода промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('В поле нельзя вводить текст'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            delete_promocode = browser.find_element(By.CSS_SELECTOR, '.total-coupon__block div[class="form-input__icon i-icon"]')
        except NoSuchElementException:
            with allure.step('В поле нет кнопки удаления промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            delete_promocode.click()
            time.sleep(0.5)
            with allure.step('Блок промокода, после удаления промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('Кнопка удаления промокода не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input.send_keys('test')
            time.sleep(0.5)
        except ElementNotInteractableException:
            with allure.step('Нельзя ввести промкод после удаления'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promo_button = browser.find_element(By.CSS_SELECTOR, '.total-coupon__wrapper .total-coupon__btn')
        except NoSuchElementException:
            with allure.step('Кнопка "Применить" промокод не найдена'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promo_button.click()
        except ElementNotInteractableException:
            with allure.step('Кнопка "Применить" промокод не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        time.sleep(1)
        try:
            message = browser.find_element(By.CSS_SELECTOR, '.total-coupon__message')
            with allure.step('Блок промокода, применения промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step('Сообщение о принялии/не принятии промокода не появилось'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(message.is_displayed(), msg = f'Блок промокода не работает {date}')

    @allure.title('Добавление товара в избранное из корзины')
    def test_d_basket_button_favorite(self):
        try:
            item_buttons = browser.find_elements(By.CSS_SELECTOR, '.basket-item__actions .basket-actions__item')
            like_button = item_buttons[1]
        except NoSuchElementException:
            with allure.step('Кнопка "Отложить товар" не найдена на странице'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        with allure.step('Корзина до клика "Отложить товар"'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            like_button.click()
            time.sleep(2)
            with allure.step('Корзина после клика "Отложить товар" у первого товара'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('Кнопка "Отложить товар" не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        cont_favarit_after_first_click = browser.find_element(By.CSS_SELECTOR, 'span[class="header-v2-favourites jlink"] span[class="fb-sticky-menu__badge_type_personal count"]').text
        like_button.click()
        time.sleep(2)
        with allure.step('Корзина после клика "Отложить товар" у второго товара'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        cont_favarit_after_second_click = browser.find_element(By.CSS_SELECTOR, 'span[class="header-v2-favourites jlink"] span[class="fb-sticky-menu__badge_type_personal count"]').text
        self.assertTrue(int(cont_favarit_after_second_click) == int(cont_favarit_after_first_click) + 1, msg =f'Количество отложенного товара не изменилось {date}')

    def test_z(self):
        ads()

@allure.feature('SEO')
@allure.severity('CRITICAL')
class seo(unittest.TestCase):
    @allure.title('Проверка корректноси Title, Description, H1')
    def test_a(self):
        browser.get(link)
        city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        if city == 'Оренбург':
            pass
        else:
            button_city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').click()
            time.sleep(1)
            browser.find_element(By.CSS_SELECTOR, 'button[class="cities__results-item js-city-btn"][data-city="Оренбург"]').click()
            time.sleep(2)
        all_result = 0
        catalog = browser.find_element(By.CSS_SELECTOR, '.menu-only').click()
        categoryes = browser.find_elements(By.CSS_SELECTOR, 'a.fb-header-catalog-menu__parent-link')
        urls = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-catalog-header-group-link__link"]')
        urls_list = []
        for l in range(3):
            url_elem = urls[randint(0, len(urls) -1)]
            url = url_elem.get_attribute('href')
            urls_list.append(url.split('//')[1])
        catalog = browser.find_element(By.CSS_SELECTOR, '.menu-only').click()
        print(urls_list)
        for c in range(3):
            button_city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').click()
            time.sleep(1)
            cityes = browser.find_elements(By.CSS_SELECTOR, 'button[class="cities__results-item js-city-btn"]')
            random_city = cityes[randint(0, len(cityes) -1)]
            city = random_city.text
            random_city.click()
            #city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"] a span').text
            time.sleep(2)
            city_name = browser.current_url.split('.')[:1]
            city_name = ' '.join(city_name)
            for u in urls_list:
                new_url = f'{city_name}.{u}'
                browser.get(new_url)
                time.sleep(3)
                page_result = 0
                results = []
                title = browser.title
                description = browser.find_element(By.CSS_SELECTOR, 'meta[name="description"]').get_attribute('content')
                page_title = browser.find_element(By.CSS_SELECTOR, 'h1[id="pagetitle"]').text
                first_city_name = city.split(' ', 1)[0]
                now_city = first_city_name[:-2]
                first_page_title = page_title.split(' ', 1)[0]
                now_page_title = first_page_title[:-2]
                if (re.findall(rf'{now_city}\w+', title, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
                    results.append(True)
                else: 
                    results.append(False)
                if (re.findall(rf'{now_city}\w+', description, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', description, flags=re.IGNORECASE)):
                    results.append(True)
                else: 
                    results.append(False)
                for i in results:
                    if i == True:
                        page_result += 1
                if page_result >=2:
                    all_result +=1
                    with allure.step(f'{city}/{page_title}'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{title}\n{city}\n{page_title}', name = 'тайтл подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{description}\n{city}\n{page_title}', name = 'диск подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{results}', name = 'смотри', attachment_type=AttachmentType.TEXT)
                else:
                    with allure.step(f'{city}/{page_title} ПРОВАЛ'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{title}\n{city}\n{page_title}', name = 'тайтл подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{description}\n{city}\n{page_title}', name = 'диск подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{results}', name = 'смотри', attachment_type=AttachmentType.TEXT)
        # self.assertTrue(all_result == 9, msg = f'Тайтл или дискрипшен не верный {date}')

    @allure.title('Проверка корректноси шаблонного текста')
    def test_b(self):
        browser.get(link)
        catalog = browser.find_element(By.CSS_SELECTOR, '.menu-only').click()
        categoryes = browser.find_elements(By.CSS_SELECTOR, 'a.fb-header-catalog-menu__parent-link')
        urls = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-catalog-header-group-link__link"]')
        urls_list = []
        result_list = []
        for l in range(3):
            url_elem = urls[randint(0, len(urls) -1)]
            url = url_elem.get_attribute('href')
            urls_list.append(url.split('//')[1])
        catalog = browser.find_element(By.CSS_SELECTOR, '.menu-only').click()
        for c in range(3):
            button_city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').click()
            time.sleep(1)
            cityes = browser.find_elements(By.CSS_SELECTOR, 'button[class="cities__results-item js-city-btn"]')
            random_city = cityes[randint(0, len(cityes) -1)]
            city = random_city.text.split(' ')
            random_city.click()
            city_name = " ".join(city)
            #city = browser.find_element(By.CSS_SELECTOR, 'a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"] a span').text
            time.sleep(2)
            city_url = browser.current_url.split('.')[:1]
            city_url = ' '.join(city_url)
            for u in urls_list:
                new_url = f'{city_url}.{u}'
                browser.get(new_url)
                page_result = 0
                results = []
                page_title = browser.find_element(By.CSS_SELECTOR, 'h1#pagetitle').text.split(' ')
                page_title_name = " ".join(page_title)
                seo = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__footer seo_text"]')
                time.sleep(0.5)
                seo_text = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__footer seo_text"]').text
                asd = True
                for i in city:
                    if (re.findall(rf'{i[:-2]}\w+', seo_text, flags=re.IGNORECASE)):
                        result_list.append(True)
                    else: 
                        result_list.append(False)
                        asd = False
                for i in page_title:
                    if (re.findall(rf'{i[:-2]}\w+', seo_text, flags=re.IGNORECASE)):
                        result_list.append(True)
                    else: 
                        result_list.append(False)
                        asd = False
                action.move_to_element(seo).perform()
                if asd == True:
                    with allure.step(f'{city_name} {page_title_name}'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{city_name}\n{page_title_name}\n{seo_text}', name = 'Данные', attachment_type=AttachmentType.TEXT)
                            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                else:
                    with allure.step(f'{city_name} {page_title_name} ПРОВАЛ'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{city_name}\n{page_title_name}\n{seo_text}', name = 'Данные', attachment_type=AttachmentType.TEXT)
                            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        for r in result_list:
            if r == False:
                finally_result = False
                break
            else: 
                finally_result = True
        self.assertTrue(finally_result, msg = 'Некорректный шаблонный текст')


@allure.feature('Проверка функциональности страницы каталога')
class test_filters(unittest.TestCase):
    @allure.title('Проверка добавления в корзину')
    def test_1adding_to_cart(self):
        browser.get(link)
        menu = browser.find_element(By.CSS_SELECTOR, 'div.menu-only').click()
        catetgoryes = browser.find_elements(By.CSS_SELECTOR, 'a.fb-header-catalog-menu__parent-link')
        category = catetgoryes[randint(0, len(catetgoryes) -1)]
        category.click()
        try:
            subcategoryes = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-deep-page__category-children-column"]')
            subcategory = subcategoryes[randint(0, len(subcategoryes) -1)]
            subcategory.click()
        except NoSuchElementException:
            pass
        time.sleep(2)
        try:
            count_icon_basket = int(browser.find_element(By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_basket count "]').text)
        except NoSuchElementException:
            count_icon_basket = 0
        products = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        count_product = len(products)
        count_before = len(browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__basket-button"]'))
        if count_product > 3:
            for i in products[:3]:
                action.move_to_element(i).perform()
                i.find_element(By.CSS_SELECTOR, 'div.fb-product-card__basket-button').click()
                time.sleep(1)
        else:
            test_filters.test_1adding_to_cart(self)
            return
        with allure.step('После добавления товаров в корзину'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        time.sleep(1)
        count_basket = browser.find_element(By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_basket count"]').text
        count_basket_plus = int(count_basket) + count_icon_basket
        count_after = len(browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__basket-button"]'))
        if int(count_basket_plus) == (count_before - count_after):
            basket_result = True
        else: 
            basket_result = False
        self.assertTrue(basket_result)


# # сортировки;
    @allure.title('Сортировка по минимальной цене')
    def test_1min_price(self):
        browser.implicitly_wait(0)
        WebDriverWait(browser, 10)
        prices = []
        sort = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select"]')
        action.move_to_element(sort).perform()
        with allure.step('До сортировки по минимальной цене'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        sort.click()
        time.sleep(0.3)
        min_price_button = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__option"][data-value="cheap"]').click()
        time.sleep(2)
        try:
            WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]')))
        except TimeoutException:
            pass
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items:
            try:
                item_black_price = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]').text
                black_price = item_black_price.split('₽')[0].replace(' ', '')
                prices.append(float(black_price))
            except NoSuchElementException:
                try:
                    item_rad_price = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value fb-product-card__price-value_attention"]').text
                    rad_price = item_rad_price.split('₽')[0].replace(' ', '')
                    prices.append(float(rad_price))
                except NoSuchElementException:
                    pass
        with allure.step('После сортировки по минимальной цене'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            allure.attach(f'{prices}', name = 'Порядок товаров после сортировки от меньшей к большей', attachment_type=AttachmentType.TEXT)
        res = []
        for idx in range(1, len(prices)):
            if prices[idx - 1] <= prices[idx]:
                res.append(True)
            else:
                res.append(False)
        for r in res:
            if r == False:
                result = False
                break
            else: 
                result = True
        self.assertTrue(result, msg = f'Сортировка по минимальной цене не работает')


# по максимальной цене
    @allure.title('Сортировка по максимальной цене')
    def test_1max_price(self):
        browser.implicitly_wait(0)
        WebDriverWait(browser, 10)
        prices = []
        sort = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select"]')
        action.move_to_element(sort).perform()
        with allure.step('До сортировки по максимальной цене'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        sort.click()
        time.sleep(0.3)
        max_price_button = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__option"][data-value="expensive"]').click()
        time.sleep(2)
        try:
            WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]')))
        except TimeoutException:
            pass
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items:
            try:
                item_black_price = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]').text
                black_price = item_black_price.split('₽')[0].replace(' ', '')
                prices.append(float(black_price))
            except NoSuchElementException:
                try:
                    item_rad_price = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value fb-product-card__price-value_attention"]').text
                    rad_price = item_rad_price.split('₽')[0].replace(' ', '')
                    prices.append(float(rad_price))
                except NoSuchElementException:
                    pass
        with allure.step('После сортировки по максимальной цене'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            allure.attach(f'{prices}', name = 'Порядок товаров после сортировки от большей к меньшей', attachment_type=AttachmentType.TEXT)
        res = []
        for idx in range(1, len(prices)):
            if prices[idx - 1] >= prices[idx]:
                res.append(True)
            else:
                res.append(False)
        for r in res:
            if r == False:
                result = False
                break
            else: 
                result = True
        self.assertTrue(result, msg = f'Сортировка по максимальной цене не работает')

    # по размеру скидки 
    @allure.title('Сортировка по размеру скидки')
    def test_1discount(self):
        discount_none = 0
        discount_list =[]
        browser.implicitly_wait(0)
        WebDriverWait(browser, 10)
        sort = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select"]')
        action.move_to_element(sort).perform()
        with allure.step('До сортировки по размеру скидки'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        sort.click()
        time.sleep(0.3)
        discount_button = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__option"][data-value="discount_size"]').click()
        time.sleep(2)
        try:
            WebDriverWait(browser, 20).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'span[class="d-inline-block product_card_badge str-shield-fix -red"] span')))
            time.sleep(5)
        except TimeoutException: 
            time.sleep(5)
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        count_items = len(items)
        for i in items:
            try: 
                discount_num = i.find_element(By.CSS_SELECTOR, 'span[class="d-inline-block product_card_badge str-shield-fix -red"] span').text
                discount_num = discount_num.replace('-', '')
                discount_num = discount_num.replace('%', '')
                discount_list.append(float(discount_num))
            except NoSuchElementException:
                discount_list.append(0)
                discount_none += 1
        with allure.step('После сортировки по размеру скидки'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            allure.attach(f'{discount_list}', name = 'Порядок товаров после сортировки от большей скидки к меньшей', attachment_type=AttachmentType.TEXT)
        res = []
        for idx in range(1, len(discount_list)):
            if discount_list[idx - 1] >= discount_list[idx]:
                res.append(True)
            else:
                res.append(False)
        for r in res:
            if r == False:
                result = False
                break
            else: 
                result = True
        if discount_none == count_items:
            pytest.skip('На странице нет товаров со скидкой')
        else: pass
        self.assertTrue(result, msg = 'Сортировка по размеру скидки не работает')

    # # по популярности                      !!!!!!!!!!!!!!!!!!!Обработать отсутствие оценок!!!!!!!!!!!!!!!!!!!!!!!!
    @allure.title('Сортировка по популярности')
    def test_2popular(self):
        stars_list =[]
        browser.implicitly_wait(0)
        WebDriverWait(browser, 10)
        sort = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select"]')
        action.move_to_element(sort).perform()
        with allure.step('До сортировки по популярности'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        sort.click()
        time.sleep(0.3)
        popular_button = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__option"][data-value="popular"]').click()
        time.sleep(2)
        try:
            WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')))
        except TimeoutException:
            pass
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items:
            try: 
                popular_num = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__info-item-value "]').text
                popular_num.split(' ')[0]
                stars_list.append(float(popular_num))
            except NoSuchElementException:
                stars_list.append(0)
        with allure.step('После сортировки по популярности'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            allure.attach(f'{stars_list}', name = 'Порядок товаров после сортировки от более популярных к менее популярным', attachment_type=AttachmentType.TEXT)
        res = []
        for idx in range(1, len(stars_list)):
            if stars_list[idx - 1] >= stars_list[idx]:
                res.append(True)
            else:
                res.append(False)
        for r in res:
            if r == False:
                result = False
                break
            else: 
                result = True
        self.assertTrue(result, msg = f'Сортировка по популярности товара не работает')

    @allure.title('Фильтрация товаров по минимальной цене')  #!!!!!!!!!!!!Проработай ожидание элементов!!!!!!!!!!!!!!!!!!!!!!!!
    def test_3filters_min_price(self):
        result = 1
        browser.implicitly_wait(0)
        WebDriverWait(browser, 10)
        error = ''
        error_price = ''
        prices = []
        before_items_str = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__products-total"]').text
        items_num_before = (int(''.join(filter(str.isdigit, before_items_str))))
        filtr = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="noUi-touch-area"]')
        min_price_before = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="noUi-handle noUi-handle-upper"]').get_attribute('aria-valuemin')
        min_price_before_int = round(float(min_price_before))
        with allure.step('До изменения минимальной цены'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_min = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] input')
        input_min.click()
        delete_min = len(min_price_before)
        time.sleep(2)
        for i in range(delete_min):
            input_min.send_keys(Keys.BACKSPACE)
            time.sleep(0.5)
        input_min.send_keys(int(min_price_before_int)+1)
        time.sleep(2)
        try:
            WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')))
        except TimeoutException:
            pass
        with allure.step('После изменения минимальной цены на 1руб.'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        after_items_str = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__products-total"]').text
        items_num_after = (int(''.join(filter(str.isdigit, after_items_str))))
        min_price_after = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="noUi-handle noUi-handle-upper"]').get_attribute('aria-valuemin')
        difference_price = float(min_price_after) - float(min_price_before)
        procent = (round(items_num_after/items_num_before*100))
        delete_items_procents = 100 - procent
        if procent < 50:
            error = f'фильтр мин. цены удаляет {delete_items_procents}% товара при изменении цены на {difference_price} руб.'
            result = False
            self.assertTrue(result, msg = f'фильтр мин. цены удаляет {delete_items_procents}% товара при изменении цены на {difference_price} руб.')
        else:
            action.drag_and_drop_by_offset(filtr, 15, 0).perform()
            time.sleep(2)
            try:
                WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')))
            except TimeoutException:
                pass
            min_price_after_filtr = float(browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="noUi-handle noUi-handle-upper"]').get_attribute('aria-valuemin'))
            difference_price_after = float(min_price_after_filtr) - float(min_price_before)
            with allure.step(f'После изменения минимальной цены на {difference_price_after}руб.'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            if len(items) == 0:
                error = f'Пропали все товары при изменении минимальной цены на {difference_price_after} руб.'
                result = False
            else: 
                for i in items:
                    try:
                        item_black_price = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]').text
                        black_price = item_black_price.split('₽')[0].replace(' ', '')
                        prices.append(float(black_price))
                    except NoSuchElementException:
                        try:
                            item_rad_price = i.find_element(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value fb-product-card__price-value_attention"]').text
                            rad_price = item_rad_price.split('₽')[0].replace(' ', '')
                            prices.append(float(rad_price))
                        except NoSuchElementException:
                            pass
                count_price = 0
                for i in prices:
                    count_price += 1
                if count_price <= 1:
                    result = False
                    error = f'В выдаче остался 1 товар или все товары пропали изменении цены на {difference_price_after}'
                else:
                    for q in prices:
                        if q >= min_price_after_filtr:
                            result = True
                        else: 
                            result = False
                            error = f'В выдаче товара есть товар с ценой выше минимальной, мин - {min_price_after_filtr}, ошибочная цена в выдаче - {error_price}'
                            break
            self.assertTrue(result, msg = f'{error}')

#сравнение
    @allure.title('Добавление товаров к сравнению')
    def test_4add_comparison(self):
        comparison_check = 2
        close_filters = browser.find_element(By.CSS_SELECTOR, 'div[id="fb-filter-chips-remove-all"]').click()
        time.sleep(1)
        try:
            WebDriverWait(browser, 20).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'div[class="fb-product-card__wrapper"] div[class="fb-product-card__compare-button"]')))
        except TimeoutError:
            pass
        with allure.step('До добавления товаров к сравнению'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        butttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__wrapper"] div[class="fb-product-card__compare-button"]')
        for b in butttons[:2]:
            b.click()
            time.sleep(0.3)
        try:
            WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_compare count"]')))
        except TimeoutException:
            pass
        comparison = int(browser.find_element(By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_compare count"]').text)
        with allure.step('После добавления товаров к сравнению'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertEqual(comparison_check, comparison)


#добавление в избранное;
    @allure.title('Добавление товаров в избранное')
    def test_4add_favourites(self):
        scroll = browser.find_element(By.CSS_SELECTOR, '#pagetitle')
        move_to(scroll)
        sleep(0.2)
        favourites = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__wrapper"] div[class="fb-product-card__favorite-button"]')
        try:
            count_favourites_before = int(browser.find_element(By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_personal count "]').text)
            favourites_check = (count_favourites_before + 2)
        except NoSuchElementException:
            favourites_check = 2
        with allure.step('До добавления товаров в избранное'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        for f in favourites[:2]:
            f.click()
            time.sleep(0.3)
        time.sleep(2)
        try:
            WebDriverWait(browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_personal count"]')))
        except TimeoutException:
            time.sleep(1)
        count_favourites = int(browser.find_element(By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_personal count"]').text)
        with allure.step('После добавления товаров в избранное'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertEqual(count_favourites, favourites_check)

    @allure.title('Проверка работы фильтров')
    def test_5filters(self):
        browser.implicitly_wait(0)
        WebDriverWait(browser, 10)
        test_dot = {}
        result_list = []
        wait = WebDriverWait(browser, 5, poll_frequency=0.5, ignored_exceptions=[ElementNotVisibleException, NoSuchElementException])
        blocks = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        for f in blocks:
            if f.is_displayed() == True:
                check_box_name = None
                min_now = None
                action.move_to_element(f).perform()
                try:
                    f.find_element(By.CSS_SELECTOR, 'div[class="fb-collapse-block fb-collapse-block--open"]')
                    name_filter = f.find_element(By.CSS_SELECTOR, 'div[class="fb-collapse-block__header"] div[class="fb-collapse-block__title"]').text
                except NoSuchElementException:
                    try: # если фильтр закрыт
                        name_filter = f.find_element(By.CSS_SELECTOR, 'div[class="fb-collapse-block__header"] div[class="fb-collapse-block__title"]').text
                        f.find_element(By.CSS_SELECTOR, 'div[class="fb-collapse-block__header"]').click()
                        time.sleep(0.5)
                    except NoSuchElementException:
                        continue # случай когда нет названия фильтра!!!!!!!! 
                try: # если в фильтре не все чек-босы помещаются, развернуть полный список # есть разворачивалка
                    f.find_element(By.CSS_SELECTOR, 'div[class="fb-collapse-items__toggle fb-collapse-items__toggle-visible"]').click()
                    time.sleep(0.3)
                except NoSuchElementException:
                    # нет разворачивалки
                    pass
                try: # если чекбоксы
                    check_boxs = f.find_elements(By.CSS_SELECTOR, 'div[class="fb-checkbox"] input[type="checkbox"]+label')
                    try:
                        check_box = check_boxs[randint(0, len(check_boxs) -1)]
                        check_box_name = check_box.text
                        action.move_to_element(check_box).perform()
                        time.sleep(0.5)
                        check_box.click()
                        time.sleep(1)
                    except: 
                        try:
                            time.sleep(2)
                            check_boxs = f.find_elements(By.CSS_SELECTOR, 'div[class="fb-checkbox"] input[type="checkbox"]+label')
                            check_box = check_boxs[randint(0, len(check_boxs) -1)]
                            check_box_name = check_box.text
                            action.move_to_element(check_box).perform()
                            time.sleep(0.5)
                            check_box.click()
                            time.sleep(1)
                        except:
                            try: # пытаюсь найти ползунки т.к. не нашел чекбосы
                                swipe_dot = f.find_element(By.CSS_SELECTOR, 'div[class="noUi-touch-area"]')
                                try:
                                    action.move_to_element(swipe_dot).perform()
                                    action.drag_and_drop_by_offset(swipe_dot, 50, 0).perform()
                                    time.sleep(1)
                                    min_now = float(f.find_element(By.CSS_SELECTOR, 'div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuetext'))
                                except:
                                    with allure.step(f'Не удалось взаимодействие с ползунком {name_filter} - False'):
                                        allure.attach(browser.current_url, name = f'url', attachment_type=AttachmentType.TEXT)
                                        allure.attach(browser.get_screenshot_as_png(), name = f'Скрин', attachment_type=AttachmentType.PNG)
                                    pass # не удалось взаимодействовать с ползунком
                            except NoSuchElementException:
                                with allure.step(f'Нет активных чекбоксов в - {name_filter} - False'):
                                    allure.attach(browser.current_url, name = f'url', attachment_type=AttachmentType.TEXT)
                                    allure.attach(browser.get_screenshot_as_png(), name = f'Скрин', attachment_type=AttachmentType.PNG)
                                result_list.append(False)
                                continue #Описать работу переключателей в наличии и акции!!!!!!!!!!!!!!!!!!!!!
                except NoSuchElementException: # нет чекбоксов
                    with allure.step(f'Нет активных чекбоксов в - {name_filter} - False'):
                        allure.attach(browser.current_url, name = f'{name_filter}', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = f'{name_filter}', attachment_type=AttachmentType.PNG)
                        result_list.append(False)
                        continue
                try:
                    wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[class="fb-product-card__wrapper"]')))
                except TimeoutException: 
                    result_list.append(False)
                    close = browser.find_element(By.CSS_SELECTOR, 'div[id="fb-filter-chips-remove-all"]')
                    action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'h1[id="pagetitle"]')).perform()
                    time.sleep(0.5)
                    with allure.step(f'Не прогрузились товары после изменения фильтра {name_filter} False'):
                        allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                    close.click()
                    continue
                items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-product-card__image-slider-item swiper-slide swiper-slide-active"]')
                item = items[randint(0, len(items) -1)]
                url = item.get_attribute('href')
                try:
                    close = browser.find_element(By.CSS_SELECTOR, 'div[id="fb-filter-chips-remove-all"]')
                    action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'h1[id="pagetitle"]')).perform()
                    time.sleep(0.5)
                    close.click()
                    time.sleep(0.5)
                except NoSuchElementException:
                    if check_box_name != None:
                        name = check_box_name
                    else: name = min_now
                    with allure.step(f'После изменения фильтра {name_filter} {name} не появилось облако фильтра'):
                        allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                browser.execute_script("window.open('');")
                browser.switch_to.window(browser.window_handles[1])
                browser.get(url)
                try:
                    if check_box_name != None:
                        name = check_box_name
                    else: name = min_now
                except:
                    name = min_now
                try:
                    price = browser.find_element(By.CSS_SELECTOR, 'div[class="h1 color-red "]').text.split('₽')[0].strip(' ').replace(' ', '')
                except NoSuchElementException:
                    price = browser.find_element(By.CSS_SELECTOR, 'div[class="h1 "]').text.split('₽')[0].strip(' ').replace(' ', '')
                test_dot['Цена, ₽'] = price
                h1 = find_elem('h1[class="dcol-8"]')
                move_to(h1)
                sleep(1)
                browser.find_element(By.CSS_SELECTOR, 'a[class="js-scrollto all_characteristics"]').click()
                params = browser.find_elements(By.CSS_SELECTOR, 'li[class="mb-32"]')
                time.sleep(2)
                for p in params:
                    key = p.find_element(By.CSS_SELECTOR, 'span[class="fw400 p_product_params--key"]').text
                    value = p.find_element(By.CSS_SELECTOR, 'span[class="fw600 p_product_params--value"]').text.replace(',', '.')
                    test_dot[key] = value 
                try:
                    if name <= float(test_dot.get(name_filter)): 
                        result = True
                        in_page = test_dot.get(name_filter)
                        in_page = float(in_page)
                    else:
                        result = False
                        in_page = test_dot.get(name_filter)
                        in_page = float(in_page)
                except: 
                    if name == test_dot.get(name_filter):
                        result = True
                        in_page = test_dot.get(name_filter)
                        in_page = str(in_page)
                    else:
                        result = False  
                        in_page = test_dot.get(name_filter)
                        in_page = str(in_page)
                if type(in_page) == float:
                    massege = f'Выбрал минимальное значение: {name}'
                else:
                    massege = f'Выбрал чек-бокс: {name}'
                result_list.append(result)
                with allure.step(f'{name_filter} - {result}'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(f'фильтр - {name_filter}\n{massege}\nНа странице - {in_page}', name = 'name', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'screen', attachment_type=AttachmentType.PNG)
                browser.close()
                browser.switch_to.window(browser.window_handles[0])
            #time.sleep(2)
        for r in result_list:
            if r == False:
                glob_result = False
                break
            else:
                glob_result = True
        self.assertTrue(glob_result, msg = 'Один из фильтров не работает')

    @allure.title('Проверка работы поиска')
    def test_5search(self):
        test_a_header_elements.test_h(self)

    @allure.title("Проверка перехода на страницы пагинации")
    def test_6pagination(self):
        browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]').click()
        block = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-pagination"]')
        action.move_to_element(block).perform()
        count_items = int(block.get_attribute('data-total'))
        with allure.step('До перехода на следующую страницу'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        if count_items <= 20:
            with allure.step('На странице нет блока пагинации'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('на странице нет блока пагинации')
        pagination_pages = browser.find_elements(By.CSS_SELECTOR, 'li[class="fb-pagination__page"]')
        for p in pagination_pages[:1]:
            next_page = browser.find_element(By.CSS_SELECTOR, 'li[class="fb-pagination__page"]').get_attribute('data-page')
            p.click()
            time.sleep(1)
            with allure.step('После перехода на следующую страницу'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            new_page = browser.find_element(By.CSS_SELECTOR, 'li[class="fb-pagination__page fb-pagination__active"]').get_attribute('data-page')
        if next_page == new_page:
            result = True
        else: result = False
        self.assertTrue(result, msg = 'Переход на страницы пагинации не работает')


class zzz(unittest.TestCase):
    def test_9zzzz(self):
        browser.quit()


if __name__ == "__main__":
    unittest.main()
