import random
import re
import sys

import unittest
import time
import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotInteractableException # Взаимодействие(ввод текста...)
from selenium.common.exceptions import NoSuchElementException # Поиск
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementClickInterceptedException # Кликабельность
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from random import randint
import datetime
import allure
from allure_commons.types import AttachmentType
import requests
import json
from selenium.webdriver.common.keys import Keys
# import undetected_chromedriver as uc
from selenium.common.exceptions import JavascriptException

UA = 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36'
mobileEmulation = {"deviceMetrics": {"width": 412, "height": 914, "pixelRatio": 2.6}, "userAgent": UA}

link = "https://stroylandiya.ru/"
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-dev-shm-usage')  

browser = webdriver.Remote (command_executor = "http://selenium__standalone-chrome:4444/wd/hub", options = chrome_options) #chrome_options = chrome_options) command_executor="http://selenium__standalone-chrome:4444"
browser.set_window_position(0, 0)
browser.set_window_size(1920, 1080)
WebDriverWait(browser, 5)
action = ActionChains(browser)
datetime.datetime.utcnow()




def find_elem(selector):
    return(browser.find_element(By.CSS_SELECTOR, f'{selector}'))

def find_elems(selector):
    return(browser.find_elements(By.CSS_SELECTOR, f'{selector}'))

steps = []
debug_list = []
debug = False

def steps_list(step_msg):
    step = f'{step_msg}'
    steps.append(step)

def steps_case():
    for i in steps:
        steps_str = ('\n'.join(steps))
    with allure.step('steps'):
        allure.attach(steps_str, name = 'steps', attachment_type=AttachmentType.TEXT)
    steps.clear()

def debugger(debug_msg):
    if debug == True:
        debug_list.append(debug_msg)

def debug_case():
    if debug == True:
        for i in debug_list:
            debug_str = ('\n'.join(debug_list))
        with allure.step('debug'):
            allure.attach(debug_str, name = 'debug', attachment_type=AttachmentType.TEXT)
        debug_list.clear()
#document.querySelector("#swiper-wrapper-fc5e87843561cf1a > div.p_product_swiper--item.swiper-slide.swiper-slide-active > article > div.product_card_shoping.js-product_card_shoping.js-product-in-cart > div.dc-row.-sm.f-nowrap.m-0 > div:nth-child(2) > div > div:nth-child(3) > button")

def allure_step_border(name_step, border, index):
    try:
        browser.execute_script(f'document.getElementsByClassName("{border}")[{index}].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
    except JavascriptException:
        pass
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
    try:
        browser.execute_script(f"document.getElementsByClassName('{border}')[{index}].style.border=\"0px\";")
    except JavascriptException:
        pass
    
def allure_step(name_step):
    with allure.step(name_step):
        allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)

def result(result_list):
    for r in result_list:
        if r == False:
            test_result = False
            break
        else:
            test_result = True
    return(test_result)


def city_def():
        try:
            popup = find_elem('#city_locate')
        except NoSuchElementException:
            allure_step('В DOM нет попапа подтверждения города')
            return
        try:
            if popup.is_displayed() == True:
                find_elem('a[class="cities-locate__button cities-locate__button-ok js-cities-locate-ok"]').click()
                time.sleep(1)
        except NoSuchElementException:
            pass
        city = find_elem('a[class="js-popup-link cities__popup-link--desktop js-region-value mobile-menu-curent-city"]').text
        if city != "Оренбург":
            find_elem('div[class="city-choose-wrapper"]').click()
            find_elem('button[class="cities__results-item js-city-btn"][data-city="Оренбург"]').click()
            time.sleep(1)

def move_to(element):
    action.move_to_element(element).perform()

def idx_photo(idx, new_idx):
    if idx != new_idx:
        result = True
        debugger('Индексы разные TRUE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} TRUE')
    else:
        result = False
        debugger('Индексы одинаковые FALSE')
        allure_step(f'Индекс первой фотографии {idx}, индекс второй фотографии {new_idx} FALSE')
    return(result)

def open_close_photo(asd, photo_popup):
    if asd == "open_photo":
        if photo_popup.is_displayed() == True:
            allure_step_border('Фотографии открылись на весь экран',
                                'popup_wrapper',
                                '0')
            result = True
            debugger('открытие фотографий на весьэкран TRUE')
            steps_list('ОР: фотография развернулась на весь экран')
        else:
            allure_step('Фотографии не открылись на весь экран')
            result = False
            debugger('открытие фотографий на весьэкран FALSE')
    elif asd == "close_photo":
        if photo_popup.is_displayed() == False:
            allure_step('Фотографии закрылись')
            result = True
            debugger('Закрытие фоторгафий TRUE')
            steps_list('ОР: Фотографии закрылись')
        else:
            allure_step('Фотографии не закрылись')
            result = False
            debugger('Закрытие фоторгафий FALSE')
    return(result)


def tabs(result_list, name, button, border, block, block_border):
    try:
        tab = find_elem(f'{button}')
        move_to(tab)
        steps_list(f'Найти кнопку "{name}"')
        allure_step_border(f'Кнопка "{name}" найдена',
                            f'{border}',
                            '0')
        debugger(f'Поиск кнопк "{name}" TRUE')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Поиск кнопк "{name}" FALSE')
        allure_step(f'Кнопа "{name}" НЕ найдена')
        result_list.append(False)
    try:
        tab.click()
        time.sleep(1)
        debugger(f'Кликабельность кнопки "{name}" TRUE')
        allure_step(f'Кнопка "{name}" кликабельна')
        result_list.append(True)
        steps_list(f'Кликнуть в кнопку "{name}"')
    except ElementClickInterceptedException:
        debugger(f'Кликабельность кнопки "{name}" FALSE')
        result_list.append(False)
        allure_step(f'Кнопка "{name}" НЕ кликабельна')
    try:
        tab_block = find_elem(f'{block}')
        move_to(tab_block)
        time.sleep(0.4)
        debugger(f'Вкладка {name} открыта')
        allure_step_border(f'Вкладка "{name}" активна', 
                        f'{block_border}', 
                        '0')
        steps_list(f'ОР: Открылась вкладка "{name}" под блоком фото')
        result_list.append(True)
    except NoSuchElementException:
        debugger(f'Вкладка {name} не открыта')
        result_list.append(False)
        allure_step(f'{name} не активна')


def no_such_element(result_list, css_sel, border, idx, msg, step, res):
    try:
        elem = find_elem(f'{css_sel}')
        try:
            move_to(elem)
        except:
            pass
        time.sleep(0.3)
        debugger(f'{msg} TRUE')
        allure_step_border(f'{msg} TRUE',
                            f'{border}',
                            f'{idx}')
        steps_list(f'{step}')
        result_list.append(True)
        return(find_elem(f'{css_sel}'))
    except NoSuchElementException:
        if res == True:
            result_list.append(True)
            allure_step(f'{msg} TRUE')
            debugger(f'{msg} TRUE')
            steps_list(f'{step}')
        else:
            result_list.append(False)
            allure_step(f'{msg} FALSE')
            debugger(f'{msg} FALSE')
        return(None)
    
    

def click_intercepted(elem, msg, step):
    try:
        elem.click()
        time.sleep(0.5)
        debugger(f'{msg} TRUE')
        allure_step(f'{msg} TRUE')
        result = True
        steps_list(f'{step}')
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == True:
        result = True
        debugger(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
        result = False
        allure_step(f'{msg} FALSE')
    return(result)

def not_displayed(elem, msg, step):
    steps_list(f'{step}')
    if elem.is_displayed() == False:
        debugger(f'{msg} TRUE')
        result = True
        allure_step(f'{msg} TRUE')
    else: 
        result = False
        debugger(f'{msg} FALSE')
    return(result)

def elements(css_sel, idx):
    elems = find_elems(f'{css_sel}')
    if idx == 'random':
        elem = elems[randint(0, len(elems) -1)]
    else:
        elem = elems[idx]
    return(elem)

def send_text(elem, text, msg):
    try:
        elem.send_keys(f'{text}')
        debugger(f'{msg} TRUE')
        allure_step(f'В поле ввода можно ввести {text} True')
        steps_list(f'Ввести в поле "{text}"')
        result = True
    except ElementClickInterceptedException:
        debugger(f'{msg} FALSE')
        allure_step(f'В поле ввода нельзя ввести {text} False')
        result = False
    return(result)

def count_value(border, idx, old_count_value, new_count_value, msg, step, sign):
    if sign == "plus":
        if int(new_count_value) - int(old_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    else:
        if int(old_count_value) - int(new_count_value) == 1:
            result = True
            allure_step_border(f'{msg} TRUE',
                                f'{border}', 
                                f'{idx}')
            debug_msg = f'{msg} True'
            steps_list(f'{step}')
        else:
            debug_msg = f'{msg} False'
            result = False
            allure_step_border(f'{msg} FALSE',
                                f'{border}', 
                                f'{idx}')
    return(result)
def sleep(sec):
    time.sleep(sec)

def delete_value(block_input, max_count):
    for r in range(len(max_count)+1):
        block_input.send_keys(Keys.ARROW_RIGHT)
        sleep(0.3)
    for d in range(len(max_count)+1):
        block_input.send_keys(Keys.BACKSPACE)  
        sleep(0.3)


def cityes(old_city_name, new_city_name):
    if old_city_name != new_city_name:
        result = True
        allure_step(f'Город изменился был - {old_city_name} стал - {new_city_name} TRUE') 
    else:
        result = False
        allure_step(f'Город не изменился был - {old_city_name} стал - {new_city_name} FALSE') 
    return(result)

def city_in_title(city, title_page):
    first_city_name = city.split(' ', 1)[0]
    now_city = first_city_name[:-2]
    if (re.findall(rf'{now_city}\w+', title_page, flags=re.IGNORECASE)):
        result = True
        with allure.step(f'Название города в тайтле страницы TRUE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    else:
        result = False
        with allure.step(f'Название города в тайтле страницы FALSE'):
            allure.attach(f'Тайтл - {title_page}\nГород - {city}', name = 'title', attachment_type=AttachmentType.TEXT)
    return(result)

def elems_displayed(result_list, css_sel, border, msg, step):
    try:
        elems = find_elems(f'{css_sel}')
    except NoSuchElementException:
        allure_step('На странице нет кнопок переключения блока')
        result_list.append(True)
        return
    idx = -1
    disp = 0
    for e in elems:
        idx += 1
        if e.is_displayed() == True:
            elem = e
            #move_to(elem)
            time.sleep(0.3)
            debugger(f'{msg} TRUE')
            allure_step_border(f'{msg} TRUE',
                                f'{border}',
                                f'{idx}')
            steps_list(f'{step}')
            result_list.append(True)
            break
    return(elem)
        
    
        
    # for d in elems:
    #     if d.is_displayed() == True:
    #         disp += 1
    # if disp <= 6:
    #     allure_step('На странице нет активных кнопок переключения блока')
    #     result_list.append(True)

def comparison(first, second, msg, equal):
    if equal == True:
        if first == second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    elif equal == False:
        if first != second:
            allure_step(f'{msg} TRUE')
            result = True
        else:
            allure_step(f'{msg} FALSE')
            result = False
    return(result)

def banner_url(page, name):
    url = browser.current_url
    if page in url:
        result = True
    else: result = False
    #allure_step(f'После клика в {name}')
    return(result)

def back():
    browser.back()  

def h1_title(name):
    page_title = find_elem('#pagetitle').text
    if name in page_title:
        result = True
    else: result = False
    allure_step_border(f'h1 title страницы, после клика в {name}',
                       'fb-font-gilroy',
                       '0')
    return(result)  

def rel_bunner_page(with_city):
    if with_city == True:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_city}\w+', title, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    else:
        title = browser.title
        try:
            page_title = find_elem('#pagetitle').text
        except NoSuchElementException:
            page_title = find_elem('h1[class="dcol-8"]').text
        city = find_elem('div[class="city-choose-wrapper"]').text
        first_city_name = city.split(' ', 1)[0]
        now_city = first_city_name[:-2]
        now_page_title = page_title[:-2]
        if (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
            result = True
            with allure.step(f'Город и Н1 в тайтле страницы TRUE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
        else:
            result = False
            with allure.step(f'Город и Н1 в тайтле страницы FALSE'):
                allure.attach(f'{browser.current_url}', name = f'url', attachment_type=AttachmentType.TEXT)
                allure.attach(f'Тайтл - {title}\nГород - {city}\nh1 страницы - {page_title}', name = 'title', attachment_type=AttachmentType.TEXT)
                try:
                    browser.execute_script(f'document.getElementsByClassName("fb-font-gilroy shares_block")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
                except JavascriptException:
                    pass
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                try:
                    browser.execute_script(f"document.getElementsByClassName('fb-font-gilroy shares_block')[0].style.border=\"0px\";")
                except JavascriptException:
                    pass
    return(result)

link = "https://stroylandiya.ru/"

def test_status_code():
    dat = {'q':'goog'}
    response = requests.get(link, params=dat, headers={'User-Agent': 'YandexDirect'})
    if response.status_code != 200:
        sys.exit(f'Статус код {test_status_code()}')
    assert response.status_code == 200



@allure.feature('Главная страница')
class test_a_header_elements(unittest.TestCase):
    @allure.story('Хедер')
    @allure.title('Выбор города')
    def test_a(self): #_choosing_city  
        browser.get(link)  
        result_list = []
        city_def()
        try:
            city = no_such_element(result_list,
                                'div[class="city-choose-wrapper"]',
                                'city-choose-wrapper',
                                '0',
                                'Поиск кнопки выбора города',
                                'Найти кнопку выбора города',
                                False)
            old_city_name = city.text
            title_page = browser.title
            result_list.append(city_in_title(old_city_name,
                          title_page))
            result_list.append(displayed(city,
                                        "Отображение кнопки города на странице",
                                        'ОР: Кнопка город отображается на странице'))
            result_list.append(click_intercepted(city,
                                                'Кликабельность кнопки выбора города',
                                                'Кликнуть в кнопку выбора города'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="popup__body cities__body"]',
                                    'popup__body cities__body',
                                    '0',
                                    'Открытие попапа со списком городов',
                                    'ОР: Попап открылся',
                                    False)
            rnd_city = elements('button[class="cities__results-item js-city-btn"]',
                                'random')
            move_to(rnd_city)
            city_name = rnd_city.text
            result_list.append(click_intercepted(rnd_city,
                                                f'Кликабельность города {city_name} в списке',
                                                f'Кликнуть в город {city_name}'))
            sleep(1)
            new_city = no_such_element(result_list,
                                'div[class="city-choose-wrapper"]',
                                'city-choose-wrapper',
                                '0',
                                'Поиск кнопки выбора города',
                                'Найти кнопку выбора города',
                                False)
            new_city_name = new_city.text
            result_list.append(cityes(new_city_name,
                                       old_city_name))
            new_title_page = browser.title
            result_list.append(city_in_title(new_city_name,
                                            new_title_page))
            find_elem('div[class="city-choose-wrapper"]').click()
            sleep(0.5)
            form_input = no_such_element(result_list,
                                        'input[class="cities__form-input js-search-city"]',
                                        'cities__form-input js-search-city',
                                        '0',
                                        'Наличие поля ввода названия города',
                                        'Найти поле ввода названия города',
                                        False)
            result_list.append(click_intercepted(form_input,
                                                'Кликабельность поля ввода названия города',
                                                'Кликнуть в поле поиска города'))
            result_list.append(send_text(form_input,
                                        old_city_name,
                                        f'Ввести в поле {old_city_name}'))
            sleep(0.3)
            search_city =  no_such_element(result_list, 
                            'div[class="cities__results js-cities-list"] button[class="cities__results-item js-city-btn"]',
                            'cities__results js-cities-list',
                            '0',
                            'Есть результаты поиска города',
                            f'ОР: В результаттах поиска есть {old_city_name}',
                            False)
            result_list.append(click_intercepted(search_city,
                                                'Кликабельность результатов поиска города',
                                                'Кликнуть в первый результат поиска'))
            sleep(0.5)
            result_list.append(cityes(new_city_name,
                                       old_city_name))
            finally_page_title = browser.title
            result_list.append(city_in_title(old_city_name, 
                                             finally_page_title))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Банер над хедером')
    def test_b(self):
        result_list = []
        city_def()
        try:
            bunner = no_such_element(result_list,
                            'div[class="banner CROP TOP_HEADER hidden-sm hidden-xs"]',
                            'banner CROP TOP_HEADER hidden-sm hidden-xs',
                            '0',
                            'Поиск баннера над хедером',
                            'Найти баннер над хедером',
                            False)
            result_list.append(displayed(bunner,
                                        'Баннер отображается на странице',
                                        'ОР: Баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                'Кликабельность баннера над хедером',
                                                'Кликнуть в баннер над хедером'))
            sleep(1)
            result_list.append(rel_bunner_page(False))
            back()
            sleep(1)
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Магазины')
    def test_c(self):
        result_list= []
        try:
            shops = no_such_element(result_list,
                                    'a[class="shop_link"]',
                                    "shop_link",
                                    '0',
                                    'Поиск кнопки "Магазины"',
                                    'Найти кнопку "Магазины"',
                                    False)
            result_list.append(click_intercepted(shops,
                                                'Кликабельность кнопки "Магазины"',
                                                'Кликнуть в кнопку "Магазины"'))
            sleep(1)
            result_list.append(banner_url('shops',
                                          'Магазины'))
            back()
            sleep(1)
        
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Статус заказа')
    def test_d(self):
        result_list = []
        try:
            status = no_such_element(result_list,
                                    'a[class="button-header"]',
                                    'button-header',
                                    '0',
                                    'Поиск кнопки "Статус заказа"',
                                    'Найти кнопку статус заказа',
                                    False)
            result_list.append(click_intercepted(status,
                                                'Кликабельность кнопки "Статус заказа"',
                                                'Кликнуть в кнопку "Статус заказа"'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Поиск попапа авторизации',
                                    'Найти попап авторизации',
                                    False)
            result_list.append(displayed(popup,
                                        'Отображение попапа авторизации на странице',
                                        'ОР: Попап авторизации виден пользователю'))
            close_button = no_such_element(result_list,
                                           'div[class="auth-popup auth-popup-mobile fancybox-content"] a[class="popup__close popup__close-x close-popup"]',
                                           "popup__close popup__close-x close-popup",
                                           '2',
                                           'Поиск кнопки закрытия попапа',
                                           'Найти кнопку закрытия попапа',
                                           False)
            result_list.append(displayed(close_button,
                                        'Отображение кнопки закрытия попапа на странице',
                                        'ОР: Кнопка закрытия попапа видна пользователю'))
            result_list.append(click_intercepted(close_button,
                                                'Кликабельность кнопки закрытия попапа',
                                                'Кликнуть в кнопку закрытия попапа'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Закрытие попапа авторизации',
                                    'ОР: Попап авторизации не виден пользователю',
                                    True) 
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Номер телефона')
    def test_e(self):
        result_list = []
        try:
            phone = no_such_element(result_list,
                                    'a[class="header-phone"]',
                                    'header-phone',
                                    '0',
                                    'Поиск номера телефона',
                                    'Найти номер телефона',
                                    False)
            result_list.append(displayed(phone,
                                         'Отображение номера телефона на странице',
                                         'ОР: Номер телефона виден пользователю'))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    


    @allure.story('Хедер')
    @allure.title('Логотип')
    def test_f(self):
        result_list = []
        city_def()
        try:
            logo = find_elem('a[class="header-v2-logo"]')
            debug_msg = 'Поиск логотипа TRUE'
            steps_list('Найти логотип на странице')
            allure_step_border('Логотип найден', 'header-v2-logo', '0')
            result_list.append(True)
        except NoSuchElementException:
            debug_msg = 'поиск логотипа FALSE'
            allure_step('Логотип не найден')
            result_list.append(False)
        debugger(debug_msg)
        try:
            logo.click()
            debug_msg = 'Клик в логотип TRUE'
            allure_step('Логотип кликабелен')
            result_list.append(True)
            steps_list("Кликнуть в логотип")
        except ElementClickInterceptedException:
            debug_msg = 'Клик в логотип FALSE'
            allure_step('Логотип не Кликабелен')
            result_list.append(False)
        debugger(debug_msg)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Проверка каталога')
    def test_g(self):
        result_list = []
        menu = no_such_element(result_list,
                'div[class="menu-only"]',
                'menu-only',
                '0',
                'Поиск кнопки "Каталог"',
                'Найти кнопку "Каталог"',
                False)
        result_list.append(click_intercepted(menu,
                           'Кликабельность кнопки "Каталог"', 
                           'Кликнуть в кнопку "Каталог"'))
        sleep(0.3)
        catalog_block = no_such_element(result_list,
                        'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]',
                        'fb-header-catalog-menu fb-header-catalog-menu_opened',
                        '0',
                        'Открытие каталога',
                        'ОР: Каталог открылся',
                        False)
        close_menu = no_such_element(result_list,
                        'div[class="menu-only"]',
                        'menu-only',
                        '0',
                        'Поиск кнопки закрытия каталога',
                        'Найти кнопку закрытия каталога',
                        False)
        result_list.append(click_intercepted(close_menu,
                            'Кликабельность кнопки закрытия каталога', 
                            'Кликнуть в кнопку закрытия каталога'))
        sleep(0.5)
        no_such_element(result_list,
                        'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]',
                        'fb-header-catalog-menu fb-header-catalog-menu_opened',
                        '0',
                        'Закрытие каталога',
                        'ОР: Каталог закрылся',
                        True)
        steps_case()
        debug_case()
        assert result(result_list) == True
    
    @allure.story('Хедер')
    @allure.title('Проверка поиска')
    def test_h(self):
        result_list = []
        search = no_such_element(result_list,
                        'input[class="search-input js-digi-search-input"]',
                        'search-input js-digi-search-input',
                        '0',
                        'Поиск блока "Поиска"',
                        'Найти блок поиска',
                        False)
        result_list.append(click_intercepted(search,
                          'Кликабельность поля поиска',
                          'Кликнуть в поле поиска'))
        search_results = no_such_element(result_list,
                        'div[class="search-results__wrapper"]',
                        'search-results__wrapper',
                        '0',
                        'Поиск блока результатов поиска',
                        'Найти блок поиска',
                        False)
        result_list.append(displayed(search_results,
                                     "Отображение блока поиска на странице",
                                     'Проверить отображение блока поиска на странице'))
        result_list.append(send_text(search,
                                     'Ламинат',
                                     'Ввести текст в поле поиска'))
        sleep(0.5)
        popular_searches = no_such_element(result_list,
                        'div[class="search-results__search-container search-results__popular-searches js-popular-searches"]',
                        'search-results__search-container search-results__popular-searches js-popular-searches',
                        '0',
                        'Поиск блока "Часто ищу"',
                        'Найти блок "Часто ищу"',
                        False)
        result_list.append(displayed(popular_searches,
                                     'Отображение блока "Часто ищу" на странице',
                                     'ОР: Блок "Часто ищу" отображается на странице'))
        search_categories = no_such_element(result_list,
                        'div[class="search-results__search-container search-results__categories js-search-categories"]',
                        'search-results__search-container search-results__categories js-search-categories',
                        '0',
                        'Поиск блока "Категории"',
                        'Найти блок "Категории"',
                        False)
        result_list.append(displayed(search_categories,
                                     'Отображение блока "Категории" на странице',
                                     'ОР: Блок "Категории" отображается на странице'))
        search_products = no_such_element(result_list,
                        'div[class="search-results__items-container js-search-products"]',
                        'search-results__items-container js-search-products',
                        '0',
                        'Поиск блока "Популярные товары"',
                        'Найти блок "Популярные товары"',
                        False)
        result_list.append(displayed(search_products,
                                     'Отображение блока "Популярные товары" на странице',
                                     'ОР: Блок "Популярные товары" отображается на странице'))
        search_submit_btn = no_such_element(result_list,
                        'button[class="search-results__all-results-btn js-search-submit-btn"]',
                        'search-results__all-results-btn js-search-submit-btn',
                        '0',
                        'Поиск кнопки "Все результаты"',
                        'Найти кнопку "Все результаты"',
                        False)
        result_list.append(displayed(search_submit_btn,
                                     'Отображение кнопки "Все результаты" на странице',
                                     'ОР: Кнопка "Все результаты" отображается на странице'))
        search_btn_desktop = no_such_element(result_list,
                        'button[class="search-input-digi-btn js-digi-search-btn-desktop"]',
                        'search-input-digi-btn js-digi-search-btn-desktop',
                        '0',
                        'Поиск кнопки "Найти"',
                        'Найти кнопку "Найти"',
                        False)
        result_list.append(displayed(search_btn_desktop,
                                     'Отображение кнопки "Найти" на странице',
                                     'ОР: Кнопка "Найти" отображается на странице'))
        reset_btn_desktop = no_such_element(result_list,
                        'button[class="search-input-digi-reset-btn js-digi-reset-btn-desktop"]',
                        'search-input-digi-reset-btn js-digi-reset-btn-desktop',
                        '0',
                        'Поиск кнопки удаления текста',
                        'Найти кнопку удаления текста',
                        False)
        result_list.append(displayed(reset_btn_desktop,
                                     'Отображение кнопки удаления текста на странице',
                                     'ОР: Кнопка удаления текста отображается на странице'))
        result_list.append(click_intercepted(reset_btn_desktop,
                          'Кликабельность кнопки удаления текста',
                          'Кликнуть в кнопку удаления текста'))
        try:
            header_grey = find_elem('header[class="header -grey"]').click()
        except:
            allure_step('не удалось кликнуть в хедер для снятия фикуса с поиска')
        result_list.append(not_displayed(search_results,
                                         'Закрытие окна поиска',
                                         'ОР: Окно результатов поиска закрыто'))
        steps_case()
        debug_case()
        assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Акции')
    def test_j(self):
        result_list = []
        try:
            stock = no_such_element(result_list,
                                    'a[class="header-v2-action"]',
                                    'header-v2-action',
                                    '0',
                                    'Поиск кнопки "Акции"',
                                    'Найти кнопку "Акции"',
                                    False)
            result_list.append(displayed(stock,
                                        'Отображении кнопки "Акции" на странице',
                                        'ОР: Кнопка "Акции" Видна пользователю'))
            result_list.append(click_intercepted(stock,
                            'Кликабельность кнопки "Акции"',
                            'Кликнуть в кнопку "Акции"'))
            sleep(0.5)
            page_title = browser.title
            result_list.append(banner_url('actions',
                                        'Акции'))
            result_list.append(h1_title('Акции'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
        

    @allure.story('Хедер')
    @allure.title('Вход')
    def test_k(self):
        result_list = []
        try:
            entrance = no_such_element(result_list,
                                    'a[class="header-v2-enter "]',
                                    'header-v2-enter ',
                                    '0',
                                    'Поиск кнопки "Вход"',
                                    'Найти кнопку "Вход"',
                                    False)
            result_list.append(click_intercepted(entrance,
                                                'Кликабельность кнопки "Вход"',
                                                'Кликнуть в кнопку "Вход"'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Поиск попапа авторизации',
                                    'Найти попап авторизации',
                                    False)
            result_list.append(displayed(popup,
                                        'Отображение попапа авторизации на странице',
                                        'ОР: Попап авторизации виден пользователю'))
            close_button = no_such_element(result_list,
                                           'div[class="auth-popup auth-popup-mobile fancybox-content"] a[class="popup__close popup__close-x close-popup"]',
                                           "popup__close popup__close-x close-popup",
                                           '2',
                                           'Поиск кнопки закрытия попапа',
                                           'Найти кнопку закрытия попапа',
                                           False)
            result_list.append(displayed(close_button,
                                        'Отображение кнопки закрытия попапа на странице',
                                        'ОР: Кнопка закрытия попапа видна пользователю'))
            result_list.append(click_intercepted(close_button,
                                                'Кликабельность кнопки закрытия попапа',
                                                'Кликнуть в кнопку закрытия попапа'))
            sleep(0.5)
            popup = no_such_element(result_list,
                                    'div[class="auth-popup__body auth-popup-mobile__body"]',
                                    'auth-popup__body auth-popup-mobile__body',
                                    '0',
                                    'Закрытие попапа авторизации',
                                    'ОР: Попап авторизации не виден пользователю',
                                    True) 
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Сравнение')
    def test_l(self):
        result_list = []
        try:
            comparison_button = no_such_element(result_list,
                                                'span[class="basket-link jlink compare header-v2-compare "]',
                                                "basket-link jlink compare header-v2-compare ",
                                                '0',
                                                'Поиск кнопки "Сравнение"',
                                                'Найти кнопку сравнение',
                                                False)
            result_list.append(displayed(comparison_button,
                                        'Отображение кнопки "Сравнение" на странице',
                                        'ОР: Кнопка "Сравнение" видна пользователю'))
            result_list.append(click_intercepted(comparison_button,
                                                'Кликабельность кнопки "Сравнение"',
                                                'Кликнуть в кнопку "Сравнение"'))
            result_list.append(banner_url('compare',
                                        'Сравнение'))
            result_list.append(h1_title('Сравнение товаров'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Хедер')
    @allure.title('Избранное')
    def test_m(self):
        result_list = []
        try:
            favorite = no_such_element(result_list,
                                                'span[class="header-v2-favourites jlink"]',
                                                "header-v2-favourites jlink",
                                                '0',
                                                'Поиск кнопки "Избранное"',
                                                'Найти кнопку "Избранное"',
                                                False)
            result_list.append(displayed(favorite,
                                        'Отображение кнопки "Избранное" на странице',
                                        'ОР: Кнопка "Избранное" видна пользователю'))
            result_list.append(click_intercepted(favorite,
                                                'Кликабельность кнопки "Избранное"',
                                                'Кликнуть в кнопку "Избранное"'))
            result_list.append(h1_title('Отложенные товары'))
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
 
    @allure.story('Хедер')
    @allure.title('Корзина')
    def test_n(self):
        result_list = []
        try:
            basket = no_such_element(result_list,
                                                'span[class="jlink header-v2-basket"]',
                                                "jlink header-v2-basket",
                                                '0',
                                                'Поиск кнопки "Корзина"',
                                                'Найти кнопку "Корзина"',
                                                False)
            result_list.append(displayed(basket,
                                        'Отображение кнопки "Корзина" на странице',
                                        'ОР: Кнопка "Корзина" видна пользователю'))
            result_list.append(click_intercepted(basket,
                                                'Кликабельность кнопки "Корзина"',
                                                'Кликнуть в кнопку "Корзина"'))
            result_list.append(banner_url('cart',
                                        'Корзина'))
            page_title = find_elem('h1[class="basket-d__heading"]').text
            if 'Корзина' in page_title:
                 result_list.append(True)
            else:  result_list.append(False)
            allure_step_border(f'h1 title страницы, после клика в "Корзина"',
                            'basket-d__heading',
                            '0')
            back()
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

@allure.feature('Главная страница')
class test_b_body_elements(unittest.TestCase):
    @allure.story('Тело')
    @allure.title('Главный баннер')
    def test_a(self):
        result_list = []
        try:
            bunner = no_such_element(result_list,
                                     'div[class="main__banners-main"]',
                                     'main__banners-main',
                                     '0',
                                     'Поиск главного баннера',
                                     'Найти главный баннер',
                                     False)
            result_list.append(displayed(bunner,
                                         'Отображение главного баннера на странице',
                                         'ОР: Главный баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                 'Кликабельность главноего баннера',
                                                 'Кликнуть в главный баннер'))
            result_list.append(banner_url('action',
                                         'Главный баннер'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            navi = no_such_element(result_list,
                                   'div[class="main__banners-main-navigation"]',
                                   "main__banners-main-navigation",
                                   '0',
                                   'Поиск блока управления слайдами',
                                   'Найти блок управления слайдами баннера',
                                   False)
            result_list.append(displayed(navi,
                                         'Отображение панели переключения слайдов в главном баннере',
                                         'ОР: Панель управления слайдами видна пользователю'))
            button_next = no_such_element(result_list,
                                   'div[class="js-swiper__main-banner-next main__banners-main-navigation-next"]',
                                   "js-swiper__main-banner-next main__banners-main-navigation-next",
                                   '0',
                                   'Поиск кнопки переключения слайдов вперед',
                                   'Найти кнопку переключения слайдов вперед',
                                   False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов вперед видна пользователю'))
            old_activ_slide = find_elem('span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_activ_slide = find_elem('span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
            result_list.append(comparison(old_activ_slide,
                                          new_activ_slide,
                                          'Переключение слайдов вперед',
                                          False))
            button_back = no_such_element(result_list,
                                   'div[class="js-swiper__main-banner-prev main__banners-main-navigation-prev"]',
                                   "js-swiper__main-banner-prev main__banners-main-navigation-prev",
                                   '0',
                                   'Поиск кнопки переключения слайдов назад',
                                   'Найти кнопку переключения слайдов назад',
                                   False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов назад видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_activ_slide = find_elem('span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
            result_list.append(comparison(finally_activ_slide,
                                          new_activ_slide,
                                          'Переключение слайдов назад',
                                          False))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    
    @allure.story('Тело')
    @allure.title('Товар дня')
    def test_b(self):
        result_list = []
        try:
            block = no_such_element(result_list,
                                    'div[class="main__banners-items"]',
                                    'main__banners-items',
                                    '0',
                                    'Поиск блока "Товар дня"',
                                    'Найти блок "Товар дня"',
                                    False)
            result_list.append(displayed(block,
                                         'Отображение блока "Товар дня"',
                                         'ОР: Блок "Товар дня" отображается на странице'))
            item_id = find_elem('div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(click_intercepted(block,
                                                 'Кликабельность блока "Товар дня"',
                                                 'Кликнуть в блок "Товар дня"'))
            page_item_id = find_elem('button[class="p_product_action--btn js_compare_item"]').get_attribute('data-item')
            back()
            result_list.append(comparison(item_id,
                                          page_item_id,
                                          'Релевантность перехода по клику в "Товар дня"',
                                          True))
            button_next = no_such_element(result_list,
                                          'div[class="js-swiper__item-banner-next main__banners-items-navigation-btn main__banners-items-navigation-next"]',
                                          "js-swiper__item-banner-next main__banners-items-navigation-btn main__banners-items-navigation-next",
                                          '0',
                                          'Поиск кнопки переключения слайдов "Товар дня" вперед',
                                          'Найти кнопку переключения слайдов "Товар дня" вперед',
                                          False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов "вперед" видна пользователю'))
            old_active_id = find_elem('div[class="swiper-slide swiper-slide-active"] div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность внопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_active_id = find_elem('div[class="swiper-slide swiper-slide-active"] div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(comparison(old_active_id,
                                          new_active_id,
                                          'Переключение слайдов кнопкой вперед',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__item-banner-prev main__banners-items-navigation-btn main__banners-items-navigation-prev"]',
                                          "js-swiper__item-banner-prev main__banners-items-navigation-btn main__banners-items-navigation-prev",
                                          '0',
                                          'Поиск кнопки переключения слайдов "Товар дня" назад',
                                          'Найти кнопку переключения слайдов "Товар дня" назад',
                                          False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов "назад" видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность внопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_active_id = find_elem('div[class="swiper-slide swiper-slide-active"] div[class="item-of-day js-article-item"]').get_attribute('data-id')
            result_list.append(comparison(new_active_id,
                                          finally_active_id,
                                          'Переключение слайдов кнопкой назад',
                                          False))
            add_cart = no_such_element(result_list,
                                  'div[class="to-cart cart js-add-to-cart js_to_cart--detail"]',
                                  "to-cart cart js-add-to-cart js_to_cart--detail",
                                  '0',
                                  'Поиск кнопки добавить в корзину',
                                  'Найти кнопку добавить в корзину в блоке "Товар дня"',
                                  False)
            result_list.append(displayed(add_cart,
                                         'Отображение кнопки добавть в корзину',
                                         'ОР: кнопка добавить в корзину видна пользователю'))
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В корзине нет товара',
                                         'Проверить что в иконке корзины нет числа',
                                         True)
            result_list.append(click_intercepted(add_cart,
                                                 'Кликабельность кнопки добавить в корзину',
                                                 'Кликнуть в кнопку "Добавить в корзину'))
            input_block = no_such_element(result_list,
                                          'div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "dc-row p_product_shoping js-product_card_shoping js-product-in-cart",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'Найти поле ввода количества товара',
                                          False)
            result_list.append(displayed(input_block, 
                                         'Отображение поля с количеством товара',
                                         'ОР: Поле с количеством товара отображается на странице'))
            new_cart_count = find_elem('span[class="fb-sticky-menu__badge_type_basket count"]').text
            browser.execute_script(f'document.getElementsByClassName("header-v2-basket")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
            result_list.append(comparison('0',
                                          new_cart_count,
                                          'Добавление товара в корзину',
                                          False))
            browser.execute_script(f"document.getElementsByClassName('header-v2-basket')[0].style.border=\"0px\";")
            plus_button = no_such_element(result_list,
                                          'div[class="item-of-a-day--first"] div[class="cart__amount-button cart__amount-increment btn_reset product_card_qty--btn js_plus-item"]',
                                          'dc-row p_product_shoping js-product_card_shoping js-product-in-cart',
                                          '0',
                                          'Поиск кнопки "+" в блоке Товар дня',
                                          'Найти кнопку "+" в блоке Товар дня',
                                          False)
            result_list.append(displayed(plus_button,
                                         'Отображение кнопки "+" на странице',
                                         'ОР: Кнопка "+" видна пользователю'))
            result_list.append(click_intercepted(plus_button,
                                                 'Кликкабельность кнопки "+"',
                                                 'Кликнуть в кнопку "+"'))
            minus_button = no_such_element(result_list,
                                          'div[class="item-of-a-day--first"] div[class="cart__amount-button cart__amount-decrement btn_reset product_card_qty--btn js_minus-item"]',
                                          'dc-row p_product_shoping js-product_card_shoping js-product-in-cart',
                                          '0',
                                          'Поиск кнопки "-" в блоке Товар дня',
                                          'Найти кнопку "-" в блоке Товар дня',
                                          False)
            result_list.append(displayed(minus_button,
                                         'Отображение кнопки "-" на странице',
                                         'ОР: Кнопка "-" видна пользователю'))
            result_list.append(click_intercepted(minus_button,
                                                'Кликабельность кнопки "-"',
                                                'Кликнуть в кнопку "-"'))
            sleep(0.5)
            result_list.append(click_intercepted(minus_button,
                                                'Закрытие поля корзины кликом в кнопку "-"',
                                                'Кликнуть в кнопку "-"'))
            input_block = no_such_element(result_list,
                                          'div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "dc-row p_product_shoping js-product_card_shoping js-product-in-cart",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'ОР: Поле корзины закрыто',
                                          True)
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В иконке корзины нет товара',
                                         'Проверить что, в иконке корзины нет числа',
                                         True)
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Популярные категории')
    def test_c(self):
        result_list = []
        try:
            popular_block = no_such_element(result_list,
                                            'div[class="main__categories"]',
                                            "main__categories",
                                            '0',
                                            'Поиск блока "Популярные категории"',
                                            'Найти блок "Популярные категории"',
                                            False)
            move_to(popular_block)
            popular_item = no_such_element(result_list,
                                           'div[class="main__categories"] div[class="swiper-slide swiper-slide-active"]',
                                           "swiper-slide swiper-slide-active",
                                           '2',
                                           'Поиск Слайда в блоке',
                                           'Найти слайд в блоке',
                                           False)
            result_list.append(displayed(popular_block,
                                         'Отображение блока "Популярные категории" на странице',
                                         'ОР: Блок "Популярные категории" виден пользователю'))
            #item_text = find_elem('p[class="categories__item-title-link"]').text
            result_list.append(click_intercepted(popular_item,
                                                 'Кликабельность блока "Популярные категории"',
                                                 'Кликнуть в блок "Популярные категории"'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            button_next = no_such_element(result_list,
                            'div[class="js-swiper__categories-next categories__slider-navigation-link categories__slider-navigation-next"]',
                            'js-swiper__categories-next categories__slider-navigation-link categories__slider-navigation-next',
                            '0',
                            'Поиск кнопки переключения слайдов вперед',
                            'Найти кнопку переключения слайдов вперед',
                            False)
            old_item_href = find_elem('a[class="categories__item-info-link"]').text
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_item_href = find_elem('a[class="categories__item-info-link"]').text
            result_list.append(comparison(old_item_href,
                                          new_item_href,
                                          'Переключение слайдов',
                                          False))
            button_back = no_such_element(result_list,
                            'div[class="js-swiper__categories-prev categories__slider-navigation-link categories__slider-navigation-prev"]',
                            'js-swiper__categories-prev categories__slider-navigation-link categories__slider-navigation-prev',
                            '0',
                            'Поиск кнопки переключения слайдов назад',
                            'Найти кнопку переключения слайдов назад',
                            False)
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_item_href = find_elem('a[class="categories__item-info-link"]').text
            result_list.append(comparison(finally_item_href,
                                          new_item_href,
                                          'Переключение слайдов',
                                          False))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Хиты продаж')
    def test_d(self):
        try:
            result_list = []
            block = no_such_element(result_list,
                            'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]',
                            'swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events',
                            '0',
                            'Поиск блока "Хиты продаж"',
                            'Найти блок "Хиты продаж"',
                            False)
            result_list.append(displayed(block,
                                         'Отображение блока "Хиты продаж" на станице',
                                         'ОР: блок "Хиты продаж" отображается на странице'))
            hits_item = no_such_element(result_list,
                                        'div[class="hits__item js-article-item"]',
                                        "hits__item js-article-item",
                                        '0',
                                        'Поиск слайда в блоке "Хиты продаж"',
                                        'Найти хотя бы один слайд в блоке "Хиты продаж"',
                                        False)
            result_list.append(click_intercepted(hits_item,
                                                 'Кликабельность слайдов',
                                                 'Кликнуть в слайд'))
            sleep(1)
            result_list.append(rel_bunner_page(True))
            back()
            button_next = no_such_element(result_list,
                            'div[class="js-swiper__hits-slider-next hits__slider-navigation-btn hits__slider-navigation-next"]',
                            "js-swiper__hits-slider-next hits__slider-navigation-btn hits__slider-navigation-next",
                            '0',
                            'Поиск кнопки переключения слайдов вперед',
                            'Найти кнопку переключения слайдов вперед',
                            False)
            old_item_id = find_elem('div[class="main__hits"] div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="swiper-slide swiper-slide-active"] a[class="hits__item-title"]').text
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.3)
            new_item_id = find_elem('div[class="main__hits"] div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="swiper-slide swiper-slide-active"] a[class="hits__item-title"]').text
            result_list.append(comparison(old_item_id,
                                          new_item_id,
                                          'Переключение слайдов в блоке',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__hits-slider-prev hits__slider-navigation-btn hits__slider-navigation-prev"]',
                                          "js-swiper__hits-slider-prev hits__slider-navigation-btn hits__slider-navigation-prev",
                                          '0',
                                          'Поиск кнопки переключения слайдов назад',
                                          'Найти кнопку переключения слайдов назад',
                                          False)
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.3)
            finally_item_id = find_elem('div[class="main__hits"] div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="swiper-slide swiper-slide-active"] a[class="hits__item-title"]').text
            result_list.append(comparison(new_item_id,
                                          finally_item_id,
                                          'Переключение слайдов назад',
                                          False))
            #############################################################################################################################################
            add_cart = no_such_element(result_list,
                                  'div[class="main__hits"] div[class="to-cart cart js-add-to-cart js_to_cart--detail"]',
                                  "hits__item js-article-item",
                                  '0',
                                  'Поиск кнопки добавить в корзину',
                                  'Найти кнопку добавить в корзину в блоке "Хиты продаж"',
                                  False)
            result_list.append(displayed(add_cart,
                                         'Отображение кнопки добавть в корзину',
                                         'ОР: кнопка добавить в корзину видна пользователю'))
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В корзине нет товара',
                                         'Проверить что в иконке корзины нет числа',
                                         True)
            result_list.append(click_intercepted(add_cart,
                                                 'Кликабельность кнопки добавить в корзину',
                                                 'Кликнуть в кнопку "Добавить в корзину'))
            input_block = no_such_element(result_list,
                                          'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "hits__item js-article-item",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'Найти поле ввода количества товара',
                                          False)
            result_list.append(displayed(input_block, 
                                         'Отображение поля с количеством товара',
                                         'ОР: Поле с количеством товара отображается на странице'))
            new_cart_count = find_elem('span[class="fb-sticky-menu__badge_type_basket count"]').text
            browser.execute_script(f'document.getElementsByClassName("header-v2-basket")[0].style.border=\"5px solid rgba(255, 0, 0, 1)\";')
            result_list.append(comparison('0',
                                          new_cart_count,
                                          'Добавление товара в корзину',
                                          False))
            browser.execute_script(f"document.getElementsByClassName('header-v2-basket')[0].style.border=\"0px\";")
            plus_button = no_such_element(result_list,
                                          'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="cart__amount-button cart__amount-increment btn_reset product_card_qty--btn js_plus-item"]',
                                          'hits__item js-article-item',
                                          '0',
                                          'Поиск кнопки "+" в блоке "Хиты продаж"',
                                          'Найти кнопку "+" в блоке "Хиты продаж"',
                                          False)
            result_list.append(displayed(plus_button,
                                         'Отображение кнопки "+" на странице',
                                         'ОР: Кнопка "+" видна пользователю'))
            result_list.append(click_intercepted(plus_button,
                                                 'Кликкабельность кнопки "+"',
                                                 'Кликнуть в кнопку "+"'))
            minus_button = no_such_element(result_list,
                                          'div[class="swiper hits__slider js-swiper__hits-slider swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[class="cart__amount-button cart__amount-decrement btn_reset product_card_qty--btn js_minus-item"]',
                                          'hits__item js-article-item',
                                          '0',
                                          'Поиск кнопки "-" в блоке "Хиты продаж"',
                                          'Найти кнопку "-" в блоке "Хиты продаж"',
                                          False)
            result_list.append(displayed(minus_button,
                                         'Отображение кнопки "-" на странице',
                                         'ОР: Кнопка "-" видна пользователю'))
            result_list.append(click_intercepted(minus_button,
                                                'Кликабельность кнопки "-"',
                                                'Кликнуть в кнопку "-"'))
            sleep(0.5)
            result_list.append(click_intercepted(minus_button,
                                                'Закрытие поля корзины кликом в кнопку "-"',
                                                'Кликнуть в кнопку "-"'))
            input_block = no_such_element(result_list,
                                          'div[class="dc-row p_product_shoping js-product_card_shoping js-product-in-cart"]',
                                          "dc-row p_product_shoping js-product_card_shoping js-product-in-cart",
                                          '0',
                                          'Поиск поля с количеством товара',
                                          'ОР: Поле корзины закрыто',
                                          True)
            cart_count = no_such_element(result_list,
                                         'span[class="fb-sticky-menu__badge_type_basket count"]',
                                         "fb-sticky-menu__badge_type_basket count",
                                         '0',
                                         'В иконке корзины нет товара',
                                         'Проверить что, в иконке корзины нет числа',
                                         True)
            add_favorite = no_such_element(result_list,
                                           'div[class="js-favourite hits__item-favourite js_wish_item"]',
                                           'js-favourite hits__item-favourite js_wish_item',
                                           '0',
                                           'Поиск кнопки добавления в избранное',
                                           'найти кнопку добавления в избранное',
                                           False)
            result_list.append(displayed(add_favorite,
                                         'Отображение кнопки добавления в избранное на карточке товара',
                                         'ОР: Кнопка добавления в избранное видна пользователю'))
            result_list.append(click_intercepted(add_favorite,
                                                 'Кликабельность кнопки добавления в избранное',
                                                 'Кликнуть в кнопку добавления в избранное'))
            favorite_conut = no_such_element(result_list,
                                             'span[class="fb-sticky-menu__badge_type_personal count "]',
                                             'fb-sticky-menu__badge_type_personal count ',
                                             '0',
                                             'Добавление товара в избранное',
                                             'ОР: В иконке избранного появился счетчик товаров',
                                             False)
            
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Популярные бренды')
    def test_e(self):
        result_list = []
        try:
            block = no_such_element(result_list,
                                    'div[class="popular-brands"]',
                                    'popular-brands',
                                    '0',
                                    'Поиск блок Популрные бренды',
                                    'Найти блок "Популярные бренды"',
                                    False)
            result_list.append(displayed(block,
                                         'Отображение блока "Популярные бренды" на странице',
                                         'ОР: Блок "Популярные бренды" виден пользователю'))
            item = no_such_element(result_list,
                                   'a[class="popular-brands__brand"]',
                                   'popular-brands__brand',
                                   '0',
                                   'Поиск слайдов',
                                   'найти хотя бы один слайд в блоке',
                                   False)
            result_list.append(displayed(item,
                                         'Отображение слайдов в блоке',
                                         'ОР: Слайды видны пользователю'))
            name = item.text
            result_list.append(click_intercepted(item,
                                                 'Кликабельность слайдов',
                                                 'Кликнуть в слайд'))
            result_list.append(h1_title(name))
            back()
            sleep(1)
            button_next = no_such_element(result_list,
                                          'div[class="js-swiper__popular-brands-next popular-brands__slider-navigation-button popular-brands__slider-navigation-next"]',
                                          'js-swiper__popular-brands-next popular-brands__slider-navigation-button popular-brands__slider-navigation-next',
                                          '0',
                                          'Поиск кнопки переключения слайдов вперед',
                                          'Найти кнопку переключения слайдов вперед',
                                          False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов вперед видна пользователю'))
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.5)
            new_name  = find_elem('div[class="popular-brands"] div[class="swiper-slide swiper-slide-active"]').text
            result_list.append(comparison(name,
                                          new_name,
                                          'переключение слайдов после клика вперед',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__popular-brands-prev popular-brands__slider-navigation-button popular-brands__slider-navigation-prev"]',
                                          'js-swiper__popular-brands-prev popular-brands__slider-navigation-button popular-brands__slider-navigation-prev',
                                          '0',
                                          'Поиск кнопки переключения слайдов назад',
                                          'Найти кнопку переключения слайдов назад',
                                          False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов назад видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.5)
            finally_name = find_elem('div[class="popular-brands"] div[class="swiper-slide swiper-slide-active"]').text
            result_list.append(comparison(finally_name,
                                          new_name,
                                          'переключение слайдов после клика назад',
                                          False))
            
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True

    @allure.story('Тело')
    @allure.title('Нижний баннер')
    def test_f(self):
        result_list = []
        try:
            bunner = no_such_element(result_list,
                                     'div[class="promos"]',
                                     'promos',
                                     '0',
                                     'Поиск Нижнего баннера',
                                     'Найти нижний баннер',
                                     False)
            result_list.append(displayed(bunner,
                                         'Отображение нижнего баннера',
                                         'ОР: Нижний баннер виден пользователю'))
            result_list.append(click_intercepted(bunner,
                                                 'Кликабельность нижнего баннера',
                                                 'Кликнуть в нижний баннер'))
            result_list.append(rel_bunner_page(True))
            back()
            sleep(1)
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ПРОВЕРКА ПЕРЕКЛЮЧЕНИЯ СЛАЙДОВ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True
    
    @allure.story('Тело')
    @allure.title('Советы эксперта')
    def test_g(self):
        browser.get(link)
        result_list = []
        try:
            block = no_such_element(result_list,
                                    'div[class="expert-advices"]',
                                    'expert-advices',
                                    '0',
                                    'Поиск блока "Советы эксперта"',
                                    'Найти блок советы эксперта',
                                    False)
            result_list.append(displayed(block,
                                         'Отображение блока "Советы эксперта" на странице',
                                         'ОР: Блок "Советы эксперта" видны пользователю'))
            item = no_such_element(result_list,
                                   'a[class="expert-advices__card expert-advices__card--dark-gray"]',
                                   'expert-advices__card expert-advices__card--dark-gray',
                                   '0',
                                   'Поиск слайдов в блоке',
                                   'Найти слайд в блоке',
                                   False)
            result_list.append(displayed(item,
                                         'Отображение слайдов в блоке "Советы эксперта"',
                                         'ОР: Слайды в блоке "Советы эксперта" видны пользователю'))
            name = find_elem('h3[class="expert-advices__card-description-title"]').text
            result_list.append(click_intercepted(item,
                                                 'Кликабельность слайда "Советы эксперта"',
                                                 'Кликнуть в слайд "Советы эксперта"'))
            result_list.append(h1_title(name))
            back()
            sleep(1)
            button_next = no_such_element(result_list,
                                        'div[class="js-swiper__expert-advices-next expert-advices__slider-navigation-button expert-advices__slider-navigation-next"]',
                                        'js-swiper__expert-advices-next expert-advices__slider-navigation-button expert-advices__slider-navigation-next',
                                        '0',
                                        'Поиск кнопки переключения слайдов вперед',
                                        'Найти кнопку переключения слайдов вперед',
                                        False)
            result_list.append(displayed(button_next,
                                         'Отображение кнопки переключения слайдов вперед',
                                         'ОР: Кнопка переключения слайдов вперед видна пользователю'))
            result_list.append(click_intercepted(button_next,
                                                 'Кликабельность кнопки переключения слайдов вперед',
                                                 'Кликнуть в кнопку переключения слайдов вперед'))
            sleep(0.5)
            new_name  = find_elem('div[class="expert-advices"] div[class="swiper-slide swiper-slide-active"] h3').text
            result_list.append(comparison(name,
                                          new_name,
                                          'переключение слайдов после клика вперед',
                                          False))
            button_back = no_such_element(result_list,
                                          'div[class="js-swiper__expert-advices-prev expert-advices__slider-navigation-button expert-advices__slider-navigation-prev"]',
                                          'js-swiper__expert-advices-prev expert-advices__slider-navigation-button expert-advices__slider-navigation-prev',
                                          '0',
                                          'Поиск кнопки переключения слайдов назад',
                                          'Найти кнопку переключения слайдов назад',
                                          False)
            result_list.append(displayed(button_back,
                                         'Отображение кнопки переключения слайдов назад',
                                         'ОР: Кнопка переключения слайдов назад видна пользователю'))
            result_list.append(click_intercepted(button_back,
                                                 'Кликабельность кнопки переключения слайдов назад',
                                                 'Кликнуть в кнопку переключения слайдов назад'))
            sleep(0.5)
            finally_name = find_elem('div[class="expert-advices"] div[class="swiper-slide swiper-slide-active"] h3').text
            result_list.append(comparison(finally_name,
                                          new_name,
                                          'переключение слайдов после клика назад',
                                          False))
        finally:
            steps_case()
            debug_case()
            assert result(result_list) == True



########################FOOTER#############################FOOTER#########################FOOTER#############################FOOTER#########################FOOTER#############################FOOTER#########################FOOTER   

    def test_zzzz(self):
        browser.quit()


if __name__ == "__main__":
    unittest.main()
