import importlib
import re
import sys
import unittest
import time
import pytest
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementNotInteractableException
from selenium.common.exceptions import ElementClickInterceptedException
from random import randint
import datetime
import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from allure import severity, severity_level

#pytest main_page_pc.py -v -s --alluredir asd
#allure serve asd

link = "https://stroylandiya.ru/"
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-dev-shm-usage')  

browser = webdriver.Remote (command_executor = "http://selenium__standalone-chrome:4444/wd/hub", options = chrome_options) #chrome_options = chrome_options) command_executor="http://selenium__standalone-chrome:4444"
browser.set_window_position(0, 0)
browser.set_window_size(1920, 1080)
browser.implicitly_wait(10)
action = ActionChains(browser)
datetime.datetime.utcnow()
date = datetime.datetime.now()

check_list = f'https://docs.google.com/spreadsheets/d/1x77VeZxM9GvXX1O3cCMIHMf95MDv1s1MkHieoDaxlf4/edit#gid=1828904958&range='

def test_status_code():
    dat = {'q':'goog'}
    response = requests.get(link, params=dat, headers={'User-Agent': 'Mozilla/5.0'})
    return(response.status_code)



browser.get(link)
if test_status_code() != 200:
        sys.exit(f'Статус код {test_status_code()}')



class test_title(unittest.TestCase):
    def test_title(self):
        title = browser.title
        browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"]').click()
        time.sleep(1)
        city = browser.find_element(By.CSS_SELECTOR, 'div[class="location-item current shown"] span').text
        result = (any(map(title.lower().__contains__, map(str.lower, city))))
        self.assertTrue(result)



@allure.feature('Главная страница')
@allure.story('Поиск элементов')
class test_find_blocks(unittest.TestCase):
        
    @allure.title('Поиск баннера над хедером')
    @allure.link(f'{check_list}E2')
    def test_availability_banners(self):
        browser.get(link)
        try:
            banner_upper_header = browser.find_element(By.CSS_SELECTOR, 'div[class="banner CROP TOP_HEADER hidden-sm hidden-xs"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден, Читай если элемент есть на скрине'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "banner_upper_header" в функции "test_availability_banners"', name = 'Читай если элемент есть на скрине', attachment_type=AttachmentType.TEXT)
        action.move_to_element(banner_upper_header).perform()
        time.sleep(0.5)
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннер над хедером', attachment_type=AttachmentType.PNG)
        self.assertTrue(banner_upper_header.is_displayed(), msg=f'Баннер над хэдером не виден пользователю {date}')

    @allure.title('Поиск логотипа')     
    def test_find_logo(self):
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-logo"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "button" в функции "test_find_button_stroylandiya"', name = 'Читай если элемент есть на скрине', attachment_type=AttachmentType.TEXT)
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин логотипа', attachment_type=AttachmentType.PNG)
        self.assertTrue(button.is_displayed(), msg = f'Кнопка не видна пользователю {date}')

    @allure.title('Поиск поля ввода, строки поиска')
    def test_find_search(self):
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[class="search-wrapper"]')
            action.move_to_element(block).perform()
            with allure.step('Поле ввода поиска'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "block"', name = 'Ошибка', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step('Не найден блок поиска'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "block"', name = 'Ошибка', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(block.is_displayed(), msg = f'Блока поика не виден пользователю')

    @allure.title('Поиск "Товары дня')
    def test_product_day(self):
        try:
            product_day = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper js-swiper__item-banner swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден, Читай если элемент есть на скрине'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "product_day" в функции "test_product_day"', name = 'Читай если элемент есть на скрине', attachment_type=AttachmentType.TEXT)
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин Товары дня', attachment_type=AttachmentType.PNG)
        self.assertTrue(product_day.is_displayed(), msg = f'Продукт дня не виден пользователю {date}')
                
    @allure.title('Поиск корзины')
    def test_find_basket(self):
        try:
            basket = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-basket"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "basket" в функции "test_find_basket"')
        with allure.step('Данные страницы'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Книпки корзина"', attachment_type=AttachmentType.PNG)
        self.assertTrue(basket.is_displayed(), msg = f'Корзина не найдена {date}')
    
    @allure.title('Поиск блока "Популярные категории"')
    def test_popular_categories(self):
        browser.get(link)
        try:
            block_popular_categories = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper js-swiper__categories swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "block_popular_categories" в функции "test_popular_categories"')
        actions = ActionChains(browser)
        actions.move_to_element(block_popular_categories)
        actions.perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин блока популярные категории', attachment_type=AttachmentType.PNG)
        self.assertTrue(block_popular_categories.is_displayed(), msg = f'Блок популярные категории не найден {date}')

    @allure.title('Поиск хедера')
    def test_find_header(self):
        try:
            header = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
            action.move_to_element(header).perform()
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "header" в функции "test_find_header"')
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин хедера', attachment_type=AttachmentType.PNG)
        self.assertTrue(header.is_displayed(), msg = f'Хедер не найден {date}')

    @allure.title('Поиск футера')
    def test_find_footer(self):
        try:
            footer = browser.find_element(By.CSS_SELECTOR, 'footer  > div[class="container-fluid"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "footer" в функции "test_find_footer"')
        actions = ActionChains(browser)
        actions.move_to_element(footer)
        actions.perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин футера', attachment_type=AttachmentType.PNG)
        self.assertTrue(footer.is_displayed(), msg = f'Футер не найден {date}')

    @allure.title('Поиск номера телефона')
    def test_find_phone(self):
        try:
            phone = browser.find_element(By.CSS_SELECTOR, 'a.header-phone')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "phone" в функции "test_find_phone"')
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин номер телефона', attachment_type=AttachmentType.PNG)
        self.assertTrue(phone.is_displayed(), msg = f'Номер телефона не найден {date}')

    @allure.title('Поиск главного баннера')
    def test_find_main_banner(self):
        try:
            banner = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper js-swiper__main-banner swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "banner" в функции "test_find_main_banner"')
        actions = ActionChains(browser)
        actions.move_to_element(banner)
        actions.perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Главный баннер', attachment_type=AttachmentType.PNG)
        self.assertTrue(banner.is_displayed(), msg = f'Баннер не найден {date}')
    
    @allure.title('Поиск нижнего баннера')
    def test_find_lower_banner(self):
        try:
            lower_banner = browser.find_element(By.CSS_SELECTOR, 'div[class="promos"]')
        except NoSuchElementException:
            with allure.step('Селектор не найден'):
                allure.attach(f'Смотри скрин, если элемент на месте, проверь селектор переменной "lower_banner" в функции "test_find_lower_banner"')
        actions = ActionChains(browser)
        actions.move_to_element(lower_banner)
        actions.perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Нижний баннер', attachment_type=AttachmentType.PNG)
        self.assertTrue(lower_banner.is_displayed(), msg = f'Баннер не найден {date}')

    @allure.title('Поиск блока "Советы эксперта"')
    @allure.description('Функция проверяет видимость блока на странице')
    def test_clickabillity_expert_articles(self):
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[class="expert-advices"]')
            action.move_to_element(block).perform()
            time.sleep(0.3)
            with allure.step('Скрин блоока "Советы эксперта"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            screen = browser.find_element(By.CSS_SELECTOR, 'div[class="seo"]')
            action.move_to_element(screen).perform()
            time.sleep(0.3)
            with allure.step('Блок "Советы эксперта не найден" смотри переменную "block" если блок есть на скрине'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(block.is_displayed(), msg = f'Блок Советы эксперта не найден {date}')

@allure.feature('Главная страница')           
@allure.story('Проверка кликабельности')
class test_header_clickability(unittest.TestCase):

    def setUp(self):
        browser.get(link)

    @allure.title('Проверка кликабельности логотипа')        
    def test_clickabillity_button_stroylandiya(self):
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-logo"]')
            button.click()           
        except ElementNotInteractableException:
            button = None
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин логотипа', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(button, msg = f'Кнопка не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки "Магазины"')
    def test_clickabillity_shops(self):
        browser.delete_all_cookies()
        time.sleep(1)
        try:
            shops = browser.find_element(By.CSS_SELECTOR, 'div[class="header-block-left"] a[href="/shops/"]')
            shops.click()
        except ElementNotInteractableException:
            shops = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Магазины"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(shops, msg = f'Кнопка "Магазины" не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки смотреть все в блоке "Новости"')
    def test_button_news(self):
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'a[class="expert-advices__all-advices-link"]')
            button.click()
        except ElementNotInteractableException:
            button = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)    
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Новости"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(button, msg = f'Кнопка "Магазины" не кликабельна {date}')

    @allure.title('Проверка кликабельности популярные категории')
    @allure.description('Функция кликает в рандомный слайд блока, если не возвращается исключение, то тест пройден')
    def test_clickabillity_popular_category(self):
        browser.delete_all_cookies()
        try:
            popular_block = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"]')
        except NoSuchElementException:
            with allure.step('Скрин'):
                allure.attach(browser.get_screenshot_as_png(), name = f'Не найден блок "Популярные категории" смотри переменную "popular_block"', attachment_type=AttachmentType.PNG)
            pytest.skip('Не найден блок "Популярные категори"')
        first = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide"]')
        activ = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide swiper-slide-active"]')
        next = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide swiper-slide-next"]')
        slides = [first, activ, next]
        item = slides[randint(0, len(slides) - 1)]
        first_name = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide"] span[class="categories__item-title-link"]').text
        activ_name = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide swiper-slide-active"] p[class="categories__item-title-link"]').text
        next_name = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide swiper-slide-next"] span[class="categories__item-title-link"]').text
        if item == first:
            item_name = first_name
        elif item == activ:
            item_name = activ_name
        elif item == next:
            item_name = next_name
        try:
            item.click()
        except ElementNotInteractableException or ElementClickInterceptedException:
            item = None    
            with allure.step('Скрин'):
                allure.attach(browser.get_screenshot_as_png(), name = f'Скрин после клика в {item_name}', attachment_type=AttachmentType.PNG)
        with allure.step('Скрин'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = f'Скрин после клика в {item_name}', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(item, msg = f'Категория {item_name} в блоке популярные категории не кликабельна {date}')

    @allure.title('Проверка кликабельности корзины')    
    def test_clickabillity_basket(self):
        try:
            basket = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-basket"]')
            basket.click()
        except ElementNotInteractableException:
            basket = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Корзина"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(basket, msg = f'Корзина не кликабельна {date}')

    @allure.title('Проверка кликабельности избранное')    
    def test_clickabillity_favourites(self):
        try:
            favourites = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-favourites"]')
            favourites.click()
        except ElementNotInteractableException:
            favourites = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Избранное"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(favourites, msg = f'Избранное не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки "Личный кабинет"')    
    def test_clickabillity_personal_account(self):
        try:
            personal_account = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-enter "]')
            personal_account.click()
            time.sleep(2)
        except ElementNotInteractableException:
            personal_account = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Личный кабинет"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(personal_account, msg = f'Кнопка личный кабинет не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки "Сравнение"')    
    def test_clickabillity_comparison(self):
        try:
            comparison = browser.find_element(By.CSS_SELECTOR, 'a[class="basket-link compare header-v2-compare "]')
            comparison.click()
        except ElementNotInteractableException:
            comparison = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Сравнение"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(comparison, msg = f'Кнопка "Сравнение" не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки "Город"')
    def test_clickabillity_city(self):
        try:
            city = browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"]')
            city.click()
            time.sleep(2)
        except ElementNotInteractableException:
            city = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Город"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(city, msg = f'Кнопка "Город" не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки "Статус заказа"')
    def test_clickabillity_order_status(self):
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'a[class="button-header"]').click()
            time.sleep(1)
        except ElementNotInteractableException:
            button = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Статус заказа"', attachment_type=AttachmentType.PNG)
        self.assertIsNone(button, msg = f'Кнопка статус заказа не кликабельна {date}')

    @allure.title('Проверка кликабельности кнопки "Акции"')
    def test_clickabillity_stocks(self):
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'a[href="/actions/"]').click()
            time.sleep(1)
        except ElementNotInteractableException:
            button = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Акции"', attachment_type=AttachmentType.PNG)
        self.assertIsNone(button, msg = f'Кнопка акции не кликабельна {date}')
    
    @allure.title('Проверка кликабельности главного баннера')
    def test_clickabillity_main_banner(self):
        try:
            banner = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper js-swiper__main-banner swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]')
            banner.click()
        except ElementNotInteractableException:
            banner = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после клика в главный баннер', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(banner, msg = f'Главный баннер не кликабелен {date}')

    @allure.title('Проверка кликабельности "Товар дня"')
    def test_clickabillity_product_day(self):
        time.sleep(0.5)
        try:
            banner = browser.find_element(By.CSS_SELECTOR, 'div[class="item-of-a-day--first"]')
            banner.click()
        except ElementNotInteractableException:
            banner = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после клика в Товар дня', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(banner, f'Продукт дня не кликабелен {date}')

    @allure.title('Проверка клика в нижний баннер')
    def test_clickabillity_lower_banner(self):
        try:
            banner = browser.find_element(By.CSS_SELECTOR, 'div[class="promos"]')
            banner.click()
        except ElementNotInteractableException:
            banner = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после клика в нижний баннер', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(banner, msg = f'Нижний баннер не кликабелен {date}')
    
    @allure.title('Проверка клика в баннер над хедером')
    def test_clickabillity_hover_banner(self):
        try:
            banner = browser.find_element(By.CSS_SELECTOR, 'div[class="banner CROP TOP_HEADER hidden-sm hidden-xs"]')
            banner.click()
        except ElementNotInteractableException:
            banner = None
        with allure.step('Данные страницы'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после клика в баннер над хедером', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(banner, msg = f'Баннер над хедером не кликабелен {date}')

    @allure.title('Кликабельности "Популярные бренды"')
    def test_clickabillity_popular_brands(self):
        browser.delete_all_cookies()
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[class="popular-brands"]')
            action.move_to_element(block).perform()
        except NoSuchElementException:
            screen = browser.find_element(By.CSS_SELECTOR, 'div[class="promos"]')
            action.move_to_element(screen).perform()
            with allure.step('Блок Популярные бренды не найден'):
                allure.attach(browser.get_screenshot_as_png(), name = f'Блок не найден, смотри скрин, если блок есть на скрине проверь переменную "block"', attachment_type=AttachmentType.PNG)
            pytest.skip('Блок "Популярые бренды" не найден')
        time.sleep(0.5)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="popular-brands__brand"]')
        item = items[randint(0, len(items) - 14)]
        try:
            item.click()
        except ElementNotInteractableException:
            item = None
            with allure.step('Слайды в блоке "Популярные бренды", не кликабельны'):
                allure.attach(browser.get_screenshot_as_png(), name = f'скрин после клика по бренду', attachment_type=AttachmentType.PNG)
        with allure.step('Скрин после клика в слайд блока "Популярные бренды"'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = f'скрин после клика по бренду', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(item, msg = f'Слайды в блоке популярые бренды не кликабельны {date}')
        
    @allure.title('Проверка клика по слайду "Хиты продаж"')
    def test_clickabillity_hit_sales(self):
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[class="main__hits-container hits"]')
            action.move_to_element(block).perform()
        except NoSuchElementException:
            action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'div[class="popular-brands"]')).perform()
            time.sleep(0.5)
            with allure.step('Блок хиты продаж не найден, если на скрине блок есть, смотри селектор "block"'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('На странице не найден блок хиты продаж')
        time.sleep(0.5)
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="main__hits-container hits"] div[class="hits__item js-article-item"]')
        items_list = [items[0], items[1], items[2], items[3], items[4]]
        item = items[randint(0, len(items_list) - 1)]
        try:
            item.click()
        except ElementNotInteractableException:
            item = None
        with allure.step('Скрин после клика в товар в блоке "Хиты продаж"'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(item, f'Товары в блоке "Хиты продаж" не кликабельны {date}')

    @allure.title('Проверка клика по слайду советы эксперта')
    def test_clickabillity_news(self):
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[class="expert-advices"]')
            action.move_to_element(block).perform()
        except NoSuchElementException:
            with allure.step('Блок советы эксперта не найден'):
                allure.attach(f'Блок советы эксперта не найден, проверь селектор переменной "block" если блок есть на скрине', name = f'Советы эксперта(новости)', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip(f'Блок советы экспертов не найден {date}')
        time.sleep(0.5)
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="expert-advices"] div[class="expert-advices__card-description expert-advices__card-description--light-gray"]')
        item = items[randint(0, len(items) - 16)]
        try:
            item.click()
        except ElementNotInteractableException or ElementClickInterceptedException: 
            item = None
        with allure.step('Скрин блока советы эксперта'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(item, msg = f'Слайды в блоке Соверты экспертов, не кликабельны {date}')

    @allure.title('Проверка кликабельности поля поиска')
    def test_clickabillity_search(self):
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[id="title-search_fixed"]')
            action.move_to_element(block).perform()
        except NoSuchElementException:
            with allure.step('Блок поиска не найден'):
                allure.attach(f'Блок поиск не найден, проверь селектор переменной "block" если блок есть на скрине', name = f'поиск', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip(f'Блок поиска не найден {date}')
        try:
            input_block = browser.find_element(By.CSS_SELECTOR, 'div[class="search-input-div"]')
            input_block.click()
            time.sleep(0.3)
            with allure.step("Скрин после клика в блок поиска"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementClickInterceptedException:
            input_block = None
        self.assertIsNotNone(block, msg = f'Блок поиска не кликабелен {date}')

    @allure.title('Проверка наличия категорий в поиске')
    def test_search(self):
        browser.get(link)
        time.sleep(2)
        search_input = browser.find_element(By.CSS_SELECTOR, '#title-search-input_fixed')
        search_input.send_keys("обои")
        time.sleep(2)
        try:
            category = browser.find_element(By.CSS_SELECTOR, 'div[class="digi-ac-block"]')
        except NoSuchElementException:
            category = None
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин категорий', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(category, msg = f'Подсказка "Категория" не найдена {date}')

@allure.feature('Главная страница')
@allure.story('Проверка переключения слайдеров')
class test_switch_sliders(unittest.TestCase):
    
    @allure.title('Проверка переключения новостей(советы эксперта)')
    def test_switching_news(self):
        #browser.get(link)
        banner = browser.find_element(By.CSS_SELECTOR, 'div[class="expert-advices"]')
        before_click_news = browser.find_element(By.CSS_SELECTOR, 'a[class="expert-advices__card expert-advices__card--dark-gray"]').is_displayed()
        actions = ActionChains(browser)
        actions.move_to_element(banner)
        actions.perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин новости до переключения', attachment_type=AttachmentType.PNG)
        button = browser.find_element(By.CSS_SELECTOR, 'div[class="js-swiper__expert-advices-next expert-advices__slider-navigation-button expert-advices__slider-navigation-next"]').click()
        time.sleep(1)
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин новости после переключения', attachment_type=AttachmentType.PNG)
        after_click_news = browser.find_element(By.CSS_SELECTOR, 'a[class="expert-advices__card expert-advices__card--dark-gray"]').is_displayed()
        self.assertNotEqual(before_click_news, after_click_news, msg = f'Слайдер не переключился {date}')

    @allure.title('Проверка переключения "Товар дня')
    def test_switching_product_day(self):
        #browser.get(link)
        screen = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
        action.move_to_element(screen).perform()
        before_click = browser.find_element(By.CSS_SELECTOR, 'a[class="item-of-day__name"]').text
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Товары дня" до переключения', attachment_type=AttachmentType.PNG)
        browser.find_element(By.CSS_SELECTOR, 'svg[class="main__banners-items-navigation-next-svg"]').click()
        time.sleep(0.5)
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Товары дня" после пеереключения', attachment_type=AttachmentType.PNG)
        after_click = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper-slide swiper-slide-active"] a[class="item-of-day__name"]').text
        self.assertNotEqual(before_click, after_click, msg = f'Слайд не переключился {date}')

    @allure.title('Проверка переключения блока "Популярые категории"')
    def test_switching_popular_category(self):
        #browser.get(link)
        screen = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
        action.move_to_element(screen).perform()
        try:
            popular_block = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"]')
        except NoSuchElementException:
            with allure.step('Блок популярные категории не найден'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        action.move_to_element(popular_block).perform()
        with allure.step('Блок популярные категории до переключения'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        activ_name = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide swiper-slide-active"] p[class="categories__item-title-link"]').text
        try:
            switch_button = browser.find_element(By.CSS_SELECTOR, 'div[class="js-swiper__categories-next categories__slider-navigation-link categories__slider-navigation-next"]')
        except NoSuchElementException:
            with allure.step('Не найдена кнопка переключения баннеров'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            num_bunners = len(browser.find_elements(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide"]'))
            if num_bunners <= 3:
                with allure.step('Все баннеры помещаются на один экран'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                    pytest.skip('Слишком мало баннеров для переключения')
            else:
                pytest.skip('Проверь селектор кнопки переключения баннеров')
        switch_button.click()
        time.sleep(0.5)
        with allure.step('Блок популярные категории после переключения'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        new_activ_name = browser.find_element(By.CSS_SELECTOR, 'div[class="main__categories"] div[class="swiper-slide swiper-slide-active"] span[class="categories__item-title-link"]').text
        self.assertNotEqual(activ_name, new_activ_name, msg = f'Баннеры не листаются {date}')

    @allure.title('Проверка переключения главного баннера')
    def test_switching_main_banner(self):
        #browser.get(link)
        block = browser.find_element(By.CSS_SELECTOR, 'div[class="main__banners-main"]')
        action.move_to_element(block).perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин главного баннера до переключения', attachment_type=AttachmentType.PNG)
        before_click = browser.find_element(By.CSS_SELECTOR, 'span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
        try:    
            browser.find_element(By.CSS_SELECTOR, 'div[class="js-swiper__main-banner-next main__banners-main-navigation-next"]').click()
            time.sleep(0.5)
        except ElementNotInteractableException:
            with allure.step('Нет кнопки переключения баннера'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                pytest.skip("Всего один слайд")
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин главного баннера после переключения', attachment_type=AttachmentType.PNG)
        after_click = browser.find_element(By.CSS_SELECTOR, 'span[class="swiper-pagination-current main__banners-main-info-current-value"]').text
        self.assertNotEqual(before_click, after_click, msg = f'слайд не переключился {date}')

    @allure.title('Проверка переключения "Популярные бренды"')
    def test_switching_popular_brands(self):
        #browser.get(link)
        try:
            block = browser.find_element(By.CSS_SELECTOR, 'div[class="popular-brands"]')
            screen = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
            action.move_to_element(screen).perform()
            action.move_to_element(block).perform()
            time.sleep(0.3)
        except NoSuchElementException:
            screen = browser.find_element(By.CSS_SELECTOR, 'div[class="promos"]')
            action.move_to_element(screen).perform()
            with allure.step('Не найден блок "Попуряные бренды"'):
                allure.attach(f'Не найден блок "Попуряные бренды" или селектор "block" не верный, проверяй скрин', name = 'Нет блока "Популярые бренды', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('Не найден блок "Попуряные бренды"')
        item_before_switch = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper-slide swiper-slide-active"] span[class="popular-brands__brand-text"]').text
        with allure.step("Скрин до переключения слайда"):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'div[class="js-swiper__popular-brands-next popular-brands__slider-navigation-button popular-brands__slider-navigation-next"]')
            button.click()
            time.sleep(0.5)
            with allure.step("Скрин после переключения слайда"):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('Нет кнопки переключения слайдера'):
                allure.attach(f'Нет кнопки переключения слайдера или селектор "button" не верный', name = 'не нашел кнопку переключения слайдов', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                slides = len(browser.find_elements(By.CSS_SELECTOR, 'a[class="popular-brands__brand"]'))
                if slides <=7:
                    with allure.step('Все слайды помещаются на один экран'):
                        allure.attach(f'недостаточно слайдов для переключения', name = 'Все слайды помещаются на один экран', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                    pytest.skip("Все слайды на одном экране")
        item_after_switch = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper-slide swiper-slide-active"] span[class="popular-brands__brand-text"]').text
        self.assertNotEqual(item_before_switch, item_after_switch, msg = f'Слайдер не переключился {date}')

    @allure.title('Проверка переключения нижнего баннера')
    def test_switching_lower_banner(self):
        #browser.get(link)
        banner = browser.find_element(By.CSS_SELECTOR, 'div[class="promos"]')
        actions = ActionChains(browser)
        actions.move_to_element(banner)
        actions.perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннера до клика', attachment_type=AttachmentType.PNG)
        try:
            before_click = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper promos__slider js-swiper__promos-banner swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[data-swiper-slide-index="0"]').is_displayed()
            swipe_button = browser.find_element(By.CSS_SELECTOR, 'svg[class="promos__slider-navigation-svg"]').click()
            slides = browser.find_element(By.CSS_SELECTOR, 'span[class="swiper-pagination-total promos__slider-total-value"]').text
            last_slide = int(slides)-1
        except NoSuchElementException or ElementNotInteractableException:
            try:
                swipe_button = browser.find_element(By.CSS_SELECTOR, 'svg[class="promos__slider-navigation-svg"]').click()
            except ElementNotInteractableException:
                with allure.step('Нет кнопки переключения баннера'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Нет кнопки переключения баннеров', attachment_type=AttachmentType.PNG)
                pytest.skip("Всего один слайд")           
        time.sleep(0.5)
        after_click = browser.find_element(By.CSS_SELECTOR, f'div[class="swiper promos__slider js-swiper__promos-banner swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"] div[data-swiper-slide-index="{last_slide}"]').is_displayed()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннера после клика', attachment_type=AttachmentType.PNG)
        self.assertEqual(before_click, after_click, msg = f'слайд не переключился {date}')

    @allure.title('Проверка переключения блока "Хит продаж"')
    def test_switching_hit_sales(self):
        browser.get(link)
        result_id1 = []
        result_id2 = []
        block = browser.find_element(By.CSS_SELECTOR, 'div[class="main__hits-container hits"]')
        action.move_to_element(block).perform()
        with allure.step('Скрин до переключения'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннера Хиты продаж до переключения', attachment_type=AttachmentType.PNG)
        before_click = browser.find_elements(By.CSS_SELECTOR, 'div[class="main__hits"] div[class="hits__item js-article-item"]')    
        for i in before_click:
            result = i.is_displayed()
            if result == True:
                data_id1 = i.get_attribute('data-id')
                result_id1.append(data_id1)
        try:
            button = browser.find_element(By.CSS_SELECTOR, 'div[class="main__hits"] div[class="js-swiper__hits-slider-next hits__slider-navigation-btn hits__slider-navigation-next"]').click()
        except NoSuchElementException:
            with allure.step('Кнопка переключения баннеров не найдена'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннера Хиты продаж', attachment_type=AttachmentType.PNG)
            print('Косяк с кнопкой')
            items = len(browser.find_elements(By.CSS_SELECTOR, 'div[class="main__hits"] div[class="hits__item js-article-item"]'))
            if items <=5:
                with allure.step('Недостаточно баннеров для переключения'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннера Хиты продаж', attachment_type=AttachmentType.PNG)
                print('косяка нет')
                pytest.skip('Все баннеры помящаются на один экран')
            else: pytest.skip('Не найдена кнопка, если кнопка есть на скрине, проверяй селектор "button"')
        time.sleep(2)
        with allure.step('Скрин после переключения'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин баннера Хиты продаж после переключения', attachment_type=AttachmentType.PNG)
        after_click = browser.find_elements(By.CSS_SELECTOR, 'div[class="main__hits"] div[class="hits__item js-article-item"]')    
        for i in after_click:
            result = i.is_displayed()
            if result == True:
                data_id = i.get_attribute('data-id')
                result_id2.append(data_id)
        if len(result_id1)+1 == len(result_id2):
            result_id2.pop(0)
        self.assertNotEqual(result_id1, result_id2, msg = f'Слайд не переключился {date}')

@allure.feature('Проверка смены города')
class test_city(unittest.TestCase):
    
    @allure.title('Проверка смены города, кликом по названию в списке городов')
    def test_choosing_city(self):
        browser.get(link)
        with allure.step('Скриншот до смены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до смены города', attachment_type=AttachmentType.PNG)
        city = browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"]')
        city.click()
        time.sleep(1)
        old_city_name = browser.find_element(By.CSS_SELECTOR, 'div[class="location-item current shown"] span').text
        city_list = browser.find_elements(By.CSS_SELECTOR, 'div[class="location-item shown"]')
        new_city = city_list[randint(0, len(city_list) - 1)]
        new_city.click()
        time.sleep(2)
        with allure.step('Скриншот после смены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после смены города', attachment_type=AttachmentType.PNG)
        browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"]').click()
        time.sleep(1)
        new_city_name = browser.find_element(By.CSS_SELECTOR, 'div[class="location-item current shown"] span').text
        with allure.step('Данные страницы'):
            allure.attach(f'Название города до смены - {old_city_name}\nНазвание города после смены - {new_city_name}', name = 'Названия городов', attachment_type=AttachmentType.TEXT)
        self.assertNotEqual(old_city_name, new_city_name, msg = f'Город не изменился {date}')

    @allure.title('Проверка смены города, вводом названия в строку поиска городов')    
    def test_search_city(self):
        input_city = 'Уфа'
        browser.get(link)
        with allure.step('Скриншот до смены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до смены города', attachment_type=AttachmentType.PNG)
        city = browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"]')
        city.click()
        time.sleep(2)
        old_city_name = browser.find_element(By.CSS_SELECTOR, 'div[class="location-item current shown"] span').text
        search_input = browser.find_element(By.CSS_SELECTOR, 'input[name="header_city_choose_val"]')
        search_input.click()
        search_input.send_keys(input_city)
        time.sleep(2)
        browser.find_element(By.CSS_SELECTOR, 'div[class="suggestions-suggestion"][data-index="0"]').click()
        time.sleep(3)
        with allure.step('Скриншот после смены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после смены города', attachment_type=AttachmentType.PNG)
        browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"]').click()
        time.sleep(2)
        new_city_name = browser.find_element(By.CSS_SELECTOR, 'div[class="location-item current shown"] span').text
        with allure.step('Названия городов'):
            allure.attach(f'Город до смены - {old_city_name}\nВыбранный город - {input_city}\nГород после смены - {new_city_name}', name = 'Названия городов', attachment_type=AttachmentType.TEXT)
        self.assertEqual(input_city, new_city_name, msg = f'Город не изменился {date}')

@allure.feature('Каталог')
class test_catalog(unittest.TestCase):

    @allure.title('Проверка кликабельности каталога')
    def test_clickabillity_catalog(self):
        browser.get(link)
        try:
            catalog = browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]')
            catalog.click()
            time.sleep(1)
        except ElementNotInteractableException:
            catalog = None
        try:
            catalog_window = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-header-catalog-menu fb-header-catalog-menu_opened"]')
        except NoSuchElementException:
            catalog = None
            with allure.step('Скрин после клика в каталог'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Кнопка каталог не кликабельна', attachment_type=AttachmentType.PNG)
        with allure.step('Скрин после клика в каталог'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин "Каталог"', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(catalog, msg = f'Кнопка "Каталог" не кликабельна {date}')

    @allure.title('Проверка "изменение цвета текста пунктов каталога при наведении курсора"')
    def test_changing_color_text_catalog_items(self):
        browser.delete_all_cookies()
        browser.get(link)
        browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]').click()
        time.sleep(1)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        item = items[randint(0, len(items) - 1)]
        text_color_before = item.value_of_css_property('color')
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до фокуса на элемент', attachment_type=AttachmentType.PNG)
        action.move_to_element(item).perform()
        with allure.step('Данные страницы'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после фокуса на элемент', attachment_type=AttachmentType.PNG)
        text_color_after = item.value_of_css_property('color')
        self.assertNotEqual(text_color_before, text_color_after, msg = f'цвет текста не изменился {date}')

    @allure.title('Проверка "изменение цвета фона пунктов каталога при наведении курсора"')
    def test_changing_color_background_catalog_items(self):
        browser.get(link)
        catalog = browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]')
        catalog.click()
        time.sleep(1)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        block_background_color = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-header-catalog-menu__parent-menu"]').value_of_css_property('background-color')
        item = items[randint(0, len(items) - 1)]
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до фокуса на элемент', attachment_type=AttachmentType.PNG)
        action.move_to_element(item).perform()
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после фокуса на элемент', attachment_type=AttachmentType.PNG)
        color_background_after = item.value_of_css_property('background-color')
        self.assertNotEqual(block_background_color, color_background_after, msg = f'цвет фона не изменился {date}')   

    @allure.title('Проверка кликабельности пунктов каталога')
    def test_clickability_catalog_items(self):
        result = []
        browser.get(link)
        catalog = browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]')
        catalog.click()
        time.sleep(0.5)
        for i in range(5):
            try:
                items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
                item = items[randint(0, len(items) - 1)]
                name_item = item.text
                item.click()
                with allure.step(f'Скрин {name_item}'):
                    allure.attach(browser.get_screenshot_as_png(), name = f'Скрин после клика в пункт {name_item}', attachment_type=AttachmentType.PNG)
            except ElementNotInteractableException:
                result.append(False)
            else: result.append(True)
            browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]').click()
            self.assertTrue(result, msg = f'Категории не кликабельны {date}')

    @allure.title('Проверка скролла каталога')
    def test_catalog_scroll(self):
        browser.get(link)
        catalog = browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]')
        catalog.click()
        time.sleep(0.5)
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до прокрутки каталога', attachment_type=AttachmentType.PNG)
        result = len(browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]'))
        last_item = browser.find_element(By.CSS_SELECTOR, f'.fb-header-catalog-menu__parent-link[data-id="{result}"]')
        action.move_to_element(last_item).perform()
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после прокрутки каталога', attachment_type=AttachmentType.PNG)
        select_item = browser.find_element(By.CSS_SELECTOR, f'a[class="fb-header-catalog-menu__parent-link fb-header-catalog-menu__parent-menu_selected"][data-id="{result}"]')
        self.assertTrue(select_item, msg = f'меню каталога не скроллится {date}')

    @allure.title('Проверка изменения кнопки каталога после клика')
    def test_catalog_button_change(self):
        before_click = []
        after_click = []
        browser.get(link)
        button = browser.find_element(By.CSS_SELECTOR, 'div[class="header-v2-catalog-button"]')
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин до клика в кнопку "Каталог"', attachment_type=AttachmentType.PNG)
        before_click.append(browser.find_element(By.CSS_SELECTOR, 'a[class="dropdown-toggle header-v2-catalog"] span[class="first"]').value_of_css_property('transform'))
        before_click.append(browser.find_element(By.CSS_SELECTOR, 'a[class="dropdown-toggle header-v2-catalog"] span[class="second"]').value_of_css_property('transform'))
        before_click.append(browser.find_element(By.CSS_SELECTOR, 'a[class="dropdown-toggle header-v2-catalog"] span[class="third"]').value_of_css_property('transform'))
        button.click()
        time.sleep(0.5)
        with allure.step('Скрин'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин после клика в кнопку "Каталог"', attachment_type=AttachmentType.PNG)
        after_click.append(browser.find_element(By.CSS_SELECTOR, 'td[class="menu-item dropdown catalog fb-toggle-catalog-button"] span[class="first"]').value_of_css_property('transform'))
        after_click.append(browser.find_element(By.CSS_SELECTOR, 'td[class="menu-item dropdown catalog fb-toggle-catalog-button"] span[class="second"]').value_of_css_property('transform'))
        after_click.append(browser.find_element(By.CSS_SELECTOR, 'td[class="menu-item dropdown catalog fb-toggle-catalog-button"] span[class="third"]').value_of_css_property('transform'))
        self.assertNotEqual(before_click, after_click, msg = f'Кнопка каталога не изменилась {date}')

    @allure.title('Проверка корректности расположения разделов каталога')
    def test_correct_location_directory_sections(self):
        standard = [f'Новогодние украшения', f'Тепловое оборудование', f'Сантехника', f'Стройматериалы', f'Напольные покрытия', f'Отделка стен и потолка', f'Керамическая плитка', f'Двери и окна', f'Лакокрасочные материалы', f'Климат и отопление', f'Инструменты', f'Товары для дачи и отдыха', f'Мебель', f'Освещение', f'Водоснабжение', f'Электротовары', f'Крепеж и фурнитура', f'Товары для дома', f'Автотовары']
        with allure.step('Эталон'):
            allure.attach(f'{standard}', name = 'Категории', attachment_type=AttachmentType.TEXT)
        new = []
        error = None
        browser.get(link)
        browser.find_element(By.CSS_SELECTOR, '.header-v2-catalog-button').click()
        selected_item = browser.find_element(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link fb-header-catalog-menu__parent-menu_selected"]')
        name_selected_item = selected_item.text
        new.append(name_selected_item)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        for item in items:
            item_text = item.text
            new.append(item_text)
        if standard == new: 
            result = True
        else: 
            result = False
            error = 'Порядок категорий не совпадает'
            with allure.step('Данные страницы'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин списка каталога', attachment_type=AttachmentType.PNG)
                allure.attach(f'{error}\n{new}', name = f'ошибка', attachment_type=AttachmentType.TEXT)
            if len(standard) != len(new):
                result = False
                error = 'Не совпадает количество категорий'
                with allure.step('Данные страницы'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин списка каталога', attachment_type=AttachmentType.PNG)
                    allure.attach(f'{error}\n{new}', name = f'ошибка', attachment_type=AttachmentType.TEXT)
        with allure.step('Эталон'):
            allure.attach(f'{new}', name = 'Категории сейчас', attachment_type=AttachmentType.TEXT)
        self.assertTrue(result, msg = f'{error} {date}')


@allure.feature('Раздел каталога')          
class catalog_section(unittest.TestCase):

    @allure.title('Проверкка открытия раздела каталога')
    def test_a_opening_directory_catalog(self):
        browser.get(link)
        catalog_button = browser.find_element(By.CSS_SELECTOR, 'div[class="table-menu"]')
        catalog_button.click()
        time.sleep(0.3)
        items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        item = items[randint(0, len(items) -1)]
        item_name = item.text
        action.move_to_element(item).perform()
        item.click()
        subcategory_items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-deep-page__category-children-column"]')
        subcategory = subcategory_items[randint(0, len(subcategory_items) -1)]
        subcategory_name = subcategory.text
        action.move_to_element(subcategory).perform()
        subcategory.click()
        global url_subcategory
        url_subcategory = browser.current_url
        try:
            products = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__product-column"]')
            product_num = len(products)
            with allure.step('Ссылки и скрин'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.title, name = 'title', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step('Не найдены товары на странице'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.title, name = 'title', attachment_type=AttachmentType.TEXT)   
        self.assertTrue (product_num > 0, msg = f'На странице отсутствуют карточки товаров {date}')

    @allure.title('Проверка "хлебных корочек"')
    def test_bread_crusts(self):
        browser.get(url_subcategory)
        try:
            bread_crusts = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-breadcrumb__value"]')
            action.move_to_element(bread_crusts).perform()
            time.sleep(0.3)
            with allure.step("Хлебные корочки"):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step("Хлебные корочки не найдены"):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(bread_crusts.is_displayed(), msg = f'Хлебные корочки не найдены на странице {date}')    

    @allure.title('Проверка скролла страницы')
    def test_scroll_page(self):
        browser.get(url_subcategory)
        with allure.step('Скрин до скролла'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        # try:
        #     scroll_to_top = browser.find_element(By.CSS_SELECTOR, 'a[class="scroll-to-top RECT_WHITE PADDING visible"]')
        # except NoSuchElementException:
        scroll = browser.find_element(By.TAG_NAME, 'footer')
        action.scroll_to_element(scroll).perform()
        time.sleep(0.3)
        with allure.step('Скрин после скролла'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        scroll_to_top = browser.find_element(By.CSS_SELECTOR, 'a[class="scroll-to-top RECT_WHITE PADDING visible"]')
        self.assertTrue(scroll_to_top.is_displayed(), msg = f'Страница не скроллится {date}')
    
    @allure.title('Проверка сортировки по цене')
    def test_sorting_min_price(self):
        before_item_list = []
        after_item_list = []
        browser.get(url_subcategory)
        items_page = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items_page:
            item_id = i.get_attribute('data-id')
            before_item_list.append(item_id)
        try:
            filters = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__container"]')
            action.move_to_element(filters).perform()
            with allure.step('Скрин до изменения сортировки по минимальной цене'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            min_price = browser.find_element(By.CSS_SELECTOR, 'div[data-value="cheap"]').click()
            time.sleep(3)
            with allure.step('Скрин после изменения сортировки по минимальной цене'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items_page2 = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page2:
                item_id = i.get_attribute('data-id')
                after_item_list.append(item_id)
        except NoSuchElementException:
            pass
        self.assertNotEqual(before_item_list, after_item_list, msg = f'Сортировка по цене не работает {date}')

    @allure.title('Проверка сортировки по скидке')
    def test_sorting_discount(self):
        before_item_list = []
        after_item_list = []
        browser.get(url_subcategory)
        items_page = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        for i in items_page:
            item_id = i.get_attribute('data-id')
            before_item_list.append(item_id)
        try:
            filters = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__container"]')
            action.move_to_element(filters).perform()
            with allure.step('Скрин до изменения сортировки по скидке'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            discount_size = browser.find_element(By.CSS_SELECTOR, 'div[data-value="discount_size"]').click()
            time.sleep(3)
            with allure.step('Скрин после изменения сортировки по скидке'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items_page2 = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page2:
                item_id = i.get_attribute('data-id')
                after_item_list.append(item_id)
        except NoSuchElementException:
            pass
        self.assertNotEqual(before_item_list, after_item_list, msg = f'Сортировка по скидке не работает {date}')

    @allure.title("Проверка сортировки по популярности")
    def test_sorting_popular(self):
        before_item_list = []
        after_item_list = []
        browser.get(url_subcategory)
        try:
            filters = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-select__container"]')
            action.move_to_element(filters).perform()
            with allure.step('Скрин по популярности'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            discount_size = browser.find_element(By.CSS_SELECTOR, 'div[data-value="discount_size"]').click()
            time.sleep(3)
            items_page = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page:
                item_id = i.get_attribute('data-id')
                before_item_list.append(item_id)
            with allure.step('Скрин до изменения сортировки по популярности'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            filters.click()
            popular = browser.find_element(By.CSS_SELECTOR, 'div[data-value="popular"]').click()
            time.sleep(3)
            with allure.step('Скрин после изменения сортировки по популярности'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            items_page2 = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_page2:
                item_id = i.get_attribute('data-id')
                after_item_list.append(item_id)
        except NoSuchElementException:
            pass
        self.assertNotEqual(before_item_list, after_item_list, msg = f'Сортировка по популярности не работает {date}')

    @allure.title('Проверка открытия фильтра')
    def test_opening_filter(self):
        #browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        try:
            input_block = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1111"] input[type="text"]')
            if input_block.is_displayed() == True:
                result_before = True
            else: result_before = False
        except NoSuchElementException:
            pass
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(buttons[9]).perform()
        with allure.step('Скрин до раскрытия фильтра'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        button = buttons[8].click()
        time.sleep(2)
        with allure.step('Скрин после раскрытия фильтра'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            input_block = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1111"] input[type="text"]')
            if input_block.is_displayed() == True:
                result_after = True
            else: result_after = False
        except NoSuchElementException:
            pass
        self.assertNotEqual(result_before, result_after, msg = f'Фильтр не открылся {date}')

    @allure.title('Проверка работы ползунков фильтра')
    def test_p_filter_sliders(self):
        error = ''
        #browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(buttons[9]).perform()
        button = buttons[9].click()
        time.sleep(1)
        with allure.step('Скрин до перемещения ползунков'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_max_before = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuemax')
        input_min_before = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuenow')
        dots = browser.find_elements(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-touch-area"]')
        dot_min = dots[0]
        dot_max = dots[1]
        action.drag_and_drop_by_offset(dot_max, -50, 0).perform()
        action.drag_and_drop_by_offset(dot_min, 50, 0).perform()
        time.sleep(2)
        with allure.step('Скрин после перемещения ползунков'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_max_after = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuemax')
        input_min_after = browser.find_element(By.CSS_SELECTOR, 'div[data-name="N1118"] div[class="noUi-handle noUi-handle-lower"]').get_attribute('aria-valuenow')
        if input_max_before != input_max_after:
            result_max = True
        else: 
            error = "Не работает ползунок максимум"
            result_max = False
            with allure.step('Максимальное значение не изменилась, ползунок не работает'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        if input_min_before != input_min_after:
            result_min = True
        else: 
            result_min = False
            error = "Не работает ползунок минимум"
            with allure.step('Минимальное значение не изменилась, ползунок не работает'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        if result_max == result_min:
            result = True
        else: result = False
        self.assertTrue(result, msg = f'{error} {date}')

    @allure.title('Проверка работы чекбоксов бренды')
    def test_filter_check_box_brands(self):
        num_after = 0
        num = 0
        browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        boxes = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"] input[name="L346[]"] + label')
        count_boxes = len(boxes)
        items_before = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__product-column"]')
        num_before = len(items_before)
        with allure.step(f'Скрин до включения фильтров'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин',  attachment_type=AttachmentType.PNG)
        for box in boxes:
            box_name = box.text
            box.click()
            time.sleep(4)
            with allure.step(f'Скрин выдачи после включения фильтра {box_name}'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин',  attachment_type=AttachmentType.PNG)
            items_after = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__product-column"]')
            num_after += len(items_after)
            title_items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-product-card__title-inner"]')
            for i in title_items:
                name = i.text
                if (any(map(name.lower().__contains__, map(str.lower, box_name)))):
                    num += 1
                else: 
                    num -= 0
                    with allure.step(f'В выдаче {box_name} есть товары не соотвтствующие выбранному чек-боксу'):
                        allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин',  attachment_type=AttachmentType.PNG)
            box.click()
        self.assertEqual(num, num_after, msg = f'В выдаче есть товары не соотвтствующие выбранному чек-боксу {date}')

    @allure.title('Проверка работы фильтра по минимальной цене')
    def test_filter_min_price(self):
        after_price = 0
        #browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')).perform()
        input_min = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider-input__input"] input[type="text"]')
        input_name = browser.find_element(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider__slider noUi-target noUi-ltr noUi-horizontal noUi-txt-dir-ltr"]').get_attribute('data-min')
        back_num = len(input_name)
        with allure.step('Скрин выдачи до изменения минимальной цены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_min.click()
        for i in range(back_num):
            input_min.send_keys(Keys.BACKSPACE)
        input_min.send_keys('3000')
        time.sleep(3)
        with allure.step('Скрин выдачи после изменения минимальной цены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        prices = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]')
        items_num = len(prices) 
        for i in prices:
            price_str = i.text
            price = int(''.join(filter(str.isdigit, price_str)))
            if price >= 3000:
                after_price += 1
            else: after_price -= 1
        clear = browser.find_element(By.CSS_SELECTOR, '#fb-filter-chips-remove-all')
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')).perform()
        clear.click()
        self.assertEqual(after_price, items_num, msg = f'Фильтр по минимальной цене не работает {date}')

    @allure.title('Проверка работы фильтра по максимальной цене')
    def test_filter_max_price(self):
        after_price = 0
        #browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(buttons[9]).perform()
        inputs = browser.find_elements(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider-input__input"] input[type="text"]')
        input_max = inputs[1]
        inputs_names = browser.find_elements(By.CSS_SELECTOR, 'div[data-name="PRICE"] div[class="fb-slider__slider noUi-target noUi-ltr noUi-horizontal noUi-txt-dir-ltr"]')
        input_name = inputs_names[1].get_attribute('data-max')
        back_num = len(input_name)
        with allure.step('Скрин выдачи до изменения минимальной цены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        input_max.click()
        for i in range(back_num):
            input_max.send_keys(Keys.BACKSPACE)
        input_max.send_keys('4000')
        time.sleep(3)
        with allure.step('Скрин выдачи после изменения минимальной цены'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        prices = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card__price-value"]')
        items_num = len(prices) 
        for i in prices:
            price_str = i.text
            price = int(''.join(filter(str.isdigit, price_str)))
            if price < 4000:
                after_price += 1
            else: after_price -= 1
        clear = browser.find_element(By.CSS_SELECTOR, '#fb-filter-chips-remove-all')
        action.move_to_element(browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')).perform()
        clear.click()
        self.assertEqual(after_price, items_num, msg = f'Фильтр по максимальной цене не работает {date}')

    @allure.title('Проверка работы фильтра по параметрам')
    def test_filter_checkbox_parameters(self):
        result = 0
        #browser.get('https://stroylandiya.ru/catalog/dvernoe-polotno/')
        buttons = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-filter__row"]')
        action.move_to_element(buttons[9]).perform()
        button = buttons[5].click()
        boxes = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-checkbox"] [name="L521[]"]+label')
        coun_boxes = len(boxes)
        with allure.step(f'Скрин до включения фильтров'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин',  attachment_type=AttachmentType.PNG)
        for box in boxes:
            id_items_before = []
            id_items_after = []
            items_before = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_before:
                i.get_attribute('data-id')
                id_items_before.append(i)
            box_name = box.text
            box.click()
            time.sleep(2)
            items_after = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
            for i in items_after:
                i.get_attribute('data-id')
                id_items_after.append(i)
            with allure.step(f'Скрин выдачи после включения фильтра {box_name}'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин',  attachment_type=AttachmentType.PNG)
            if id_items_before != id_items_after:
                result += 1
            else: 
                result -= 1
                with allure.step(f'Чек-бокс {box_name} не работает, либо нет товаров соответствующих фильтру'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин',  attachment_type=AttachmentType.PNG)
            box.click()
            id_items_before.clear()
            id_items_after.clear()
            time.sleep(2)
        self.assertEqual(result, coun_boxes, msg = f'Один из чек-боксов не работает {date}')

@allure.feature('Карточка товара, поле корзины')
@allure.severity('CRITICAL')
class test_product_card(unittest.TestCase):
    @allure.title('Проверка кнопки добавить в корзину')
    @allure.severity('BLOCKER')
    def test_add_to_cart(self):
        catalog_button = browser.find_element(By.CSS_SELECTOR, 'div[class="table-menu"]')
        catalog_button.click()
        time.sleep(0.3)
        catalog_items = browser.find_elements(By.CSS_SELECTOR, 'a[class="fb-header-catalog-menu__parent-link"]')
        item = catalog_items[randint(0, len(catalog_items) - 1)]
        item.click()
        category_items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-deep-page__category-children-column"]')
        category_item = category_items[randint(0, len(category_items) - 1)]
        category_item.click()
        browser.find_element(By.CSS_SELECTOR, 'div[class="fb-switch"]').click()
        items = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-product-card fb-product-card-2"]')
        item = items[randint(0, len(items) - 1)]
        item.click()
        button_before = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').is_displayed()
        with allure.step('До клика добавить в корзину'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name= 'Скрин', attachment_type=AttachmentType.PNG)
        button = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]')
        try:
            button.click()
        except ElementNotInteractableException:
            with allure.step('Кнопка добавить в коризину не кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name= 'Скрин', attachment_type=AttachmentType.PNG)
        basket_count = browser.find_element(By.CSS_SELECTOR, 'span[class="fb-sticky-menu__badge_type_basket count"]').text
        time.sleep(3)
        with allure.step('после клика добавить в корзину'):
            allure.attach(browser.get_screenshot_as_png(), name= 'Скрин', attachment_type=AttachmentType.PNG)
        button_after = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').is_displayed()
        self.assertNotEqual(button_before, button_after)
    
    @allure.title('Проверка кнопки бонусы')
    def test_bonus(self):
        try:
            bonus_before = browser.find_element(By.CSS_SELECTOR, 'div[class="price-block--bonus---tooltip-text"]').is_displayed()
        except NoSuchElementException:
            bonus_before = None
        try:
            bonus = browser.find_element(By.CSS_SELECTOR, 'div[class="dcol-0 price-block--bonus"]')
        except NoSuchElementException:
            pytest.skip('на странице нет кнопки "Бонусы"')
        bonus.click()
        with allure.step('После клика по кнопке "бонусы"'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        bonus_after = browser.find_element(By.CSS_SELECTOR, 'div[class="price-block--bonus---tooltip-text"]').is_displayed()
        self.assertNotEqual(bonus_before, bonus_after)
    
    @allure.title('Поиск и проверка кликабельности поля для ввода количества товара')
    def test_click_quantity_field(self):
        error = ''
        try:
            input_quatity = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_shoping--wrapper"] input[type="number"]')
            scroll = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
            action.move_to_element(scroll).perform()
        except NoSuchElementException:
            input_quatity = None
            error = 'Поле для ввода не найдено'
        try:
            input_quatity.click()
        except ElementNotInteractableException:
            input_quatity = None
            error = 'Поле для ввода не кликабельно'
        with allure.step('После клика в поле ввода количества товара'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertIsNotNone(input_quatity, msg = f'{error} {date}')

    @allure.title('Проверка ввода максимального количества товара')
    def test_max_items(self):
        max_items = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] button[class="btn_reset product_card_qty--btn js_plus"]').get_attribute('data-max')
        input_quantity = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] input[type="number"]')
        input_quantity.click()
        input_quantity.send_keys(Keys.ARROW_RIGHT)
        input_quantity.send_keys(Keys.BACKSPACE)
        input_quantity.send_keys(max_items)
        browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"]').click()
        quantity = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
        with allure.step('Скрин после ввода максимального количества товара'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertEqual(max_items, quantity, msg = f'В поле нельзя ввести максимальное количество товара {date}')

    @allure.title('Проверка ввода, больше максимального количества товара')
    def test_more_max_items(self):
        max_items = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] button[class="btn_reset product_card_qty--btn js_plus"]').get_attribute('data-max')
        input_quantity = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] input[type="number"]')
        quantity = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
        input_quantity.click()
        cunt_qunt = len(quantity)
        input_quantity.click()
        for i in range(cunt_qunt):
            input_quantity.send_keys(Keys.ARROW_RIGHT)
        for i in range(cunt_qunt):
            input_quantity.send_keys(Keys.BACKSPACE)
        input_quantity.send_keys(int(max_items)+1)
        with allure.step('Скрин значения введенного в поле'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"]').click()
        time.sleep(1)
        quantity = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
        with allure.step('Скрин после снятия фокуса с поля'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertEqual(max_items, quantity, msg = f'В поле можно ввести количество товара больше максимального {date}')

    @allure.title('Проверка ввода "1"')
    def test_one_items(self):
        max_items = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] button[class="btn_reset product_card_qty--btn js_plus"]').get_attribute('data-max')
        input_quantity = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] input[type="number"]')
        quantity = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
        input_quantity.click()
        cunt_qunt = len(quantity)
        input_quantity.click()
        for i in range(cunt_qunt):
            input_quantity.send_keys(Keys.ARROW_RIGHT)
            time.sleep(0.5)
        for i in range(cunt_qunt):
            input_quantity.send_keys(Keys.BACKSPACE)
            time.sleep(0.5)
        input_quantity.send_keys('1')
        with allure.step('Скрин значения введенного в поле'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"]').click()
        time.sleep(1)
        quantity = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
        with allure.step('Скрин после снятия фокуса с поля'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(quantity == '1', msg = f'В поле нельзя ввести "1" {date}')

    @allure.title('Проверка ввода "0"')
    def test_wnull_items(self):
        input_quantity = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] input[type="number"]')
        quantity = browser.find_element(By.CSS_SELECTOR, 'button[class="dc-btn -primary -hover p_product_shoping--btn js_to_cart js_to_cart--detail btn-to-basket--full-width"]').get_attribute('data-quantity')
        cunt_qunt = len(quantity)
        input_quantity.click()
        for i in range(cunt_qunt):
            input_quantity.send_keys(Keys.ARROW_RIGHT)
            time.sleep(0.5)
        for i in range(cunt_qunt):
            input_quantity.send_keys(Keys.BACKSPACE)
            time.sleep(0.5)
        input_quantity.send_keys('0')
        with allure.step('Скрин значения введенного в поле'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"]').click()
        time.sleep(2)
        with allure.step('Скрин после снятия фокуса с поля'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertFalse(browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_block -shoping mb-24 js-shoping_block"] div[class="dc-row product_card_qty "]').is_displayed(), msg = f'В поле можно ввести 0 {date}')

    @allure.title('Проверка наличия фотографий')
    def test_photo_find(self):
        result_list = []
        try:
            main_photo = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"] img')
            result = main_photo.size
        except NoSuchElementException:
            try:
                only_one_photo = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom"] [class="img"]')
                result = only_one_photo.size
            except NoSuchElementException:
                error = "На странице нет активного фото"
                with allure.step('Нет главного фото товара'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                pytest.skip('На странице нет активного фото')
            with allure.step('Только одно фото'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        with allure.step('Главное фото товара есть на странице'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            for i in result.values():
                if int(i) > 50:
                    result_list.append(True)
                else: result_list.append(False)
            for i in result_list:
                if i == False:
                    result = False
                    break
                else:
                    result = True           
        self.assertTrue(result, msg = f'Главного фото нет на странице {date}')

    @allure.title('Проверка увеличения фото')
    def test_photo(self):
        try:                                
            open_photo = browser.find_element(By.CSS_SELECTOR, 'div[class="swiper p_product_swiper_main--swiper js-product-main swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events"]')
            try:
                style_before = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"] img[class="img"]').get_attribute('style')
                with allure.step('Фото до фокуса'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                action.move_to_element(open_photo).perform()
                time.sleep(1)
                with allure.step('Фото после фокуса'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                style_after = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom swiper-slide-active"] img[class="img"]').get_attribute('style')
            except NoSuchElementException:
                pass
        except NoSuchElementException:
            try:
                open_photo = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom"] img')
                style_before = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom"] img').get_attribute('style')
                with allure.step('Фото до фокуса'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                action.move_to_element(open_photo).perform()
                time.sleep(1)
                with allure.step('Фото после фокуса'):
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                style_after = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_swiper_main--item swiper-slide js-zoom"] img').get_attribute('style')
            except NoSuchElementException:
                with allure.step('На странице нет фото товара'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        if style_before != style_after:
            result = True
        else: result = False
        self.assertTrue(result, msg = f'Фото не увеличивается {date}') 

    @allure.title('Проверка переключения фотографий')
    def test_switching_photos(self):
        result = 0
        photos = browser.find_elements(By.CSS_SELECTOR, 'div[class="p_product_swiper_main_nav--wrapper d-none d-lg-block"] img[class="img"]') 
        coun_photo = len(photos)
        if coun_photo < 1:
            with allure.step('Всего одно фото'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('Всего одно фото')
        for i in photos:
            try:
                i.click()
                with allure.step('После клика по фото'):
                    allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                    allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                result += 1
            except ElementNotInteractableException:
                try:
                    button_next = browser.find_element(By.CSS_SELECTOR, 'button[id="p_product_swiper_main_nav--next"]')
                    try:
                        button_next.click()
                        try:
                            i.click()
                            with allure.step('После клика по фото'):
                                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                            result += 1
                        except ElementNotInteractableException:
                            result += 0
                            with allure.step('Фотография не кликабельна'):
                                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                    except ElementNotInteractableException:
                        with allure.step('Кнопка прокрутки фотографий не кликабельна"'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                except NoSuchElementException:
                    with allure.step('Кнопка прокрутки фотографий не найдена'):
                        allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertEqual(coun_photo, result, msg = f'Фотографии не переключаются {date}')

    @allure.title('Проверка отображения наличия товара для самовывоза')
    def test_zdisplaying_availability_self_pickup(self):
        scroll = browser.find_element(By.CSS_SELECTOR, 'header[class="header -grey"]')
        action.move_to_element(scroll).perform()
        pickup = browser.find_element(By.CSS_SELECTOR, 'a[class="quantity-shops js-popup-open-link"]')
        pickup.click()
        try:
            availability = browser.find_element(By.CSS_SELECTOR, 'div[class="modal__map-block-shops-inner-all-li-body-text"]')
        except NoSuchElementException:
            with allure.step('Элементы "В наличии" отсутствуют на странице'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('Элементы "В наличии" отсутствуют на странице')
        with allure.step('Элементы "В наличии"'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(availability.is_displayed(), msg = f'Наличие в магазинах не видно пользователю {date}')

    @allure.title('Проверка переключения, все, магазины, пвз')
    def test_zswitching_all_shop_pv(self):
        result = 0
        error = ""
        try:
            button_all = browser.find_element(By.CSS_SELECTOR, 'button[data-tab="all"]')
        except NoSuchElementException:
            with allure.step('Кнопка "Все" не найдена'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_all.click()
            activ_all = browser.find_element(By.CSS_SELECTOR, 'button[class="modal__map-block-map-tabs-item js-modal-map-btn-tab is-active"]').get_attribute('data-tab')
            with allure.step('После клика во "Все"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            if activ_all == 'all':
                result += 1
            else:
                result -= 1
                error = 'Кнопка "Все" не работает'
        except ElementNotInteractableException:
            with allure.step('Кнопка "Все" кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_shops = browser.find_element(By.CSS_SELECTOR, 'button[data-tab="shop"]')
        except NoSuchElementException:
            with allure.step('Кнопка "Магазины" не найдена'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_shops.click()
            activ_all = browser.find_element(By.CSS_SELECTOR, 'button[class="modal__map-block-map-tabs-item js-modal-map-btn-tab is-active"]').get_attribute('data-tab')
            with allure.step('После клика в "Магазины"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            if activ_all == 'shop':
                result += 1
            else:
                result -= 1
                error = 'Кнопка "Магазины" не работает'
        except ElementNotInteractableException:
            with allure.step('Кнопка "Маазины" кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_pv = browser.find_element(By.CSS_SELECTOR, 'button[data-tab="pvz"]')
        except NoSuchElementException:
            with allure.step('Кнопка "Пункты выдачи" не найдена'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            button_pv.click()
            activ_all = browser.find_element(By.CSS_SELECTOR, 'button[class="modal__map-block-map-tabs-item js-modal-map-btn-tab is-active"]').get_attribute('data-tab')
            with allure.step('После клика в "Пункты выдачи"'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            if activ_all == 'pvz':
                result += 1
            else:
                result -= 1
                error = 'Кнопка "Пункты выдачи" не работает'
        except ElementNotInteractableException:
            with allure.step('Кнопка "Пункты выдачи" кликабельна'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(result == 3, msg = f'Кнопки не работают {error} {date}')

    @allure.title('Поиск блока с характеристиками рядом с фото')
    def test_brief_characteristics(self):      
        try:
            specifications = browser.find_element(By.CSS_SELECTOR, 'div[class="dcol-8 dcol-lg-11 dcol-xl-12 d-none d-lg-block ml-five_percents"]')
            with allure.step("Блок характеристик рядом с фото найден"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step("Блок характеристик рядом с фото не найден"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(specifications, msg = f'Характеристики не видны пользователю {date}')

    @allure.title('Проверка кнопки все характеристики')
    def test_xbutton_all_characteristics(self):      
        try:
            specifications_button = browser.find_element(By.CSS_SELECTOR, 'a[class="js-scrollto all_characteristics"]')
        except NoSuchElementException:
            with allure.step("Кнопка все характеристики не найдена"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            specifications_button.click()
            time.sleep(1)
            with allure.step("Открытие блока характеристик"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
             with allure.step("Кнопка все характеристики не кликабельна"):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        block = browser.find_element(By.CSS_SELECTOR, 'div[class="container-fluid"] div[class="p_product_params js-btns js-params active"]')
        self.assertTrue(block.is_displayed(), msg = f'Блок характеристик не виден пользователю {date}')

    @allure.title('Проверка описания товара')
    def test_xdescription(self):
        buttons = browser.find_elements(By.CSS_SELECTOR, 'button[class="fw700 btn_reset p_product_info--item"]')
        time.sleep(5)
        buttons[0].click()
        try:
            rev  = browser.find_element(By.CSS_SELECTOR, 'div[class="p_product_about js-btns js-about text active"]')
        except NoSuchElementException:
            with allure.step('Нет блока описания товара'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        with allure.step('Блок описание товара'):
                allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(rev.is_displayed(), msg = f'Описание товара не отображается на странице {date}')

@allure.feature('Корзина')
@allure.severity('CRITICAL')
class test_basket(unittest.TestCase):
    @allure.title('Корзина')
    def test_basket_items(self):
        browser.get('https://stroylandiya.ru/')
        error = ''
        items_names = []
        basket_items_names = []
        count_click_item = 0
        hit_sales = browser.find_element(By.CSS_SELECTOR, 'a.hits__all-hits-link')
        hit_sales.click()
        items = browser.find_elements(By.CSS_SELECTOR, 'div.fb-catalog-listing-page__product-column')
        count_items = len(items)
        if count_items > 5:
            for i in items[:5]:
                action.move_to_element(i).perform()
                item_name = i.find_element(By.CSS_SELECTOR, 'a.fb-product-card__title-inner').text
                items_names.append(item_name)
                i.find_element(By.CSS_SELECTOR, 'div.fb-product-card__basket-button').click()
                time.sleep(0.5)
                count_click_item +=1
        else:
            for i in items:
                action.move_to_element(i).perform()
                time.sleep(0.5)
                item_name = i.find_element(By.CSS_SELECTOR, 'a.fb-product-card__title-inner').text
                items_names.append(item_name)
                i.find_element(By.CSS_SELECTOR, 'div.fb-product-card__basket-button').click()
                count_click_item +=1
        with allure.step('Выбранные товары из каталога'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        basket_button = browser.find_element(By.CSS_SELECTOR, 'a.header-v2-basket').click()
        time.sleep(3)
        with allure.step('Добавленные товары в корзине'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        basket_items = browser.find_elements(By.CSS_SELECTOR, 'div.basket-item')
        count_basket_items = len(basket_items)
        for i in basket_items:
            i.find_element(By.CSS_SELECTOR, 'div.basket-item__content a[href]')
            basket_item_name = i.text.split("\n")[0]
            basket_items_names.append(basket_item_name)
        if count_click_item == count_basket_items:
            count_result = True
            error_count = 'Количество совпадает'
        else: 
            count_result = False
            error_count = 'Количество не совпадает'
        if items_names == basket_items_names:
                items_result = True
                error_name = 'Наименование совпадает'
        else:
            items_result =False
            error_name = 'Наименование не совпадает'
        
        self.assertTrue(count_result and items_result, msg = f'{error_count}, {error_name} {date}')

    @allure.title('Удаление товаров из корзины')
    def test_delete_item(self):
        basket_items_before = browser.find_elements(By.CSS_SELECTOR, 'div.basket-item')
        count_items_before = len(basket_items_before)
        with allure.step('Корзина до удаления товара'):
            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        delete_item = browser.find_element(By.CSS_SELECTOR, 'div.basket-actions__items a.basket-actions__item')
        try:
            delete_item.click()
        except ElementNotInteractableException:
            with allure.step('Кнопка удаления товара не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
            pytest.skip('Кнопка удаления товара не кликабельна')
        time.sleep(2)
        with allure.step('Корзина после удаления 1ед. товара'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        basket_items_after = browser.find_elements(By.CSS_SELECTOR, 'div.basket-item')
        count_items_after = len(basket_items_after)
        self.assertNotEqual(count_items_before, count_items_after, msg = f'Товар не удалился из корзины {date}')

    @allure.title('Поле ввода промокода')
    def test_promo_code(self):
        with allure.step('Блок промокода, до ввода промокода'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input = browser.find_element(By.CSS_SELECTOR, '.total-coupon__block input')
        except NoSuchElementException:
            with allure.step('Поле ввода промокода не найдено'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input.click()
            with allure.step('Блок промокода, после клика в поле'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)   
        except ElementNotInteractableException:
            with allure.step('Поле не кликабельно'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input.send_keys('test')
            time.sleep(0.5)
            with allure.step('Блок промокода, после ввода промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('В поле нельзя вводить текст'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            delete_promocode = browser.find_element(By.CSS_SELECTOR, '.total-coupon__block div[class="form-input__icon i-icon"]')
        except NoSuchElementException:
            with allure.step('В поле нет кнопки удаления промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            delete_promocode.click()
            time.sleep(0.5)
            with allure.step('Блок промокода, после удаления промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('Кнопка удаления промокода не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promocode_input.send_keys('test')
            time.sleep(0.5)
        except ElementNotInteractableException:
            with allure.step('Нельзя ввести промкод после удаления'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promo_button = browser.find_element(By.CSS_SELECTOR, '.total-coupon__wrapper .total-coupon__btn')
        except NoSuchElementException:
            with allure.step('Кнопка "Применить" промокод не найдена'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            promo_button.click()
        except ElementNotInteractableException:
            with allure.step('Кнопка "Применить" промокод не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        time.sleep(0.5)
        try:
            message = browser.find_element(By.CSS_SELECTOR, '.total-coupon__message')
            with allure.step('Блок промокода, применения промокода'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except NoSuchElementException:
            with allure.step('Сообщение о принялии/не принятии промокода не появилось'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        self.assertTrue(message.is_displayed(), msg = f'Блок промокода не работает {date}')

    @allure.title('Добавление товара в избранное из корзины')
    def test_d_basket_button_favorite(self):
        try:
            item_buttons = browser.find_elements(By.CSS_SELECTOR, '.basket-item__actions .basket-actions__item')
            like_button = item_buttons[1]
        except NoSuchElementException:
            with allure.step('Кнопка "Отложить товар" не найдена на странице'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        with allure.step('Корзина до клика "Отложить товар"'):
            allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        try:
            like_button.click()
            time.sleep(0.5)
            with allure.step('Корзина после клика "Отложить товар" у первого товара'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        except ElementNotInteractableException:
            with allure.step('Кнопка "Отложить товар" не кликабельна'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        cont_favarit_after_first_click = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-favourites"] span[class="fb-sticky-menu__badge_type_personal count"]').text
        like_button.click()
        time.sleep(0.5)
        with allure.step('Корзина после клика "Отложить товар" у второго товара'):
                allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        cont_favarit_after_second_click = browser.find_element(By.CSS_SELECTOR, 'a[class="header-v2-favourites"] span[class="fb-sticky-menu__badge_type_personal count"]').text
        self.assertTrue(int(cont_favarit_after_second_click) == int(cont_favarit_after_first_click) + 1, msg =f'Количество отложенного товара не изменилось {date}')

@allure.feature('SEO')
@allure.severity('CRITICAL')
class seo(unittest.TestCase):
    @allure.title('Проверка корректноси Title, Description, H1')
    def test_z_title_and_desc_h1(self):
        browser.get(link)
        all_result = 0
        for i in range(3):
            button_city = browser.find_element(By.CSS_SELECTOR, 'div.region_wrapper').click()
            time.sleep(0.5)
            cityes = browser.find_elements(By.CSS_SELECTOR, 'div.location-item')
            random_city = cityes[randint(0, len(cityes) -1)]
            random_city.click()
            time.sleep(3)
            for c in range(3):
                page_result = 0
                results = []
                city = browser.find_element(By.CSS_SELECTOR, 'div.region_wrapper a span').text
                catalog = browser.find_element(By.CSS_SELECTOR, '.menu-only').click()
                categoryes = browser.find_elements(By.CSS_SELECTOR, 'a.fb-header-catalog-menu__parent-link')
                category = categoryes[randint(0, len(categoryes) - 1)]
                category_name = category.text
                category.click()
                time.sleep(1)
                subcategoryes = browser.find_elements(By.CSS_SELECTOR, 'div.fb-catalog-deep-page__category-children-column')
                subcategory = subcategoryes[randint(0, len(subcategoryes) - 1)]
                subcategory.click()
                title = browser.title
                description = browser.find_element(By.CSS_SELECTOR, 'meta[name="description"]').get_attribute('content')
                page_title = browser.find_element(By.CSS_SELECTOR, 'h1[id="pagetitle"]').text
                first_city_name = city.split(' ', 1)[0]
                now_city = first_city_name[:-2]
                first_page_title = page_title.split(' ', 1)[0]
                now_page_title = first_page_title[:-2]
                if (re.findall(rf'{now_city}\w+', title, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', title, flags=re.IGNORECASE)):
                    results.append(True)
                else: 
                    results.append(False)
                if (re.findall(rf'{now_city}\w+', description, flags=re.IGNORECASE)) and (re.findall(rf'{now_page_title}\w+', description, flags=re.IGNORECASE)):
                    results.append(True)
                else: 
                    results.append(False)
                for i in results:
                    if i == True:
                        page_result += 1
                if page_result >=2:
                    all_result +=1
                    with allure.step(f'{city}/{category_name}/{page_title}'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{title}\n{city}\n{page_title}', name = 'тайтл подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{description}\n{city}\n{page_title}', name = 'диск подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{results}', name = 'смотри', attachment_type=AttachmentType.TEXT)
                else:
                    with allure.step(f'{city}/{category_name}/{page_title} ПРОВАЛ'):
                            allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{title}\n{city}\n{page_title}', name = 'тайтл подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{description}\n{city}\n{page_title}', name = 'диск подкатегории', attachment_type=AttachmentType.TEXT)
                            allure.attach(f'{results}', name = 'смотри', attachment_type=AttachmentType.TEXT)
        self.assertTrue(all_result == 9, msg = f'Тайтл или дискрипшен не верный {date}')

    @allure.title('Проверка корректноси шаблонного текста')
    def test_template_text(self):
        browser.get(link)
        result_list = []
        for i in range(2):
            button_city = browser.find_element(By.CSS_SELECTOR, 'div.region_wrapper').click()
            time.sleep(1)
            cityes = browser.find_elements(By.CSS_SELECTOR, 'div.location-item')
            random_city = cityes[randint(0, len(cityes) -1)]
            random_city.click()
            time.sleep(3)
            for c in range(2):
                city = browser.find_element(By.CSS_SELECTOR, 'div[class="region_wrapper h_full"] span').text
                menu = browser.find_element(By.CSS_SELECTOR, 'div.menu-only').click()
                catetgoryes = browser.find_elements(By.CSS_SELECTOR, 'a.fb-header-catalog-menu__parent-link')
                category = catetgoryes[randint(0, len(catetgoryes) -1)]
                action.move_to_element(category).perform()
                time.sleep(0.3)
                category.click()
                try:
                    subcategoryes = browser.find_elements(By.CSS_SELECTOR, 'div[class="fb-catalog-deep-page__category-children-column"]')
                    subcategory = subcategoryes[randint(0, len(subcategoryes) -1)]
                    subcategory.click()
                except NoSuchElementException:
                    pass
                url = browser.current_url
                page_title = browser.find_element(By.CSS_SELECTOR, 'h1#pagetitle').text
                seo = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__footer seo_text"]')
                action.move_to_element(seo).perform()
                time.sleep(0.5)
                seo_text = browser.find_element(By.CSS_SELECTOR, 'div[class="fb-catalog-listing-page__footer seo_text"]').text
                # if (re.findall(rf'{city[:-2]}\w+', seo_text, flags=re.IGNORECASE)) and (re.findall(rf'{page_title[:-2]}\w+', seo_text, flags=re.IGNORECASE)):
                if city in seo_text and page_title in seo_text:
                    result = True
                    result_list.append(result)
                    with allure.step(f'{city} {page_title}'):
                        allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                        allure.attach(f'{city}\n{page_title}\n{seo_text}', name = 'Данные', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
                else: 
                    result = False
                    result_list.append(result)
                    with allure.step(f'{city} {page_title} ПРОВАЛ'):
                        allure.attach(browser.current_url, name = 'url', attachment_type=AttachmentType.TEXT)
                        allure.attach(f'{city}\n{page_title}\n{seo_text}', name = 'Данные', attachment_type=AttachmentType.TEXT)
                        allure.attach(browser.get_screenshot_as_png(), name = 'Скрин', attachment_type=AttachmentType.PNG)
        for r in result_list:
            if r == False:
                finally_result = False
                break
            else: finally_result = True
        self.assertTrue(finally_result, msg = 'Некорректный шаблонный текст')

    # def test_quit(self):
    #     browser.quit()

if __name__ == "__main__":
    unittest.main()
